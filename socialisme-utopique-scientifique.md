---
geometry: margin=3cm
---

# Socialisme utopique et socialisme scientifique




## Introduction

De toutes parts émergent des discours qui mettent en opposition les élites et
les peuples.  Pourtant, que ce soit dans les mouvements pour l'écologie, pour
l'actualité au Chili ou pour les évènements historiques, nous préférons parler
des travailleurs : quel est le rapport de force qu'ils imposent, la direction
prise est-elle dans leurs intérêts, que peuvent-ils mettre en avant, etc.

Cette grille de lecture provient d'analyses des sociétés qui ont été développé
durant le 19ème siècle, notamment par Marx et Engels.  Ces analyses ne donnent
pas des solutions clefs en mains, mais permettent de comprendre certains
mécanismes et fournissent une méthode militante.

Pourquoi se revendiquer du marxisme ? Est-ce encore pertinent aujourd'hui ?




## L'origine du débat

Pour comprendre d'où viennent les analyses de Marx et Engels, il faut revenir un
peu avant leurs écrits.

- Les idées ne viennent pas de nulle part

Au cours des siècles, avec le développement progressif du mode de production
capitaliste, s'est développée une classe sociale qui occupe de plus en plus de
place dans l'économie : la bourgeoisie, commençant par accumuler de l'argent par
le commerce, puis par l'exploitation des travailleurs dans les premières usines,
manufactures, ateliers.  Parallèlement se développe le prolétariat qui se
retrouvera sur les devants de la scène avec notamment le mouvement chartiste en
Angleterre et la révolution de 1848.

Avec ces changements importants de société, naissent également de nouvelles
idées, qui découlent directement de ces changements.  Deux courants du
socialisme vont se succéder, le socialisme utopique, puis le socialisme
scientifique.

- Le socialisme utopique

La fin du 18ème siècle voit se dérouler la Révolution française.  Celle-ci n'est
pas venue à bout des inégalités malgré les aspirations progressives qu'elle a
porté.

Du constat de ces inégalités naissent les premières idées socialistes avec le
socialisme utopique.  Ces idées s'appuient sur les principes des philosophes des
Lumières du 18ème siècle.  Il faut donc revenir un peu sur ces principes.

Les idées des Lumières sont révolutionnaires à leur époque : elles énoncent
qu'il faut tout baser sur la raison, y compris l'organisation de la société et
l'État.  Ces idées, en opposition aux idées religieuses et à la monarchie
absolue, vont créer une base théorique permettant à la bourgeoisie de rallier
les paysans, artisans, sans-culottes, etc, c'est-à-dire les forces sociales
nécessaires pour renverser l'ancien ordre social.

Mais dés le début du 19ème siècle, les contradictions exploiteurs/exploités vont
amener les penseurs à remettre en question la nouvelle organisation sociale,
c'est-à-dire l'organisation capitaliste de la société.

La Révolution française, au delà des espoirs qu'elle a suscité à travers le
monde, a surtout eu pour rôle de sacraliser la propriété privée des moyens de
production, socle de la société capitaliste, en l'inscrivant même dans la
fameuse déclaration des droits de l'Homme et du citoyen : _le but de toute
association politique est la conservation des droits naturels et
imprescriptibles de l'Homme. Ces droits sont la liberté, la propriété, la
sûreté, et la résistance à l'oppression (Article 2)_.

Peu après la révolution, la volonté d'un nouveau changement de société vit le
jour.

La production encore peu développée, et un capitalisme encore à ses
balbutiements produisirent alors, à cette époque, des théories socialistes
imparfaites chez St Simon, Fourrier et Owen notamment.

- Robert Owen

Robert Owen est un utopiste anglais qui dirigea une fabrique de 500 ouvrieres,
puis une filature de coton de 2500 ouvriers en Écosse au début du 19ème siècle.
Il y menera diverses expérimentations pour rendre concrètes ses idées de
réduction des inégalites.  Il va par exemple ouvrir des crèches à proximité de
sa filature, réduire le nombre d'heure de travail (10h30 au lieu de 13h ou 14h
chez les concurrents).

Il se rendit compte que les ouvriers de sa filature continuaient à être
exploités, qu'ils étaient payés en deça du coup réel de leur force de travail.
Il milita alors pour une société dans laquelle les fabriques appartiendrait aux
ouvriers qui y travaillent, bref une société socialiste.  Mais, alors qu'il
était bien vu dans les sphères dirigeantes lorsqu'il menait ses experiences
socialisante dans son coin, militer activement en dehors de sa fabrique le mis
au ban de la société, et il ne parvint jamais à améliorer les conditions de vie
en Angleterre.

La faiblesse des Lumières et des socialiste utopiques est de croire en un
changement de société possible par un changement des idées.  Cette conception ne
prend pas en compte les forces sociales, essentielles dans le processus de
modification de la société.

Pour réellement mettre en pratique les idées socialistes, il lui manquait une
analyse de l'histoire tenant compte des intérêts matériels et des luttes de
classes basées sur ces intérêtes matériels.  Sans cela, impossible pour lui de
comprendre le rôle du prolétariat et celui de la bourgeoisie, ni même de les
identifier comme des classes sociales.








## Les fondements du socialisme scientifique

- Matérialisme dialectique (l'Histoire est l'histoire de la lutte des classes)

Cette analyse est fournie par la matérialisme historique de Marx et Engels.

À chaque période, le mode de production et d'échange d'une société produit une
certaine ditribution des richesses, et donc une division de la société en
classes sociales aux intérêts antagonistes.

**Citation** : "la structure économique d'une société donnée forme toujours la
base réelle qui permet, en dernière analyse, de comprendre toute la
superstructure des institutions juridiques et politiques, aussi bien que des
idées religieuses, philosophiques et autres qui lui sont propres".

Cela conduit à une conception de l'histoire comme l'histoire de la lutte des
classes.

Cette lutte n'est pas le produit de la bêtise ou la méchanceté de certaines
personnes, mais provient des rapports économiques entre classes sociales.  Ces
rapports économiques peuvent être étudiés, analysés.  C'est un des buts du
matérialisme historique, rendu possible par le développement des sciences
sociales comme l'économie politique, l'anthropologie, etc.

Les groupes sociaux ont différentes formes d'actions à leurs disposition, selon
leur place dans l'appareil de production et leur rapport à la propriété.  C'est
pour cela qu'en plus d'apporter une analyse de la société, le socialisme
scientifique permet de mettre au point un programme en fonction des interêts
d'une classe sociale.




- Économie politique, rapport à la propriété, exploitation, plus-value

Un apport important de Marx à la théorie socialiste est son analyse du
capitalisme.  Cette analyse permet de comprendre les contradictions entre le
prolétariat et la bourgeoisie.

Alors que le capitaliste a interêt à l'augmentation de son profit, le
travailleur, lui, a interêt à l'augmentation de son salaire.  Ces interêts sont
divergeants car augmenter les profits revient à diminuer les salaires et
inversement.


- Les 3 données du capitalisme

L'analyse du fonctionnement du capitalisme nous donne 3 faits qui présagent de
son effondrement :

1) L’anarchie croissante de l’économie

Après plusieurs centaines d'années sous mode de production capitaliste, nous
continuons à voir des crises de surproduction.  Elles apparaissent, entre autre,
car la planification de l'économie est impossible sous le capitalisme, car la
production n'est pas calquée sur les besoins des populations.

Ainsi des entreprises qui vendent le même type de produit ont toutes intérêt à
en vendre un maximum.  Cela va les entrainer à produire plus de marchandises
qu'il n'est possible d'en écouler sur le marché : c'est la surproduction.
Perdant l'argent qu'elles ont investi pour produire ces marchandises en trop,
les entreprises vont licencier.  Mais en licenciant, elles retirent le revenu
des travailleurs qui auraient pu acheter leurs marchandises, agravant la crise.

Finalement, on se retrouve dans des situations absurdes où des marchandises sont
jetées, alors que les travailleurs appauvris par la crise auraient pu les
consommer mais n'ont plus les moyens de les acheter !



2) La socialisation croissante du processus de la production

La production est collective, et à une échelle bien plus grande qu'à l'époque de
Marx.  Les grands groupes industriels sont implantés partout dans le monde, on
peut citer par exemple l'entreprise Renault, qui possède des usines dans 39
pays, Electrolux dans 16 pays ou encore Bosch dans 50 pays.  La division du
travail est telle qu'aucun travailleur ne peut dire "ceci est l'objet que j'ai
fabriqué".  Mais il y a un antagonisme profond entre cette production collective
des travailleurs et l'accaparement individuel du profit par le patron.  On
pourrait même dire que la production est de plus en plus collective et
l'accaparement de plus en plus individuel.

3) L’organisation et la conscience de classe du prolétariat

À l'époque de Karl Marx ou de Rosa Luxemburg la conscience de classe du
prolétariat était indubitablement sur sa pente ascendante.  Depuis, elle a connu
des hauts et des bas, et les partis politiques, réformistes ou révolutionnaires,
se revendiquant ouvertement de la classe ouvrière ont perdu en poids politique.
Cependant, les liens entre les travailleurs ne pourront jamais disparaître : ils
se retrouvent tous les jours au boulot, ont les mêmes conditions de vie et de
travail et sont exploités par les mêmes patrons.  D'ailleurs, dans des secteurs
aussi récent que celui des jeux vidéos, on a pu voir se former des syndicats.






## Aujourd'hui une autre période, mais d'autres conclusions ?

- Classe ouvrière en France et à l'internationale

Aujourd'hui, force est de constater que les idées marxistes ne sont plus
largement répandues, comme elles pouvaient l'être au début du 19ème siècle.

La faute en partie à plusieurs régimes politiques peu enviables qui s'en sont
revendiqué, à commencer par celui de Staline et ses successeurs.

De plus, des décennies de partis politiques aux directions réformistes (PCF, PS,
etc, pour la France) ont brouillé les pistes et participer à faire reculer la
conscience de classe.  Ceux-ci ont peu à peu abandonné le vocabulaire de la
lutte des classes, en s'adressant à des citoyens et non à des travailleurs, ou
encore en se rangeant directement du côté de la bourgeoisie française en
exhortant à "produire français".


Pour autant, les idées marxistes sont autant d'actualité qu'elles l'étaient à
l'époque de Marx et Engels.

On essaye de nous vendre un monde où les antagonismes de classes auraient
disparus.  Il y auraient toujours des "pauvres" et des "riches", mais pas
vraiment de conflits entre deux pôles de la société, plutôt un aplanissement en
une large classe moyenne.  D'ailleurs il n'y auraient plus non plus de
prolétariat, et tant qu'à faire, plus de bourgeoisie non plus, juste quelques
entrepreneurs doués qui ont mieux réussi que les autres.


Pourtant il y en a des millions des vendeuses, ouvriers, techniciennes,
soignants, etc, rien qu'en France, à être indispensable au fonctionnement de la
société, tandis que seule une poignée d'actionnaires et de grands patrons
prennent les décisions sur les conditions de vie et de travail de toute la
population.

Dans un rapport sur les inégalités mondiales, parus récemment, l'École
d'économie de Paris estime que la part du gâteau perçues par les 10% les plus
riches serait passée, entre 1980 et 2016, de 32 à 37 pourcents en Europe, et 35
à 47 pourcents aux États-Unis.  On est donc loin du ruisselement.

Par ailleurs, si aujourd'hui encore les patrons de Michelin ou General
Electrics, pour ne citer qu'eux, décident de fermer des usines selon leurs bon
vouloirs, au détriment des travailleurs, c'est bien que le rapport à la
propriété n'a pas changé.  Ce rapport à la propriété permet toujours à la
bourgeoisie de s'accaparer les richesses produites par les travailleurs et de
décider de comment le faire.

Les antagonismes de classe sont donc les mêmes qu'à l'époque de Marx et Engels.




## Conclusion

- Socialisme utopique aujourd'hui

Aujourd'hui, le socialisme utopique n'a pas disparu.  On retrouve chez certains,
en effet, un constat des inégalités, des guerres entre les peuples, etc, et une
envie d'y mettre fin.  Mais, comme pour les socialistes utopiques du 19ème
siècle, ils ne poussent pas l'analyse de la société jusqu'à la lutte des
classes.  Il ne suffit pas d'avoir de bonnes idées pour changer la société, il
faut que ces idées soient portées par des forces sociales capables de les mettre
en oeuvre, et qui ont intérêt à le faire.

- Avoir une grille de lecture : des situations toutes différentes mais toutes
  régies par les mêmes mécanismes

La grille de lecture marxiste est souvent dénoncée comme étant trop
"vieillotte", mais les mécanismes d'exploitation inhérents au capitalisme n'ont
pas évolué.  Les classes sociales engendrées par ce mode de production n'ont pas
non plus évolué, l'une possède les moyens de production et l'autre ne possède
que sa force de travail.

- Importance de ne pas avoir une attitude passive

Enfin, comme l'a montré l'Histoire à plusieurs reprises, de la Commune de Paris
au stalinisme, même appuyée par des milliers de travailleurs prêts à en
découdre, une révolution peut aller dans le mur.  D'où l'importance d'avoir une
méthode pour analyser l'Histoire et en tirer des conclusions pour les
révolutions d'aujourd'hui et de demain.


### Sources

_Socialisme utopique et socialisme scientifique_, Engels

_Les Trois Sources du Marxisme_, Kautsky

Renault prduction dans 39 pays : https://group.renault.com/groupe/implantations/nos-implantations-industrielles/

Bosch production dans 50 pays : https://fr.wikipedia.org/wiki/Bosch_(entreprise)

Electrolux production dans 16 pays : https://fr.wikipedia.org/wiki/Electrolux#Sites_de_production

Les inégalités mondiales s'aggravent : https://www.lutte-ouvriere.org/breves/inegalites-une-aggravation-mondiale-126730.html

La classe ouvrière aujourd'hui : https://www.lutte-ouvriere.org/qui-sommes-nous/classe-ouvriere
