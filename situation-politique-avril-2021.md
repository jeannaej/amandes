# TOPO situation politique en France, Jussieu-16 avril

## Crise sanitaire

A.  Des restrictions sanitaire sucré-salé

Ces temps-ci, l’actualité concernant la crise sanitaire semble être un festin d’anecdotes croustillantes pour les journalistes, mais dont le goût amer reste en travers de la gorge des travailleurs.

En effet, il semblerait  que tout le monde n’ai pas le droit au même régime face aux restrictions sanitaires. Rappelons que depuis plus de 6 mois l’ensemble de la population est soumise à des restrictions de déplacements, à un couvre-feu et que les restaurateurs et propriétaires de bars coulent peu à peu.
Mais c’est sans compter sur le bon esprit des patrons, magistrats, politiciens ou encore policiers dont la fonction ou la fortune garantit  apparemment un passe-droit moral: diners aux prix exorbitants, fêtes clandestines, rassemblement sans port du masque, tout ça au coeur de grands appartements Haussemaniens.

Le gouvernement a en effet oublié quelques précisions quant aux restrictions sanitaires: la loi ne s’applique plus dès lors que votre logement dépasse le 200m2 et que vous êtes éligible à l’impôt sur la fortune, parce que c’est bien connu, l’argent bloque la progression du virus.

Au contraire, la bourgeoisie n’hésite pas à envoyer sa police intervenir contre les restaurants servant derrière le rideau, ou contre des barbecues en plein air entre voisins comme à Saint Denis, afin de renvoyer les classes populaires chez eux dans leurs appartements exigus pour qu’ils reprennent bien les transports en communs bondés le lendemain, et fissa!

Tout ça est soupoudré d'une moralisation infantilisante de la population appelée à exercer une responsabilité individuelle pour soutenir les profits du patronat, car le gouvernement, à force de demi-mesures, a clairement présenté le menu: pas de répit pour les salariés, invités à vivre une demi-vie sans saveur pour pouvoir continuer à travailler et faire tourner la machine à profit, bien que les deux tiers des contaminations continuent à se faire dans les transports en commun et sur les lieux de travail.

B.  Le plan vaccinal à la traîne

En effet, on aimerait qu’ils déploient autant d’énergie à mettre en place des solutions, qui existent bel et bien, contre l’épidémie. Or la vaccination est encore à la traîne, et la logique capitaliste en recherche de profit n’y est pas pour rien. La rapidité de production des vaccins est pourtant notable, grâce notamment à une réorientation de toutes les recherches et de colossaux investissement publics. Ces derniers ont néanmoins résultés en brevets privés, contrôlés par les entreprises pharmaceutiques qui ont donc l'aval sur les Etats et leur population et nous soumettent à leur bon vouloir.

Pas de production suffisante donc, car pas de volonté du gouvernement de mettre en place les moyens nécessaires: embauche et réquisition d'installations chimiques et pharmaceutiques pour mettre en place une chaîne de production et d'approvisionnement efficace. La loi de la concurrence capitaliste et l'appétit financier des actionnaires plutôt que la santé de sa population, l'Etat a ses priorités que la morale ignore!

Même l'OMS, pourtant peu connue pour ses positions divergentes de celles de la bourgeoisie, alerte sur la "faillite morale catastrophique". Car oui, si l'on regarde la situation au niveau mondial, le bilan est encore plus catastrophique. Dix pays s'approprient les trois quarts des doses vaccinales, laissant le reste du monde dans une crise sans fin, dont le virus peut se réjouir puisque l'absence de vaccination généralisée et la circulation du virus dans le monde ne peut conduire qu'à l'apparition de nouveaux variants. Encore une fois, ce sont les pauvres qui vont souffrir le plus longtemps.

L’absence de contrôle démocratique de l’industrie pharmaceutique laisse la population dans le flou, le bec ouvert et attendant les providentielles doses qui n’arrivent toujours pas. Pendant ce temps-là, le budget de l’hôpital ne cesse de reculer, le gouvernement étant plus piqué par l’idée d’engraisser la fortune des actionnaires et le budget des grandes entreprises que par le recrutement des soignants et l’ouverture des lits.

C. L'hôpital en crise

Epuisés, les soignants « bricolent » des solutions, mobilisent les étudiants infirmiers en première ligne après une courte formation en réanimation, invite les patients à quitter le plus vite possible les établissements de santé.
Cependant, en remontant légèrement en arrière, 69 000 places d’hospitalisation à temps complet ont disparu entre 2003 et 2017), 500 postes d’infirmiers vacants dans les établissements de l’AP-H… si la situation sanitaire exacerbe les aberrations qui existe au sein du système public, la crise existait bien avant.

## Les attaques contre le monde du travail

### Licenciements, suppressions de postes : les plans du patronat

Au delà de la situation sanitaire, c'est la situation sociale qui est alarmante.

Pour beaucoup de salariés du public comme du privé, mais aussi beaucoup d'artisans ou de petits commerçants, la situation et les perspectives d'avenir se sont beaucoup dégradées depuis le début de l'épidémie. Mais la crise ne touche pas toutes les couches de la société de la même manière.

Ainsi, la France comptait en 2020 4 nouveaux milliardaires, et on ne peut pas vraiment dire que les grandes fortunes aient été beaucoup impactées par ces derniers mois. Certains ont même trouvé le filon : comme des grands patrons de la livraison, la grande distribution ou l'industrie pharmaceutique qui ont même fait des profits records durant cette dernière année, à l'image de Jeff Bezoz, PDG d'Amazon, qui a dépassé la barrière des 200 milliards de dollars.

Cette crise aura en plus permis au patronat d'accélerer des restructurations déjà prévues avant l'épidémie.

À La Poste, les salariés font face à des fermetures de bureaux, des tournées de plus en plus éprouvantes pour les facteurs et factrices, ...

À Renault, dans le secteur automobile, l'annonce des 15 000 suppressions de postes arrivent dans un contexte où la boîte essaye de se tailler une place dans le marché des voitures électriques et des voitures autonomes (pas grand chose à voir donc avec l'épidémie).

Chez Sanofi, à Total, à Cargill, dans l'aéronautique, la grande distribution, le tourisme, la liste est longue des entreprises qui imposent des suppressions de postes ou des plans de licenciements (que ce soit sous forme de départ dits "volontaires", de rupture conventionnelle collective, de plan de sauvegarde de l'emploi, ...). En 2020, 360 000 emplois salariés ont été détruit dans le secteur privé, et des centaines de milliers d'autres sont menacés, y compris dans le public. Dans notre société, avoir un emploi est vital pour s'acheter à manger, se loger, payer ses factures : le perdre ou rentrer sur un marché du travail saturé, c'est la garantie d'années de galères, de drames personnels ou familiaux, etc.

Et c'est dans cette situation que le gouvernement choisit de s'attaquer encore plus aux chômeurs et futurs chômeurs avec une nouvelle réforme de l'assurance chômage. Une réforme qui va diminuer le montant des allocations chômages et durcir les conditions pour les obtenir.

Le gouvernement et le MEDEF veulent également s'attaquer à la durée du temps de travail, nous expliquant qu'il faudra se serrer la ceinture pour faire face à la crise. Et c'est un chantage à l'emploi permanent.

Non seulement le patronat fait la guerre aux travailleurs, mais l'Etat fait passer des lois pour lui faciliter la tâche, que ça soit par la mise en place de lois contre le code du travail (loi travail en 2016, ordonnances Macron en 2017), la réforme de l'assurance chômage, la réforme des retraites, qui est toujours dans les tirroirs, etc. Mais ce n'est pas une coïncidence, et tous les gouvernements, de Mitterand à Macron, en passant par De Gaulle et tous les autres, se sont rangés dans le camps des capitalistes.

### Pourtant le patronat est arrosé d'argent publique

C'est ce même Etat qui a débloqué au printemps dernier 300 milliards d'euros en cadeaux aux grands patrons (sous formes de prêts garanties par l'Etat). Il faut y rajouter les dizaines de milliards sous forme de subventions directes aux entreprises, rien que depuis un an ! Tout ça sans contreparties évidemment, car parmi les entreprises bénéficiaires, on en trouve, comme Renault et Airbus, qui suppriment des postes !

Rajoutons encore le dispositif de chômage partiel : dans certaines entreprises, 84% des salaires sont payés par l'Etat et non pas l'entreprise ! Ainsi, beaucoup de grandes boîtes en profitent pour faire payer des salaires directement à l'Etat, donc à la population, en fraudant allègrement d'ailleurs. Un vrai racket. Bref, toutes ces mesures sont autant de façons de faire payer aux travailleurs la continuité des profits du grand patronnat.

Il y a un an Macron affirmait que le gouvernement mettrait tout en place pour sauver des vies "quoi qu'il en coûte". Mais il fallait comprendre "quoi qu'il en coûte pour les classes populaires".

### Atonie des directions syndicales

Du côté des directions syndicales, l'ambiance est au mieux à l'accompagnement. Pas de dates de mobilisation interpro depuis le 4 février, mais des accords signés à tour de bras, à l'instar d'un accord sur le chômage partiel à la SNCF, signé jusqu'au syndicat Sud Rail, qui se donne pourtant des airs combatifs dans les grèves de cheminots. La direction de la CGT, la centrale syndicale qui reste la plus puissante aujourd'hui, se perd dans ses plans de réindustrialisation qui sont bien plus des gages au patronat français qu'un véritable plan de bataille pour les travailleurs. L'urgent ce n'est pas d'adresser aux patrons en proposant une façon crédible, chiffres à l'appui, de réindustrialiser le pays, mais c'est de défendre nos intérêts, d'interdire les licenciements, d'imposer des titularisation de postes et des augmentations de salaires. Il n'y a pas de raison que ça soit aux travailleurs et aux pauvres de payer la crise alors que l'argent coule à flot pour une partie de la société.

### Quelques réactions collectives : Appel des TUI, coordonner les luttes pour rompre avec l'isolement

Ces derniers mois, on a pu voir des réactions collectives : à la SNCF, à la Poste, à Total, dans l'ingénierie automobile, mais aussi dans l'éducation nationale et le milieu de la culture.

Encore difficile cependant d'embrayer sur un mouvement d'ampleur et de sortir de l'isolement site par site, entreprise par entreprise. C'est pourtant ce qu'il faudra faire si on veut engager un vrai bras de fer.

C'est le sens qu'a pris l'appel des salariés de TUI, une société de voyage et de tourisme, qui se battent contre un plan de licenciement qui touche deux tiers de leurs effectifs. Cet appel était censé s'adresser à l'ensemble des travailleurs qui voulaient se battre et coordonner leurs luttes.
Ils ont ainsi pu organiser une manif en janvier regroupant un certains nombre de salariés de différents secteurs en lutte.


## Situation de la jeunesse

A. Une précarité accrue des jeunes

Et c’est aussi le cas pour la jeunesse.

Jeunes chômeurs, jeunes travailleurs précaires, ils sont des milliers à attendre à la soupe populaire le soir.
Pas question d'attendre de réponse de la part de l'exécutif, qui, face à la proposition d'un "minimum jeunesse", ne laisse même pas porte ouverte au débat. Bruno Le Maire a bien illustré le fond de pensée de la majorité, en répondant à la proposition d'élargissement du RSA, pourtant déjà bien insuffisant, au moins de 25 ans: « À 18 ans, ce qu’on veut, c’est un travail. On veut une rémunération de son travail, on ne veut pas une allocation ».
A 18 ans, on veut donc apparemment un travail, mais cette affirmation enflammée ne semble étrangement pas s'appliquer lorsqu'on bourgeois, et qu'on accède donc à L'Ecole Normale, ou encore à l'ENA, où l'on est rémunéré autour de 1700 euros pendant ses études,  comme une majorité de députés.
Le gouvernement préférera donc encourager les jeunes à l'emploi, avec par exemple le brillant plan "un jeune une solution" qui, sous couvert d'intentions bienveillantes, est en fait un moyen d'offrir gracieusement aux patrons le salaire des apprentis et alternants déjà payés en dessous du SMIC, tout ça en piochant dans l'argent public.
D'autant plus qu'un étudiant sur deux travaille déjà pour payer ses études- et ne bénéficie que d'aides ponctuelles et dérisoires, à la hauteur de l'insuffisance des échelons boursiers. Alors qu’ils forment un tiers des emplois des 18-24 ans, les contrats courts, l’intérim et les autres types de contrats précaires ont été les premiers touchés, notamment dans la restauration, la culture ou les services. Ils ne touchent d'ailleurs pas souvent le chômage, car Pôle emploi n'est pas tendre avec ceux qui travaillent peu ou qui cumulent avec une bourse. Sans compter que la catastrophe est déjà engrangé en amont, avec la sélection scolaire : la possibilité de faire ses études est encore aujourd'hui dépendante du financement des parents, faute  d'aide suffisante de l'Etat, creusant les inégalités entre enfants de cadres et enfants d'ouvriers. Voilà donc la méritocratie.

Etudiants, qui, en plus d'être précaires, n’ont pas vu la couleur de leur fac depuis 2020, malgré la fameuse annonce du jour en présentiel par semaine du gouvernement. Rouvrir les facs dans des conditions sanitaires acceptables serait pourtant bien possible, avec un budget pour la ventilation et la purification d’air, ainsi que les locaux nécessaire pour garantir la distanciation sociale.

Mais encore une fois, ce n’est pas là une priorité du gouvernement, qui préfère attaquer l’Université déjà au bout du rouleau sans relâche: attaques contre l’UNEF, dénonciation d’un « islamogauchisme » menaçant au sein des départements de recherches, la chasse à la sorcière touche tous les niveaux de la fac, pendant que les chiffres de décrochages, suicide et dépression grimpent, en silence. Rappelons que le chèque psy auréolé de Frédéric Vidal ne contient en réalité que 3 séances, comme si la santé mentale pouvait être rentabilisé en terme de temps, et ne démarre que très mal, car peu de psychologues ont accepté de participer pour être payé une somme dérisoire.



## Écologie

A. L'hypocrisie climatique dénoncée par la jeunesse

Ce sont pourtant seulement ces mêmes jeunes qui trouvent le courage de dénoncer la crise climatique, rassemblant des milliers de manifestants le dimanche 28 mars pour dénoncer l'hypocrisie du gouvernement.  En effet, cette génération qui vit dans l’angoisse du dérèglement climatique et dans la colère face à l’inaction des grandes puissances et aux réponses des grandes entreprises, n’a pas supporté l’hypocrisie de la très attendue loi climat proposée par Emmanuel Macron. Supposée être une forme inédite de dialogue social en réaction aux marches pour le climat notamment, il s’agit encore une fois d’une lourde mascarade. Les 150 citoyens tirés au sort pour élaborer des propositions ont bien sûr été lourdement encadrés par « les plus grands experts de la question climatique » choisis avec soin par le gouvernement dans lesquels on retrouve des lobbyistes ou encore des PDG d’entreprises comme Areva ou Aéroports de Paris. Au final, peu importe, puisque 7 propositions sur 8 ont été allégées, voir totalement supprimées et que c’est bien sûr le gouvernement qui a rédigé le projet de lois.

B.  Lien écologiques avec la crise sociale et la crise sanitaire

D'ailleurs, si l'objectif principal des capitalistes n'est pas de régler la crise climatique, ils ont tendance à oublier le que leur priorité n°1, la crise sanitaire, n'est qu'un symptôme de leur méthode de production et d'exploitation. La quête de profit produit le risque épidémique et le fait grandir de jour en jour.

En très bref et en effleurant à peine le sujet, les espèces grands vecteurs d'agents pathogènes supportent en soi très bien leur cohabitation avec les virus par des stratagèmes naturels (température élevée, production rapide d'anticorps...).

Mais lorsque l'ogre du capital vient s'en prendre à leur habitat, que ce soit par la destruction de leur état naturel par la déforestation ou encore la modification de leur conditions de vies du fait de la pollution de l'air et des sols, des facteur comme leur rapprochement géographiques avec d'autres espèces ou le stress engendré interviennent dans leur gestion des agents pathogènes, qui se réfugient alors sur un porteur plus propices, d'une autre espèce, et ainsi de suite, jusqu'à... atteindre l'homme.

Le marché noirs des animaux sauvages, que ce soit pour satisfaire la fine bouche des bourgeois des pays riches en recherche d'exotisme ou pour marquer le prestige des riches qui font parader leur compagnons de vie rugissants participe d'autant plus à ce déplacement d'espèces et aux voyages, avec elles, des virus.

L'élevage intensif, qui concentre dans des conditions désastreuses des espèces dénaturées et au système immunitaire affaibli destinées à l'alimentation, augmente également la rapidité de la propagation du virus, qui, s'il trouve en un animal d'élevage un hôte accueillant, a tôt fait de contaminer l'exploitation entière et donc l'homme sur tous les continents sur laquelle la viande sera importée. Le virus Nipah, apparu en Malaisie en 1998, a par exemple découlé du "saut d'espèce" de la chauve souris à une exploitation industrielle de porcs, dont la viande ont contaminés la Malaisie et Singapour.  Le SRAS, deuxième coronavirus le plus célèbre après le COVID 19, est quand à lui apparu chez l'homme suite à la présence de civettes vendues comme aliments sur des marchés de la province du Guangdong (Chine) et infectées par le syndrome respiratoire aigu sévère.

Mini Conclu

Pour résumer, il parait bien vain d'espérer allier système capitaliste et solutions à la crise écologique, deux notions qui dans la même phrase font figure d'oxymore. La doctrine productiviste du système et la course au profit qui fait son essence sont les causes même de dérèglement climatique, alors comment penser qu'une résolution de la crise puissent être initiées par ses acteurs même, les grandes entreprises et les appareils d'Etat à leur solde? Non, la réponse ne se trouve pas dans des changements par voie électorales, car quand bien même ces derniers seraient pavés des meilleures intentions du monde, leur application ne sera jamais satisfaisante. L'urgence est dans la défense par la population de ses propres intérêts, et pour cela elle ne peut compter que sur elle-même. Le véritable changement climatique ne peut se faire sous le capitalisme, c'est pourquoi la meilleure solution reste dans la réappropriation des moyens de productions et de décisions, afin d'agir ensemble directement sur les causes de la crise climatique: production de masse, émission de gaz à effet de serre du aux transports à outrance. Si nous ne voulons pas vivre sur "une planète enfiévrée habitée par des gens fièvreux", il faut des méthodes révolutionnaires.



### La gauche de gouvernement

Réorganiser la production, réorganiser la société, ça semble être une tâche titanesque.
Face à la hauteur et aux enjeux, certains se tournent vers les partis de gauche, ceux qui prétendent qu'en votant pour eux tout irait mieux : ils abrogeraient les lois nuisibles, ils créeraient des emplois écolo, diminueraient les inégalités, etc.
Bon, heureusement on peut se faire un avis sur la faisabilité de leurs programmes politiques sans avoir à voter pour eux aux prochaines élections.

On a déjà parlé plus tôt des centaines de milliards d'euros de cadeaux aux grandes entreprises, sous forme de prêts garanties par l'Etat et de subventions. Eh bien ces milliards ont été voté à l'assemblée nationale en mars 2020 par tous les députés présents, y compris ceux de la France Insoumise et du Parti Communiste.
Quand il s'agit de sauver l'économie ils votent tous ensemble malgré les divergences.

Qu'attendre de ces partis politiques qui nous promettent qu'une fois au pouvoir eux ils feraient différemment ?
N'est-ce-pas ce que Hollande, et Mitterand avant lui, avaient promis ? C'est ce que promettaient également des formations politiques comme Syriza en Grèce et Podemos en Espagne il y a quelques années.

À chaque fois, les politiques menées par ces partis une fois au pouvoir n'ont rien changé pour les classes populaires. Pire, cela a démoralisé une grande partie de la population qui mettait leurs espoirs dans l'élection de tel ou tel sauveur auto-proclamé. Il faut dire que ces formations politiques savent habilement détourner la protestation de rue vers le terrain électoral. À chaque fois les appels aux votes sont les mêmes : "vous vous êtes bien battu, vous avez bien manifesté, maintenant il faut voter pour nous".

Même avec de bonnes intentions, il n'est pas possible de simplement s'emparer de l'appareil d'Etat pour en faire un outil d'émancipation des travailleurs. L'Etat dans la société capitaliste, c'est un outil utilisé par la classe dirigeante pour maintenir son pouvoir. En effet, l'Etat fournit un arsenal juridiques et une police pour réprimer les pauvres et mater toutes celles et ceux qui relèvent la tête. Pour un réel changement de société, il ne suffit pas de changer de gouvernements ou de majorité parlementaire. De même, changer le numéro de la République, fonder une 6ème ou une 7ème République, ne nous débarrassera pas des inégalités et de l'exploitation.

## Conclusion

Il y a 150 ans, de mars à mai 1871, les classes populaires prenaient le pouvoir à Paris durant quelques semaines et proclamaient la Commune. Ce pouvoir, ils et elles l'ont pris les armes à la main et alors que la bourgeoisie parisienne avait abandonné la ville pour se réfugier à Versailles. En construisant leurs propres organes de décisions, en s'assurant eux-mêmes et elles-mêmes que les décisions prises étaient mises à exécution, en s'armant pour défendre la ville, en réorganisant la société pour répondre aux besoins de la population, les communards et communardes ont créé le premier Etat ouvrier de l'histoire. Un Etat pour la première fois au service de la majorité de la population, une étape pour mettre fin aux inégalités.

Bien sûr la situation a changé depuis, le développement du capitalisme a entraîné d'énormes transformations à travers le monde, à commencer par des guerres (dont deux guerres mondiales), la colonisation et la domination de régions entières du globe, des crises, des famines, ... Ce mode de production apparaît aujourd'hui clairement comme un frein au développement de l'humanité, il détruit la planète et il est urgent de s'en débarrasser. Alors comment faire ?

Eh bien, la Commune de Paris avait ses limites, il ne s'agit pas de refaire tout pareil. Et depuis, il y a eu d'autres moments dans l'histoire où l'édifice capitaliste a vacillé, des expériences où des millions de gens ont montré leur force collective et leur détermination à changer la société. À travers toutes ces expériences, nous sommes convaincus que seule une révolution pourra nous permettre de nous débarrasser de ce système et reléguer l'exploitation et les oppressions au rang de reliques du passé.
