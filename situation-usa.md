# Situation aux États-Unis

## Message d'invitation

Les images de l'assaut du Capitole début janvier, par quelques centaines de militants d'extrême droite, ont de quoi inquiéter. Bien plus que les politiciens de tous bords et les grandes fortunes qui se placent en grands défenseurs de la démocratie, ce sont les millions d'opprimés et d'exploités outre Atlantique qui sont en première ligne face à la montée des courants réactionnaires. L'élection de Biden n'a pas franchement rebattu les cartes de la société américaine, profondement inégalitaire et en proie à une grave crise économique. Et c'est bien cette société qui fait germer les idées les plus nauséabondes dans une partie de la population qui se sent laissée pour compte par les gouvernements successifs.
Mais si la situation est préoccupante, il n'est pas exclu que la classe ouvrière américaine reprenne le chemin de la lutte collective pour son avenir, en sortant de la fausse alternative Républicains/Démocrates !

## L'assaut contre le capitole, un nouveau cap franchi

- Coup de force de l'extrême droite américaine ...

Le 6 janvier dernier, suivant l'appel lancé par Trump lors d'un meeting quelques heures avant, des milliers de militants et sympathisants pro-Trump sont allés manifester devant le Capitole contre le soi-disant vol de l'élection présidentielle par Joe Biden.
Une centaine d'entre eux sont rentrés de force à l'intérieur du bâtiment, lors d'une réunion du Congrès qui devait ratifier l'élection de Biden.

Cet assaut représente un véritable coup de force de l'extrême droite américaine.
Il s'agit pour ces militants de montrer que, peu importe le résultat des élections, ils aspirent au pouvoir : un pouvoir autoritaire, qui s'attaque aux migrants, aux femmes, aux minorités de genre, aux Noirs, et à toutes celles et ceux qui ne se laisseront pas faire.
Proud boys, Oathkeepers, Qanon, néo nazies, et autres groupuscules réactionnaires, ils n'étaient "que" quelques centaines parmi les milliers de manifestants pro-Trump à envahir ce bâtiment emblématique du pouvoir.
Mais l'idéologie et l'ordre social qu'ils veulent imposer peut rapidement trouver de l'écho dans la situation de crise du capitalisme que nous connaissons, et il ne faut pas prendre à la légère de telles actes.
En s'attaquant à un symbole de la démocratie étatsunienne, un avertissement est lancé à tous les travailleurs de ce pays, car il n'y a pas de doutes que ce sont bien eux qui se prendront de plein fouet les restrictions de droits démocratiques et les attaques patronales si le pouvoir venait à se durcir.
La bourgeoisie, quant à elle, garderait sa place bien au chaud à la tête des entreprises et dans l'appareil d'Etat.

- .. dans une période de crise du capitalisme

Si l'extrême droite se sent les coudées franches en ce moment, c'est que nous connaissons une grave crise économique, qui a accru les tensions et les inégalités dans la société.
Une partie de la petite bourgeoisie américaine, boutiquiers, restaurateurs, etc, asphyxiée par le grand capital et menacée de faillite, cherche un débouché politique et se radicalise.
D'autres pans de la population, qui ont l'impression d'être laissés pour compte, se font entrainer par des idées de plus en plus réactionnaires et dangeureuses.
Ces déclassés viennent grossir les rangs de l'extrême droite, allant jusqu'à employer des moyens violents contre les militants du mouvement ouvrier dans les mobilisations récentes.
On a en effet vu des bandes armées s'organiser contre les manifestations du printemps dernier, défendant la police et la propriété privée.

- Forces de l'ordre réactionnaire

Toute aussi inquiétante est la connivence des forces de l'ordre avec ces groupes.
Il y a quelques mois, un tireur s'était infiltré dans une manifestation pour tuer plusieurs participants, avant de quitter les lieux sans être inquiété par la police, qui ne l'arrêtera que plus tard.
Les vidéos prises au capitole il y a 3 semaines montrent clairement que les flics n'ont pas voulu empêché l'assaut.
À Washington DC, la ville dans laquelle la police est sûrement la plus préparée du pays à affronter des manifestants, comment expliquer que quelques centaines de personnes ont pu pénétrer dans un des plus grands lieux de pouvoir américain ? Une simple comparaison avec la gestion du maintien de l'ordre pendant les manifs antiracistes permet de voir que la défense du capitole il y a 3 semaines était une farce.
D'ailleurs la foule émeutière comptait cette fois-ci un certain nombre de policiers et de militaires, en fonction ou retraités, visiblement moins malmenés que les manifestants qui dénonçait les violences policières il y a quelques mois (même si l'émeute a quand même causé plusieurs morts).

## Les élections en novembre dernier n'ont pas chassé les idées de Trump

- Les idées de Trump étaient déjà là avant (histoire de l'ED américaine)

Des propriétaires de champs de coton segrégationnistes à Trump, en passant par le Klu Klux Klan et les fondamentalistes chrétiens, l'extrême droite a toujours prospérée aux États-Unis.
L'idéologie raciste héritée de l'esclavagisme a été utilisée et distillée à grands renforts de propagande patronale dès la fin du 19ème et tout au long du 20ème siècle, pour diviser les travailleurs selon leur couleur de peau et éviter toute solidarité entre pauvres blancs et noirs.
Le racisme contre les mexicains, les irlandais, les italiens, etc, dans cette terre d'émigration, ainsi que le sexisme et autres idées retrogrades ont également été bien utile au patronat afin d'éviter que la colère d'une partie de sa main d'oeuvre ne se transmette au reste de la classe ouvrière.

Des décennies de lutte des Noirs américains ont réussies à imposer des avancées, mais ne sont pas venues à bout du racisme.
Aujourd'hui, Noirs, natifs américains, hispaniques sont toujours entassés dans des ghettos, occupent les jobs les plus précaires, sont surreprésentés dans les prisons, etc.
Pas plus qu'Obama, Biden ne mettra fin au racisme et les idées qui ont porté Trump au pouvoir il y a 4 ans sont toujours bien présentes.

- Bandes armées =/= électeurs de Trump, mais danger du fascisme

Parmi les 74 millions d'électeurs de Trump, peu soutiennent réellement l'idéologie nazie ou ségrégationiste et les bandes armées d'extrême droite ne sont pas encore, heureusement, en mesure d'imposer leurs propres lois et de rivaliser ouvertement avec la police et l'armée.
Mais, si aucune autre alternative ne s'offre à eux, les millions de partisans du président sortant pourraient un jour former un mouvement de masse qui s'attaquerait frontalement au mouvement ouvrier et à la démocratie, tels les mouvements fascistes dans les années 20 et 30 en Europe.


## Les États-Unis sous Trump : idées retrogrades et capitalisme décomplexé

### Montée en puissance de l'extrême droite sous Trump

- Trump et ses idées retrogrades portés au pouvoir

Pour ses électeurs, Trump offre une sorte d’espoir, ou du moins un point d’appui contre un système qui a apporté beaucoup de souffrances dans leurs vies.
Ce système a jeté beaucoup de ces personnes au chômage ou a réduit leurs salaires, augmenté le coût de la vie, réduit leurs pensions, relevé l’âge de la retraite et rendu leur avenir plus incertain.
Pendant des années, la population américaine a vu les mêmes politiciens multiplier les promesses et n’en tenir aucune, tout en soutenant les banques et les grandes entreprises.
Mais au lieu que la colère accumulée s'oriente contre le système capitaliste, Trump l'a détournée vers des boucs émissaires tels que les migrants, les musulmans, la Chine, etc.

Ses appels incessants pour construire un mur à la frontière avec le Mexique, ses insultes à l’encontre des migrants qui tentent de venir aux États-Unis ont alimenté un sentiment profondément nationaliste parmi ceux qui le soutiennent.
Mais au delà des insultes il y a toute une politique menée par l'ICE (Service de l’immigration et des contrôles douaniers) de retention et d'expulsion de migrants.
Sous Obama déjà, de 2009 à 2012, l’ICE expulsait plus de 385 000 migrants par an. Tombé à moins de 250 000 entre 2015 et 2016 et encore moins en 2017 sous Trump, le nombre des expulsions est remonté à plus de 250 000 en 2018 et à 282 242 rien que pour la première moitié de l’année 2019.

L’ICE fait des rafles devant des écoles, dans des centres commerciaux, ou encore à la sortie des tribunaux.
Les rafles se sont aussi concentrées sur les militants et les travailleurs sans papiers.

Du côté du Parti Démocrate, on critique mais on vote les budgets accordés à l'ICE !

La classe dirigeante aux États-Unis comprend bien que le nombre de personnes forcées de quitter leur pays pour se rendre dans le sud des États-Unis – ou sur d’autres continents – ne va pas cesser d’augmenter.
De toute façon, les entreprises américaines ont besoin de cette main d’œuvre plus facile à exploiter et qui contribue à faire pression à la baisse sur le niveau général des salaires et sur les conditions de travail.
Mais ils préfèrent éviter les flux non contrôlés de migrants.
Pour y parvenir, les politiciens qui représentent les intérêts des dirigeants capitalistes sont prêts à mettre en œuvre de nouveaux contrôles inhumains, à durcir et militariser la frontière, et à lancer des rafles de l’ICE partout sur le territoire.

Mais au delà de son discours xénophobe, Trump a également coché à peu près toutes les cases du parfait réactionnaire.
Il a défendu l'interdiction de l'avortement, et, en réduisant les budgets au niveau des États, il en a limité l'accès de façon drastique.
On a pu également observé une hausse des attaques et des meurtres de personnes transgenres, encouragés par plus de 50 décrets anti-LGBTI qui ont été inclus dans l’arsenal législatif de 19 États en 2017.
Les positions de Trump contre la présence de personnes transgenres dans l’armée envoient un message fort à l’adresse de tous les bigots et, en sens inverse, aux victimes de plus en plus nombreuses du climat réactionnaire qu’il encourage.

Il a également donné dans le climatoscepticsisme à de nombreuses reprises, n'hésitant pas à démantèler diverses agences consacrées à la protection, à la recherche et au développement de politiques environnementales.
Et que dire de sa réaction face au Covid-19 et à tous ses mensonges ?

- Make capitalism great again ? Protectionnisme, ...

Durant ses années au pouvoir, Trump n'a eu de cesse de mener une politique qui enrichit les riches.
Son administration a tenté d'imposer des coupes budgétaires dans les programmes d’assistance aux pauvres : accès aux soins, à l’aide alimentaire, à l’aide aux dépenses pour le logement et le chauffage ; allocations handicapé ; aide pour les prêts étudiants, etc.
Déjà avant la crise du Covid, les inégalités dans la société américaine avaient explosé.
En 2018, les 400 personnes les plus riches des États-Unis possédaient une richesse totale de 2 890 milliards de dollars, plus que les richesses combinées des 64 % les plus pauvres de la population du pays.

Pour simplement s’en sortir, les travailleurs doivent s’endetter massivement.
En 2019, la dette totale des ménages était la plus haute jamais enregistrée : 13 300 milliards, soit 618 milliards de plus que lors du pic précédent de 2008, juste avant que la crise des subprimes n’éclate.
Toujours en 2019, les États-Unis comptaient 12 pourcents de personnes vivant sous le seuil de pauvreté.
La crise du Covid a, en fait, accentué une situation déjà catastrophique.

En pretendant rendre sa grandeur aux États-Unis, Trump a fait infusé son nationalisme dans une large partie de la population.
Cela s'est concrétisé notamment par le durcissement des frontières et la mise en place de mesures protectionnistes.
D'ailleurs il n'y a pas qu'au États-Unis que le protectionnisme a le vent en poupe.
En France aussi, sous couvert de lutter contre les délocalisations, certains politiciens se positionnent en faveur de telles mesures.
Mélenchon se range parmi ceux-là, mais cherche à s'en distinguer en parlant de "protectionnisme solidaire" !

Pendant la campagne présidentielle, Trump accusait Ford de produire du chômage aux États-Unis en annonçant la fabrication d’une nouvelle voiture au Mexique, dans l’usine de San Luis Potosi de 2 800 salariés.
Il voulait construire un mur douanier à 35 %, pour pénaliser les Toyota, Mazda, Honda, Nissan, Volkswagen, Mercedes ou BMW qui concurrencent les automobiles américaines sur le marché national.
Mais que se passerait-il si l’Allemagne et le Japon, qui investissent aux États-Unis, faisaient pareil et rapatriaient leurs usines ? BMW a 2 usines et 4 centres de recherche sur le territoire américain ; Honda 4 usines ; Toyota 6 usines. Il suffit de calculer combien d’emplois seraient détruits aux États-Unis par la fermeture de 12 usines de production et 4 centres techniques.

Ne pouvant pas se permettre d'appliquer un tel taux de douane, Trump a finalement obtenu que Ford investisse au États-Unis et non pas au Mexique ... en baissant le taux d’imposition sur les sociétés de 35 % à 15 % !
Autant d'économie pour le patronat qui se répercuteront inevitablement en coupes dans les services publiques, les allocations, etc.
Bref protectionnisme ou pas, on sait qui paye la note à la fin.


### Lutter contre le complotisme

Au delà des préjugés racistes, sexistes, homophobes, relayés par Trump, il nous faut également nous attaquer aux idées complotistes propagées entre autre par lui et ses sbires.
Ces idées, largement relayées par l'extrême droite, s'infiltrent dans tous les pores de la société en crise.
Il peut être tentant d'expliquer notre situation par un complot tramé dans l'ombre par l'industrie pharmaceutique, par une organisation secrète, ou encore d'autre groupes restreint d'individus, qui agirait dans le dos de nos dirigeants.

S'il existe bien des lobbies qui influencent les prises de décisions et s'il est bien vrai que la démocratie bourgeoise ne sert pas les interêts de l'immense majorité de la population, méfions nous des explications simplistes qui repose sur une compréhension bancale du capitalisme.

À toutes celles et ceux qui pourraient être attirer par les idées complotistes, il nous faut esquisser notre compréhension de la société capitaliste.
Bien plus que quelques individus dans l'ombre, c'est avant tout une classe sociale, la grande bourgeoisie, qui est aux manettes de la société.
Elle a son gouvernement, sa police, son administration, ses lois, etc, bref c'est elle qui régit l'ogranisation de toute la société.
S'appuyant sur toutes les discriminations pour diviser ceux qui pourraient contester l'ordre social, son idéologie est un frein aux luttes et à la convergence de tous les exploités.

Durant toute l'épidémie, les gouvernements à travers le monde ont mené une politique qui favorise les grands patrons.
Le manque d'investissement dans la santé, les transports, l'éducation, les confinements et les couvres-feux, réduisant la vie des travailleurs au métro-boulot-dodo et en diminuant aux maximum les loisirs et la vie sociale, sont autant de décisions rationnelles du point du vue d'un patronat rapace qui cherche avant tout à maintenir ses profits au plus haut.
Il ne s'agit pas d'un complot, cette situation résulte du fonctionnement du capitalisme.

Nous devons lutter contre les idées complotistes, car en plus de renier les sciences et les faits, en plus de propager le racisme et l'antisémitisme qu'elles contiennent toujours plus ou moins, elles ne peuvent que nous envoyer dans le mur.
Quelles sont les perspectives mises en avant par les adeptes de théorie du complot ?
Certains, à l'instar du très médiatique groupe QAnon, se rangent derrière Trump ou autres gourou richissimes qui s'attaqueraient à un ennemi invisible, une élite, un "Etat profond".
D'autres estiment que nous sommes démunis face à toutes les forces obscures qui tirent les ficelles du monde dans l'ombre.

Plutôt que de s'en remettre à des sauveurs suprêmes ou de sombrer dans la résignation, il faut combattre le problème à la racine : le système capitaliste.

## Combattons l'extrême droite

### Les réponses de notre camp contre l'extrême droite

Mais alors, comment combattre l'extrême-droite ?

Les réactions, souvent faussement radicales, du personnel politique américain ont émaillé les réseaux sociaux dans les heures qui ont suivi l'attaque du Capitole : appels à la révocation de Trump, soutiens à la "plus grande démocratie du monde", etc.
Qu'ils soient républicains ou démocrates, qu'attendre de ceux qui participent à entretenir un système raciste, à s'attaquer aux plus pauvres dans un des pays les plus riches du monde et à semer le chaos aux quatre coin du monde ?
N'attendons pas de solutions miracles venant d'en haut, une procédure d'impeachment contre Trump ne suffira pas à chasser les idées qui l'ont porté au pouvoir, pas plus qu'un congrès ou un bureau ovale repeint au couleur du parti démocrate.
Trump s'en va et c'est tant mieux, mais le nouveau gouvernement continuera de mener une politique dans l'interêt des grands groupes capitalistes qui veulent faire payer la crise aux classes populaires américaines, alimentant le terreau sur lequel l'extrême droite se développe !

Pour lutter efficacement contre les idées réactionnaires, nous ne pouvons pas compter sur les institutions américaines. Elles entretiennent elles-mêmes, par des politiques anti-sociales, les théories du complot, le racisme, le repli identitaire, etc.
Si le parti démocrate apparait aujourd'hui comme une force progressiste (et ce n'est pas compliqué à côté de Trump), voir socialisante, à travers les figures d'Alexandria Ocasio Cortez et Bernie Sanders, il est important de nous démarquer de leur projet politique : détourner les mouvements sociaux de la rue vers les urnes.
Notre réponse doit venir d'en bas, de celles et ceux qui subissent la crise, le racisme et les inégalités. Seule une mobilisation massive des travailleurs et de la jeunesse pourrait imposer une autre société, débarassée des idées nauséabondes portées par les militants qui ont envahi le Capitole.

### Une politique qui suscite des réactions du monde du travail

La politique anti-ouvrière de Trump a également suscité des mouvements de grève à plusieurs reprises.
Chez Verizon 40 000 travailleurs ont lutté contre des licenciements, des délocalisations, la législation du travail, les salaires et avantages spécifiques.
Une branche du géant des télécommunications AT&T a également connu une grève.
En 2019, une série de grèves parmi les enseignants a commencé en Virginie de l’Ouest, avant de s’étendre rapidement aux États où le financement de l’enseignement public est le plus bas des États-Unis.
À des degrés divers, les enseignants de l’Arizona, de l’Oklahoma, du Kentucky, du Colorado et d’autres États encore se sont mobilisés avec les parents d’élèves et ont obtenu quelques améliorations des conditions d’enseignement, ce qui a donné des idées aux enseignants de tout le pays.

### BLM, un mouvement de masse contre les violences policières

- Une mobilisation salutaire contre l'arbitraire policier

Il n'y a pas à regarder loin dans le passé pour chercher des mobilisations massives aux États-Unis.
Le printemps dernier, en réponse à un n-ième crime policier, des millions de manifestants avait envahi les rues de tout le pays.

Cette large mobilisation contre l'arbitraire policier a été amplifiée par la situation économique désastreuse du pays en pleine crise sanitaire et économique.
Quelques semaines avant, les images de files d'attente de plusieurs kilomètres pour l'aide alimentaire avaient beaucoup fait réagir.
Sans chômage partiel, des dizaines de millions de travailleurs se sont retrouvés, en quelques semaines seulement, sans travail, et de nombreux petits patrons ont été menacés de faillite.
De même, l'irresponsabilité de Trump face à la menace du Covid-19, causant des milliers de morts par jour, a rajouté de l'huile sur le feu.

Bref, tous les ingrédients étaient présents pour que la colère contre les violences policières et le racisme se transforme en une colère plus générale contre le système qui les maintient en place.

- Limites institutionnelles, voie de garage électorale

La détermination des manifestants est apparue dés les premiers jours, alors que de nombreux participants s'affrontaient à la police et n'ont pas hésité à bruler des commissariats et à deboulonner des statues d'anciens esclavagistes.
C'est dans ce genre de moment que les millions de travailleurs et de jeunes en lutte peuvent prendre conscience de ce qui les unit et des interêts qu'ils ont en commun.
Le rôle des révolutionnaires dans de tels mouvements est de mettre en avant l'auto-organisation de ces masses en lutte.
Décider démocratiquement quelles actions mener, dans quel but, discuter politique largement, réflechir collectivement à comment s'adresser au delà de nos propres forces, sont autant d'éléments essentiels pour amplifier et structurer la mobilisation.

Cela permet également d'éviter toutes sortes de récupérations.
Et quand il s'agit de récupérer la colère et la détourner vers les élections, on sait que le Parti Démocrate est très efficace !
Fort de son implentation dans les syndicats et le monde associatif, il agit en véritable canalisateur de mouvements sociaux.
Mais de nombreux manifestants ne se sont pas laissés tromper.
Après tout, le mouvement Black Lives Matter a été lancé sous Obama, président démocrate, et la police dans les états et les villes dirigés par des démocrates a réprimé durement les manifestants.
Espérons que la défiance vis-à-vis du Parti Démocrate amène les millions de personnes révoltées par cette société à d'autres solutions que le "vote blue no matter who" (votez bleu (le colère du parti Démocrate) peu importe qui) !


## Conclusion

L'extrême droite dans toutes ses déclinaison est un poison pour les travailleurs.



## Discussion : censure des GAFAM, ce qui pourrait nous inquiéter

https://speakoutsocialists.org/silencing-the-right-wing-who-holds-the-power-of-censorship/

## Idées :

- Inverser l'ordre (Actualité, années Trump, danger réel de cette extrême droite (nos solutions (grèves de 2019, BLM)))
- Ne pas compter sur la démocratie pour se défendre
- https://www.convergencesrevolutionnaires.org/Investiture-de-Biden-La-Maison-Blanche-n-est-pas-devenue-rouge

## Sources

Assaut du capitole :
- SON : https://www.convergencesrevolutionnaires.org/Trump-incite-ses-partisans-a-prendre-d-assaut-le-Capitole?navthem=1
- LO (edito) : https://www.lutte-ouvriere.org/editoriaux/lextreme-droite-au-capitole-un-avertissement-pour-les-travailleurs-153974.html
- TMI : https://www.marxiste.org/international/amerique-du-nord/etat-unis/2821-apres-l-invasion-du-capitole-comment-vaincre-le-trumpisme
- BBC : The 65 days that led to chaos at the Capitol https://www.bbc.com/news/world-us-canada-55592332
- CR (edito) : https://www.convergencesrevolutionnaires.org/La-classe-ouvriere-americaine-Great-again?navthem=1
- SON : Un coup d'État ? https://www.convergencesrevolutionnaires.org/Un-coup-d-Etat-L-Amerique-sait-ce-que-c-est-Pour-le-coup-ca-n-en-etait-pas-un

Années Trump
- CR où vont les USA : https://www.convergencesrevolutionnaires.org/Ou-vont-les-Etats-Unis?navthem=1
- SON : La véritable démocratie ? https://www.convergencesrevolutionnaires.org/Une-election-volee-Nous-n-avons-jamais-eu-de-veritable-democratie
- CR : https://www.convergencesrevolutionnaires.org/L-election-de-Trump-le-point-de-ralliement-de-l-extreme-droite?navthem=1
- CR : https://www.convergencesrevolutionnaires.org/Une-politique-ultra-reactionnaire?navthem=1
- CR : https://www.convergencesrevolutionnaires.org/Tableau-de-l-Amerique-sous-Trump?navthem=1
- CR : https://www.convergencesrevolutionnaires.org/Etats-Unis-la-situation-aux-frontieres-la-barbarie-capitaliste-en-acte?navthem=1
- CR : https://www.convergencesrevolutionnaires.org/La-vague-de-la-greve-des-enseignants-deferle-sur-Oakland?navthem=1
- CR : https://www.convergencesrevolutionnaires.org/Une-politique-anti-ouvriere-qui-suscite-des-reactions?navthem=1
https://www.convergencesrevolutionnaires.org/Trump-Ford-un-exemple-de-protectionnisme-solidaire?navthem=1
https://mensuel.lutte-ouvriere.org/2019/12/15/la-crise-de-leconomie-capitaliste_137974.html
https://journal.lutte-ouvriere.org/2019/08/07/usa-chine-guerre-commerciale-et-monetaire_133729.html
https://mensuel.lutte-ouvriere.org/2018/09/16/trump-et-sa-guerre-commerciale_113296.html
