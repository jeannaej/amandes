---
geometry: margin=4cm
---

# Réformes sociales ou révolution ?

Réformisme : Le réformisme désigne des doctrines politiques visant à améliorer les structures existantes, économiques et sociales, par des modifications progressives des lois plutôt que par une révolution.

Révolution : Changement des institutions politiques et sociales par des moyens radicaux.
On pourrait rajouter qu'il s'agit d'une situation historique où une classe opprimée renverse la classe qui l'opprime.

## I - La social-démocratie allemande à la fin du XIX ème siècle

D'où vient cette question ?
D'où vient cette question ?
En fait, depuis la fin du XIX ème siècle, c'est une question qui a traversé et divisé le mouvement ouvrier organisé.
Pour comprendre un peu mieux l'origine et les enjeux de cette question il faut revenir sur la social-démocratie allemande de la fin du 19e siècle. Car si aujourd'hui on peut parler de mouvement ouvrier organisé dans nos discussions, a cette époque, le sens de cette expression est beaucoup plus évident.
Le SPD (le parti social-démocrate allemand, a l'époque, sociale démocratie veut dire parti révolutionnaire. La distinction entre sociale démocratie comme on l'entend aujourd'hui, réformiste, et parti révolutionnaire, sera justement la conclusion de ce debat.) compte 384 000 membres en 1906, progressant jusqu'à plus d'1 million en 1914. C'est donc un parti de masse du prolétariat, comme il n'en existe pas aujourd'hui.
Les syndicats allemands passent, eux, de 300 000 en 1890 à 2,5 millions de membres en 1913.
La classe ouvrière allemande de cette époque est donc massivement organisée, et on peut facilement imaginer que les choix politiques pris par la direction du SPD revêtent un enjeu primordial.

Par ailleurs, on peut observer le développement des coopératives et les premiers succès électoraux d'un parti qui se donne pour but la poursuite des conquêtes sociales, ce qui est inédit à l'époque.
Cela est dû au fait que le SPD recueille 19.8% des voix aux élections législatives de 1890, et progresse, petit à petit, jusqu'à plus de 34% en 1912 ! Le poids politique de la sociale démocratie est incontestable. L'organisation de la classe ouvrière derrière la sociale démocratie pose un problème de taille a la bourgeoisie allemande qui est forcée de composer avec, chose inédite !

Fort de ses succès électoraux et de son soutien massif dans la population, le SPD semblait inarrêtable. Certains, a la tête du parti, commencèrent a confondre leur influence dans les institutions légales, comme dans les communes ou au parlement, avec le rapport de force réel que peuvent donner un million de prolétaires disciplinés près à se battre pour la révolution socialiste. Ces parlementaires, convaincus de leur importance furent les premiers a  envisager la mise en place du socialisme par la voie légale, en passant par les institutions. 

La propagation de ces pratiques réformistes au sein du parti et des syndicats, avaient besoin d'une base théorique, d'une transition souple, permettant de justifier aux masses revolutionnaires organisées par le SPD, une nouvelle orientation. Le marxisme vieux d'à peine 50 ans sera alors révisé, notamment par Eduard Bernstein.
Selon lui, le capitalisme aurait manifesté une grande capacité d'adaptation.
Il aurait résolu de lui-même ses propres contradictions.
Devenu un système viable pour l'évolution progressive(positive) de la société dans son ensemble (prolétaires et bourgeois compris) Il faudrait alors que le Parti se donne pour tâche d'accomplir le passage progressif au socialisme, bref qu'il mène une politique réformiste.

En effet, pourquoi renverser la bourgeoisie et son état (ses institutions), alors qu'il est indéniable que la sociale démocratie grandie sans cesse et est en capacité d'imposer ses choix par la voix légale. Pour ces révisionnistes du marxisme, oubliée la guerre sans merci que mène la bourgeoisie au prolétariat, aux oubliettes les leçons des révolutions précédente (la commune de Paris), l'heure est à la transition pacifique.

Néammoins, un autre courant dans le Parti va dénoncer cette direction politique, ce qui donnera notamment lieu à l'écriture du livre _Réformes Sociales ou Révolution_, en 1898, par Rosa Luxemburg.
Celle-ci démontre que le capitalisme n'est pas en train de s'adapter, que de nouvelles crises vont se produire et que la classe capitaliste ne va pas se laisser déposséder par la voie instituionnelle.
Elle va réaffirmer que le fonctionnement du capitalisme, basé sur la recherche de profits, entraînera toujours des crises.

Celles-ci proviennent de la contradiction entre la tendance à l'expansion de la production et la capacité restreinte du marché. De la concurrence sans limite et sans remords que se mènent les capitalistes entre eux.
On peut toujours voir ces crises à notre époque, facilitées, entre autre, par le crédit et les spéculations financières.
Par ailleurs, le capitalisme répose sur l'exploitation, ce qui crée une asymétrie entre les exploiteurs et les exploités.

Le changement de société viendra donc des exploités, formant la classe ouvrière, car c'est la partie de la popultaion qui a tout intérêt à ce changement.
C'est pour cela que Rosa Luxemburg réaffirmera dans son livre que le but que doit se fixer le Parti doit être la prise de pouvoir par le prolétariat.
De part les contradictions de classes, il ne peut pas y avoir une politique favorable à la fois à la classe capitaliste et à la classe ouvrière.
Or, dans un monde où règne la sacro sainte propriété privée des moyens de productions, l'État est conçu pour être l'instrument de la classe capitaliste, pour être le garant du respect de cette propriété privée, en douceur (via la justice) ou avec violence, grâce a sa police.
La seule façon pour la classe ouvrière de prendre le pouvoir est de détruire cet État et de le remplacer par le sien. C'est ce changement de classe dominante que l'on appelle Révolution.

Si l'on s'intéresse encore auourd'hui à cette période, c'est parce qu'elle a été riche en événements historiques et en débats théoriques.
Ces débats théoriques sont en fait toujours d'actualité, en particulier le débat entre réformes et révolution.




## II - Une question encore d'actualité

On a pu le retrouver récemment dans les mobilisations en Algérie ou au Soudan, mais on peut se pencher également sur sa place dans le mouvement des Gilets Jaunes.
En effet, il est ressorti, après quelques semaines de mobilisation, une revendication portant sur le fameux Referendum d'Initiative Citoyenne, le RIC.
Il s'agit pour ses défenseurs de mettre en place un referendum en toute matière, déclenché en recueillant quelques centaines de milliers de signatures.
Les partisans du RIC ont bien raison de se sentir dépossedé du pouvoir de décision.
Ce pouvoir est entre les mains des grandes entreprises et du gouvernement, qui n'hésite pas à envoyer sa police reprimer violemment les manifestants.

Pour autant, le RIC a pu faire naître de nombreuses illusions.
Dans un premier temps, sa mise en place aurait permis au gouvernement d'anesthesier la contestation, en faisant rentrer tout le monde chez soi avec la promesse de changements quelques mois ou années plus tard, une fois le RIC mis en place.

En outre, le référendum repose sur la croyance qu'en changeant les lois, on peut changer de société.
En réalité, avec ou sans lois, les capitalistes ont tous les droits : le droit de licencier, d'imposer leurs horaires, leurs conditions de travail.
Estimer pouvoir les contraindre à renoncer à ces droits par la voie légale est naïf.
C'est oublier que la classe capitaliste est organisée derrière ses intérêts, et ne lachera rien tant qu'elle n'y sera pas forcée sous peine de disparaitre.

Et de telles situations ont existés, où de meilleurs conditions de vie ou de travail ont été arrachés aux capitalistes. Et cela est toujours passé par la mobilisation massive des travailleurs.

On peut penser aux grèves et occupations d'usine en juin 1936, ou en mai 1968 en France par exemple.


Ce sont des moments dans lesquels la bourgeoisie a tellement peur de tout perdre, jusqu'à la propriété des usines, qu'elle lâche des augmentations de salaires, des améliorations de conditions de travail, etc.


Par ailleurs, les lois sont souvent contournées par les grandes entreprises (heures non payées, mesures de sécurité insuffisantes, etc).
Faudrait-il compter sur l'État pour faire respecter les lois contraignant les entreprises ?
En réalité, l'État ferme les yeux sur la responsabilité (ou plutôt l'irresponsabilité) des patrons dans les catastrophes industrielles comme Lubrizol, de même qu'il ferme les yeux sur l'évasion fiscale des multinationales, qui volent des milliards aux impôts.



## III - Le rôle de l'État

En fait ce n'est pas une coïncidence si l'État se comporte de cette façon vis-à-vis des entreprises.
Comme nous l'avons vu dans le dernier topo pour celleux qui étaient là, l'État n'est pas au-dessus des classes.
C'est en réalité, un produit de la société de classe, utilisé par la classe dominante pour asseoir son pouvoir sur la classe opprimée.



### Attaque judiciaire contre l'État

Au delà du RIC, on a pu voir dans le mouvement pour le climat, des actions tentées pour contraindre l'État à agir selon la volonté des personnes mobilisés.
On peut penser à "l'affaire du siècle", une pétition qui a recueillit près de 2 millions de signatures, pour tenter une action en justice contre l'État, ou encore Greta Thunberg qui attaque elle-aussi l'État français pour inaction contre le changement climatique.
Encore une fois, malgré leurs aspects relativement inédits et médiatiques, ces actions risquent surtout d'avoir un effet démoralisateur en incitant les gens à rentrer chez eux tout en laissant faire la justice.
De plus, cela perpétue le mythe selon lequel l'État serait au service du plus grand nombre, et que son inaction sur le plan écologique serait le résultat de quelques mauvais choix politiques du gouvernement, quand il s'agit en fait de son fonctionnement normal, à savoir laisser le plus possible les mains libres aux grandes entreprises.



### Mythe de l'État "providence"

Une croyance répandue dans la société est le mythe de l'État "providence", qui aurait pour rôle de garantir à la population certaines conditions de vie, un travail, un certain pouvoir de décision, etc.
Ce serait le "gentil" État contre le grand capital.
Une fois que l'on comprend que l'État n'est pas au-dessus des classes, on comprend que celui-ci n'a de compte à rendre qu'à la classe dominante.
L'État n'est pas au service des chômeurs qui ont de plus en plus de mal à accéder aux aides financières, pas plus qu'il n'est au service de l'ensemble des travailleurs quand il détériore le code du travail, ou qu'il diminue les retraites.
L'État est aux premières lignes quand il s'agit de démanteler les hôpitaux, alors que la majorité des gens s'y opposent et ne votent pas pour ça.



### Mythe des services publics

L'État n'est pas non plus un rempart au capitalisme à travers les entreprises/services publics.
Comme on a pu le voir avec les suicides à France Télécom ou plus récemment dans la colère des cheminots, des postiers, des soignants, les conditions de travail dans ces fameux services publics sont les mêmes que dans le privé.
Par ailleurs, les services publics ne sont jamais acquis, il sont en réalité une cristallisation des rapports de force.
En ce moment, le rapport des force est tel que le gouvernement se sent pousser des ailes en supprimant le statut des cheminots et en intensifiant les cadences à la poste et dans les hôpitaux.
Il faut bien sûr soutenir les luttes pour les services publics, mais ne pas avoir d'illusions : lutter uniquement pour de meilleurs services publics ne débarassera pas la société de l'exploitation.
Même avec de meilleurs réseaux de transports, les trains de la SNCF emmèneront quand même les gens se faire exploiter au travail tout les matins.


## Conclusion

Il ne faut pas renoncer à la lutte pour les réformes sociales, mais si cette lutte n'est pas orientée vers la prise de pouvoir par la classe ouvrière, elle perd tout caractère révolutionnaire.


En fait il s'agit d'utiliser les luttes en faveur des réformes sociales pour faire évoluer la conscience de classe, par exemple en militant pour créer des instances d'organisation "depuis la base" durant ces luttes (tel que des comités de mobilisation ou des comités de grève).

Nous ne nous sommes pas opposés au ric lorsqu'il a été mis sur la table par les gilets jaunes. Nous nous opposions a ceux qui voulaient tout cacher derrière lui. De la même manière, nous nous opposons a ceux qui voient dans les réformes sociales une fin et non un moyen.

Comme l'a dit Rosa Luxemburg il y a près de 120 ans "la lutte pour la réforme est le moyen, la révolution sociale le but".


## Sources

Page wiki de l'histoire de la sociale-démocratie allemande : https://fr.wikipedia.org/wiki/Histoire_de_la_social-d%C3%A9mocratie_allemande#Vers_le_parti_de_masse

L'État et la Révolution, Lénine : https://www.marxists.org/francais/lenin/works/1917/08/er00t.htm

Réformes Sociales ou Révolution, Rosa Luxemburg : https://www.marxists.org/francais/luxembur/works/1898/

Court article de LO sur le RIC : https://journal.lutte-ouvriere.org/2018/12/19/ric-aucun-referendum-ne-remplace-la-mobilisation-des-travailleurs_115791.html

Wikipédia article sur l'État : https://fr.wikipedia.org/wiki/%C3%89tat

Article sur les classes sociales sous le capitalisme : https://sites.google.com/site/theoriecommuniste/les-classes
