# La classe ouvrière, au coeur de l'exploitation capitaliste

## Message d'invitation

Cela fait quelques temps maintenant que le champs lexical de la lutte des
classes a disparu du vocabulaire de la plupart des politiciens, éditorialistes,
mais aussi des militants. On ne parle plus d'exploiteurs et d'exploités, ni de
classe ouvrière, qui est souvent remplacée par "peuple" ou "classe
moyenne". Pourtant l'exploitation capitaliste fait rage sur toute la planète et
celles et ceux qui ne vivent que de leurs travail ont tout intérêt à y mettre un
terme. Y a-t-il encore un sens à parler de classe ouvrière ? Quel programme
politique pour les exploités ?

## Révolution industrielle, développement de la bourgeoisie et du prolétariat

### Chute de l'Ancien Régime, avenement de la bourgeoisie

À la fin du 18ème et au début du 19ème siècle, la Révolution industrielle
entraine des transformations profondes dans de nombreux pays. Ces
transformations sont bien sûr techniques (la vapeur, les progrès dans le textile
et la sidérurgie, etc) mais également économiques et politiques. La bourgeoisie,
jusqu'ici entravée par le clergé et la noblesse, réussit à s'emparer du pouvoir,
comme en France avec la Révolution Française. Elle fait sauter le joug du
féodalisme, organisation sociale devenue obsolète et obtient, entre autre :

- la diminution, voire suppression, des entraves institutionnelles à la libre
  activité des artisans, commerçants, manufacturiers et industriels, en
  particulier le libre achat de la force de travail et la libre vente des
  marchandises et services produits

- la suppression de l’organisation corporative des métiers, et des douanes
  intérieures

Les anciennes normes qui régissaient l'organisation du travail et des échanges
de marchandises étant abolies, on observe un essor rapide de l'industrie dans de
nombreux pays. Dans un nouveau cadre juridique et institutionnel, les forces
productives se développent à grande vitesse, la productivité et la production
sont en forte hausse, et le capitalisme, encore naissant, semble promis à un
avenir radieux... pour ceux qui possèdent les moyens de production.

### Développement du prolétariat

Avec le passage progressif du servage au salariat, une autre classe sociale se
développe et se retrouve bientôt sur le devant de la scène : la classe
ouvrière. Le serf, paysan autrefois lié à un domaine qu'il devait cultiver pour
un seigneur, est désormais poussé à s'employer auprès d'un patron. Quittant sa
condition de serf, il devient un prolétaire. Les prolétaires étaient autrefois
les citoyens les plus pauvres de la société romaine de l'Antiquité.  Dans le
vocabulaire marxiste, ils deviennent de simples travailleurs, qui n'ont que leur
force de travail à vendre pour subsister.

Entrainée par l'immense force centrifuge du capitalisme, une masse toujours plus
grande de paysans quitte donc les champs pour rejoindre les manufactures et les
usines.

C'est cette réorganisation de la production (et donc de la société) qui sera
analysée par des économistes, comme Karl Marx.


## Analyse marxiste, exploitation, classes sociales

Car au coeur de l'analyse marxiste, on retrouve l'idée que, durant toute
l'Histoire humaine, les institutions juridiques et politiques, mais également
les idées (philosophiques, religieuses, etc) sont, en dernière analyse, produit
par les rapports de production et les conditions matérielles d'existence.

Les idées socialistes et communistes ne font pas exceptions à la règle, et elles
sont le produit de rapports de production, c'est-à-dire d'une certaine
organisations sociale de la production : le capitalisme. C'est pourquoi les
idées de Marx et Engels ont été élaborées au moment de la Révolution
industrielle.


### Salaire et profit

Dans ce système, la production est organisée de la façon suivant : le prolétaire
vend sa force de travail à un patron contre un salaire. Ce salaire, fixé par le
patron, correspond à la valeur nécessaire au salarié pour survivre (payer sa
nourriture, son logement, etc), et ainsi, être en mesure de continuer de
travailler. Dans le vocabulaire marxiste, le salaire permet au salarié de
reproduire sa force de travail (se nourrir, se loger) pour pouvoir revenir
travailler le lendemain. Le reste de la valeur que le salarié a créé par son
travail ira dans la poche du patron sous forme de profit.

Ce système est donc bien différent du servage, dans lequel le serf est lié à la
terre qu'il cultive, en partie pour lui-même, et en partie pour un seigneur, une
abbaye, un domaine royal, etc. La démarcation entre le travail qu'il fournit
pour lui, et le travail qu'il fournit pour son seigneur est visible : un serf
peut, par exemple, travailler un certain nombre de jours pour son seigneur, et
travailler pour lui-même le reste de la semaine.
Mais le salarié, lui, n'a pas de délimitation dans son travail entre le moment
où il travaille pour lui-même et le moment où il travaille pour enrichir son
patron.

Le salaire est la cristallisatoin d'un rapport de force : là où les
prolétaires aspirent à de meilleures salaires, leur patron aspire lui à
augmenter ses profits, ce qui revient à rogner sur les salaires. Selon le niveau
des luttes, le niveau de vie d'une population, le salaire oscille donc entre un
minimum vital, et un salaire qui peut paraitre satisfaisant au salarié, par
exemple pour des salariés qui pensent appartenir aux "classes moyennes". Mais
même dans ce cas de figure, le salarié est exploité par son employeur dans la
mesure où son salaire ne reflète jamais la totalité de la valeur
produite... sinon il n'y aurait pas de profit pour l'entreprise.

Au delà de fixer le fonctionnement économique de la société, le mode de
production crée donc une division en classes sociales.

C'est ce que Marx et Engels comprendront à leur époque.

Au milieu du 19ème siècle, ils écriront, dans le Manifeste du Parti Communiste,
que "la société se divise de plus en plus en deux vastes camps ennemis, en deux
grandes classes diamétralement opposées : la bourgeoisie et le prolétariat".

Cette idée sera au coeur des théories socialistes et communistes, jusqu'à
aujourd'hui encore, car loin de nous laisser simples spectateurs de la société
d'exploitation, ces théories nous fournissent des pistes pour nous en émanciper
en la renversant.

### La classe ouvrière est révolutionnaire

Pour accroître la productivité, les travailleurs sont regroupés dans les mêmes
usines, habitent dans les mêmes quartiers et vivent dans les mêmes
conditions. C'est de cette prise de conscience d'une condition commune et
d'intêrets communs que naissent, dés les débuts du capitalisme, des soulevements
et les premières grèves ouvrières pour revendiquer des augmentations de salaires
et la diminution du temps de travail.

Les travailleurs, celles et ceux qui produisent toutes les richesses dans notre
société, ont tout intérêt à renverser ce système dans lequel une minorité
s'accapare le fruit de leur travail. "Le développement de l'industrie, non
seulement accroit le nombre des prolétaires, mais les concentre en masses plus
considérables; la force des prolétaires augmente, et ils en prennent
conscience", écrivait Karl Marx dans le Manifeste du parti communiste en
1848... Est-ce encore valable aujourd'hui ?!

Dans notre courant politique, on continue à penser que la classe ouvrière, par
son rôle essentiel dans la production, est la seule classe capable de renverser
la bourgeoisie et de s'emparer des moyens de production (usines, machines,
etc). C'est ce changement radical de société qui est en germe dans chaque grèves
et occupations d'usine, et c'est ce qui a terrifié le patronat à bien des
moments dans l'Histoire. Et nous espérons connaitre à nouveau ces moments !


## La classe ouvrière aujourd'hui

### La classe ouvrière existe encore, elle est même bien plus développée qu'à l'époque de Marx ...

Aujourd'hui le capitalisme est le mode de production de l'écrasante majorité des
sociétés humaines à l'échelle du globe. Il est donc bien plus étendu qu'au temps
de Marx et Engels, où la révolution industrielle était loin d'avoir touché
encore tous les continents. Partout où est il s'est immiscé, le capitalisme a eu
besoin de main d'oeuvre sous forme de salariés produisant les richesses et
consommant les marchandises. En se développant sur une si grande surface, et en
faisant gonfler sans cesse les rangs du prolétariat par la même occasion, le
capitalisme alimente la source de sa propre destruction. Par exemple, si la
classe ouvrière chinoise, qui compte actuellement des millions de personnes, se
mettait massivement en lutte, les exploiteurs du monde entier auraient un
sérieux problème...


La condition de salarié est aujourd'hui majoritaire : en 2019, selon
l'Organisation Internationale du Travail, plus de 52 pourcents de la population
active mondiale est salariée (le reste constitue les travailleurs à leur compte
(34%), les travailleurs familiaux (11%) et les employeurs (3%)). En 1991, il y a 30 ans, les
salariés ne représentaient "que" 44% (c'est déjà beaucoup) de la population
active mondiale.

En France, on comptait, en 2017, 23,7 millions de salariés pour 26,8 millions
d'emplois.

En plus de cette masse salariale de près de 2 milliards de personnes, il
faudrait rajouter les plus de 170 millions de chômeurs. D'autre part, dans
certains pays, comme l'Inde, de nombreux travailleurs sont rattachés à
l'économie dite "informelle", ce qui ne veut pas dire qu'ils ne puissent pas
jouer un rôle dans les luttes sociales.

Au delà de l'aspect quantitatif, la classe des prolétaires a également évolué
dans sa composition, ce qui à notre avis ne remet pas en cause sa capacité à
renverser la société actuelle.

### ... malgré ses mutations

Par exemple, il faudrait également rajouter tous les travailleurs et les
travailleuses "uberisées", c'est-à-dire formellement à leur compte, mais de fait
dépendant de grands groupes capitalistes, qui sont obligé par leur employeur de
se déclarer comme indépendants, pour réduire à néant toute protection sociale.

Quant à la tertiarisation de l'économie dont on parle depuis quelques décennies
dans certains pays comme la France, elle n'a pas vraiment rebattu les cartes de
la société de classes. Faisant la part belle à la sous-traitance dans des
boulots autrefois considérés comme des boulots d'ouvriers, la tertiarisation a
souvent externalisé des tâches vers d'autres entreprises.

Ainsi, on peut maintenant voir des employés d'entreprises sous-traitantes
s'occuper du nettoyage des usines et des lieux de travail, tâche autrefois
réalisée par des salariés de la boîte elle-même. Les travailleurs sous-traitants
sont d'ailleurs souvent les premiers à être éjectés quand de grosses boîtes
décident de fermer un site, car leur activité est directement liée à celle des
grands groupes qui font appel à leurs services.

Bien que des classes intérmédiaires, subsistent sous le capitalisme
(paysannerie, petite bourgeoisie), la tendance du capitalisme est à la
prolétarisation des populations. Ainsi, certaines catégories de travailleurs,
comme les ingénieurs dans l'automobile, effectuent un travail répétitif dans une
partie bien spécifique du processus de production, un rôle d'exécutant et pas
vraiment un rôle d'encadrement. Il y a donc actuellement dans les pays
développés une certaine prolétarisation des cadres, concentrés dans de grands
pôles d'activité comme le Technocentre à Renault Guyancourt (10 000 salariés
dans ce centre de recherche Renault) et qui peuvent eux aussi jouer un rôle dans
les mouvements à venir.

### Les luttes de la classe ouvrière continuent

Non seulement la classe ouvrière est toujours bien présente dans les usines, les
bureaux, au volant des transports, dans les mines, sur les chantiers, dans les
entrepots, etc, mais elle est également présente dans la rue, à se battre pour
un monde meilleur. Les soulèvements n'ont pas manqué, rien que dans les
dernières années, au Liban, en Algérie, aux Etats-Unis, en Colombie, ...

En France, la contestation ces dernières années a notamment pris la forme du
mouvement des Gilets Jaunes en hiver 2018 et au début 2019. Les conditions de
vie, et donc les salaires, ainsi que des questions démocratiques, ont fait
sortir dans les rues du pays une foule de personnes, dont beaucoup n'avaient
jamais manifesté. Si, assez vite, les Gilets Jaunes sont apparus comme une
frange de la classe ouvrière, leur action n'a malheureusement pas pris la forme
d'une lutte consciente contre le capitalisme, car elle se déroulait à
l'extérieur des lieux de travail et ne s'en prenait donc pas directement aux
profits de la bourgeoisie, sous la forme de grèves.

Car c'est justement par la grève que cette lutte consciente peut s'incarner,
c'est-à-dire une confrontation directe avec les patrons.

Elle permet de développer la conscience de classe des travailleurs, car leur
rôle de producteurs apparaît alors clairement, en opposition au parasitisme de
la bourgeoisie qui, elle, fait tout son possible pour mettre fin à la grève.

Durant la grève de l'hiver 2019, les travailleurs de la SNCF et la RATP ont
montré que, quand ils ne sont pas là, le réseau de transports en commun est
paralysé. Ils ne représentent qu'une petite partie de la classe ouvrière et on
peut imaginer ce qu'aurait donné une grève suivi plus largement dans le monde du
travail.  D'ailleurs, même si cette grève n'a pas gagné la bras de fer avec le
patronat et le gouvernement, la peur que la grève s'étende a donné des sueurs
froides à nos dirigeants ; à cause, sans doute, de souvenirs amers et finalement
pas si éloignés, de novembre-décembre 1995, où le gouvernement Juppé avait dû
remiser sa réforme des retraites. Ou de périodes plus lointaines, mais présentes
dans la mémoire de la bourgoisie, comme en juin 36 ou en mai 68, des périodes où
des explosions sociales l'ont fait vaciller, et où elle s'est montrée prête à
lâcher des avancées sociales plutôt que de perdre son pouvoir !

## Conclusion

Les travailleurs, aussi nombreux et divers soient-ils à l'échelle mondiale, sont
tous soumis à un même ordre social dans lequel ils sont condamnés à être
exploités. Mais la prise de conscience de leurs intérêts communs donne lieu à
une lutte pour une autre société.

Les notions de "classe moyenne" et de "peuple", qui peuvent avoir un
sens dans certaines situations, empêche de formuler le projet politique que nous
défendons : l'organisation de la classe ouvrière en vue du renversement du
capitalisme. C'est pourtant bien la seule façon de se débarasser de
l'exploitation !


## Sources

- https://www.lutte-ouvriere.org/documents/archives/cercle-leon-trotsky/article/le-proletariat-international-la-14434
- https://www.lutte-ouvriere.org/qui-sommes-nous/classe-ouvriere
- https://d-meeus.be/marxisme/classeO/classeO-EM-JP.html
- https://wikidebats.org/wiki/La_classe_ouvri%C3%A8re_est-elle_la_seule_classe_r%C3%A9volutionnaire_%3F
- https://www.union-communiste.org/fr/1997-11/seule-la-classe-ouvriere-est-une-classe-revolutionnaire-606
- Rapport de l'OIT : https://www.ilo.org/wcmsp5/groups/public/---dgreports/---dcomm/---publ/documents/publication/wcms_713012.pdf
- Nombre de salariés dans le monde (1991- https://data.worldbank.org/indicator/SL.EMP.WORK.ZS
