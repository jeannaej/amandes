## Introduction

Ces dernières semaines, des grèves se sont déclenchées dans des dépôts de bus de la grande couronne parisienne (Melun, Marne-la-Vallée, Saint Gratien, Fontainebleau, ...) et dans d'autres endroits en France (Bretagne, Toulouse, Lyon, ...)
Elles sont la réponse des salariés face à l'ouverture à la concurrence, qui ne fait que tirer leurs conditions de travail vers le bas.
Quels réponses face aux récentes attaques patronnales dans le secteur du transport ?
Viens en discuter avec des militant.e.s du npa jeunes ce mardi 26 à 18h30 à l'ENS !

## Déclenchement
https://www.convergencesrevolutionnaires.org/Ile-de-France-jusqu-a-l-absurde?archive=1

### Pleins d'accords pour entériner des conditions de travail au rabais

Pour aborder la question des conditions de travail dans le secteur du transport routier de voyageurs il faut discuter un peu des différents niveaux d'accord dans des boîtes comme Transdev.
Il y a d'abord la convention collective du transport qui fixe les conditions de travail minimum, mais c'est un minimum très bas !
Un peu au dessus il y a l' "accord socle", négocié entre Transdev et les organisations syndicales représentatives des salariés au niveau de l'IDF. 4 des 6 organisations syndicales ont signé dans le dos des salariés l'accord l'année dernière.
Enfin, il y a les accords locaux, qui sont négociés et signés localement, mais avec une épée de Damocles au dessus de la tête : si un accord n'est pas trouvé entre l'employeur et les délégués syndicaux, c'est l'accord socle, nécessairement moins favorable, qui s'applique.
Avec tout ça on obtient une pyramide d'accord et de conventions, qui sont en réalité de bien faibles garanties quant aux conditions de travail, mais qui crée un mille feuille difficile à comprendre et divise les travailleurs dépôt par dépôt.
Pour rajouter à cette division, Transdev utilise le principe de la clause du grand père, déjà bien connue dans tout un tas d'entreprises comme à la SNCF et la RATP, avec la question du statut.
Cette clause du grand père permet à Transdev d'affirmer qu'il n'y aura pas de perte de salaire pour les anciens embauchés, ce seront uniquement les nouveaux qui feront les frais des baisses de salaires.
Dans les faits ce n'est pas tout à fait vrai, sans rentrer dans les détails c'est dû au fait que la part des primes dans les salaires des conducteurs est très importante.
Mais la direction compte bien jouer sur cette division.

### Appel d'offre, rachat des lots par Transdev

Qu'est-ce-qui a changé récemment ?
La Région IDF est tenue de procéder à des appels d'offres en découpant le réseaux en lots (des bouts de réseau representés par un ou plusieurs dépôts).
Transdev a ainsi remporté 4 appels d'offre : elle reprend les salariés, mais profite de l'appel d'offre pour remettre à plat les conditions de travail

### Les raisons de la colères

L’impact sur les conditions de travail est vertigineux : la disparition des primes ampute les rémunérations de 3 000 à 6 000 euros par an et l’introduction de temps de coupure non payés, seulement partiellement « indemnisés », permet de construire des semaines de plus de quarante heures au travail, sans aucune heure supplémentaire, avec des cadences intensifiées.
Sur la banderole des grévistes de Melun, on peut lire "Non aux 45h payées 35".
Concrètement les temps de pause des chauffeurs qui étaient auparavent payés comme des heures de travail deviennent indemnisés à 50%, 25% voire pas du tout selon le lieu où ils font la pause !
La direction de Transdev veut aussi rogner sur le temps de prise de service, mais aussi sur le temps de pause au terminus entre 2 trajets.
En moins de 10 min, les chauffeurs sont supposer avoir le temps de faire descendre tout le monde, prendre une pause, aller aux toilettes, et faire monter les passagers suivants !
Bref des cadences insupportables qui les poussent à devoir constamment rattraper leur retard.
Et ces économies de bouts de chandelle pour la boîte ça se répercute directement sur les conditions de travail et les salaires, et une fois mises bout à bout ça fait des gros dégats.
Les plannings eux aussi se sont dégradés récemment et ce sont les journées en double vacations qui se sont mises à pulluler.
Une journée en double vacation ça veut dire une journée avec une pause au milieu de la journée entre deux services.
Typiquement tu prends ton service à 7h30 jusqu'à 9h et ensuite tu as une pause de 8h puis tu reprend de 17h à 20h30 par exemple.
Et ces amplitudes peuvent aller jusqu'à 13h30/14h !
Les conducteurs de bus franciliens qui ont subi ces dégradations témoignent tous de la dangerosité de la conduite dans cet état de fatigue.
D'ailleurs ces conditions de travail ont poussé un grand nombre de chauffeurs à se mettre en arrêt maladie, qui sont remplacés par des interimaires.

## Melun en grève puis extension MLV, Fontainebleau, Chelles et en région
https://www.convergencesrevolutionnaires.org/Troisieme-semaine-de-greve-a-Transdev-Ile-de-France-ca-commence-a-faire-tache-d?navthem=1

### Aucun bus ne sort !

Face à cette lame de fond d'attaques patronnales, on a vu une autre lame de fond, c'est la colère des salariés de Transdev.
Une colère venue de la base, bien plus que des syndicats, assez faibles dans la boîte, et qui de toute façon avaient signés pour la plupart les accords locaux qui avalisent les dégradations de conditions de travail.
La grève a donc commencé dans l'agglomération de Melun, Sénart, Fontainebleau, dans le 77 et Saint Gratien dans le 95, puis Marne-la-Vallée, Chelles, Rambouillet, et en région (Bretagne, Toulouse, Lyon, Abbeville, ...), avec des modalités et rythmes de grève parfois très différents mais avec la même volonté de se battre contre ces dégradations.
D'ailleurs certains dépôts se sont mis en grève "préventivement" car ils ne sont pas encore passés à l'appel d'offre mais savent bien à quelle sauce ils vont être mangés dans les prochains mois.

### Auto-organisation ? La grève aux grévistes ?

C'est dans le 77 que la grève a été la plus suivie, parfois de façon reconductible pendant plusieurs semaines durant lesquelles aucun bus ne sortaient.
C'est dans ces moments que les militants révolutionnaires peuvent essayer de proposer une politique aux grévistes qui pourrait permettre d'éviter certains pièges et d'arracher des avancées.
Si personne ne porte une politique pour les grévistes de la base, ceux qui étaient les plus remontés, c'est la politique des bureaucraties syndicales, parfois plus proche de la direction dans son rôle de partenaire social, qui risque d'être prédominante.
Un des enjeux de cette grève à nos yeux était non seulement l'extension mais la coordination des différents dépôts entre eux pour contrer la politique de division de Transdev mais plus généralement du patronnat du transport.
Si l'extension s'est faite de façon presque spontannée au début, tant la colère montait un peu partout, elle n'a pas vraiment étaient prise en charge par les grévistes les plus déterminés quand la grève commençait à durer, de même que la coordination ou plutôt l'absence de coordination était trop souvent organisée par des petits chefs syndicaux locaux.

### Après plusieurs semaines de grèves : négociations, bons et mauvais points

Le problème des attaques patronnales, d'autant plus à l'échelle d'un même groupe comme Transdev, c'est le problème de tous les travailleurs de ce groupe mais aussi plus largement de tout le monde du travail.
Pour lutter contre ces attaques, plus les grévistes sont nombreux et coordonnés, plus ils réussiront à imposer un rapport de force en leur faveur.
Mais s'ils se laissent diviser dépôt par dépôt, c'est Transdev qui aura le rapport de force de son côté, c'est pour ça que la direction a choisi de négocier dépôt par dépôt, faisant mine de ne pas voir le lien entre les revendications similaires des grévistes des différents coins.
Malgré tout, les grévistes de différents dépôts ont réussi a obtenir quelques avancées, grâce à la peur qu'ils ont inspiré à la direction, pour qui un mouvement qui dure voire qui pourrait s'étendre est une vraie épine dans le pied.
Parmi les dépôts qui ont repris le boulot après négociations, ils ont obtenu globalement des primes de fin de grève (pour rattraper une partie des pertes de rémunerations liées à la grève), le payement des temps de pause à 100% (mais ils ne sont pas comptabilisés comme du temps de travail donc ne permettent pas de déclencher des heures supp), quelques faibles avancées sur les plannings, sur les primes et les heures supp, mais rien de comparable à ce qu'ils avaient avant le passage en appel d'offre, donc une victoire en demi teinte.
Cette expérience de lutte sera très bénéfique pour les travailleurs de Transdev et il faudra voir dans les prochains temps si des réactions continuent d'apparaître contre les dégradations de conditions de travail.
En tout cas la direction de Transdev y réfléchira sûrement à deux fois avant de s'attaquer à nouveau aux conducteurs.

## L'ouverture à la concurrence

Calendrier de l'ouverture à la concurrence : https://www.convergencesrevolutionnaires.org/Calendrier-des-sales-coups-et-des-luttes?archive=1
À la SNCF : https://www.convergencesrevolutionnaires.org/Privatisation-de-la-SNCF-declencher-le-frein-d-urgence?archive=1
https://twitter.com/klocrodile/status/1442887905329008645
https://www.convergencesrevolutionnaires.org/Contre-leur-privatisation-capitaliste-nous-serons-300-000-et-plus?archive=1
https://www.convergencesrevolutionnaires.org/Les-transports-publics-terrain-de-jeu-des-capitalistes?archive=1
27 sept https://www.convergencesrevolutionnaires.org/Mouvement-d-ensemble-des-salaries-du-transport-on-vous-dit-pourquoi-c-est-le?navthem=1

La grève à Transdev n'est pas finie partout, même si le boulot a repris dans la plupart des endroits.
Mais plus généralement, cette vague de fond d'attaques patronnales dont je parlais au début va continuer à faire des ravages dans les entreprises du transport, puisque l'ouverture à la concurrence est toujours synonyme de casse des conditions de travail.

Cette ouverture à la concurrence pour l'instant c'est le partage du marché entre 3 géants à capitaux publiques : Transdev (détenue par la Caisse des dépôts), RATP Dev (filliale de RATP) et Keolis (filliale de la SNCF).
En IDF, le processus a tout juste commencé et est prévu pour durer vingt ans : cette année et la suivante les réseaux de bus de la grande couronne (Optile) sont attribués ; fin 2024, c’est le réseau de la petite couronne jusque-là intégré dans la RATP qui sera découpé en lots ; les trains de banlieue de la SNCF (de 2023 à 2033) ; enfin les métros et RER à partir de 2033.
En fait la grève à Transdev n’est que le début d’un combat de longue haleine entre les salariés du transport et les patrons.



## Les services publics sous le capitalisme

CLT services publiques, nationalisations : https://www.lutte-ouvriere.org/documents/archives/cercle-leon-trotsky/article/des-nationalisations-aux-6457

Mais alors comment se battre face à ceux qui détricotent systématiquement les quelques avancées, obtenues souvent de haute lutte, des salariés du transport, Transdev, RATP, SNCF et tous les autres.

En luttant contre Pécresse présidente de région comme le proposent les élus LFI et PCF passés sur les piquets de grève ?
Bien que Pécresse soit une ennemie zélée des travailleurs, c'est lui faire beaucoup d’honneur que de la rendre responsable de la situation.
D’une part parce que l’ouverture à la concurrence des transports en commun est discutée depuis au moins quinze ans à l’Assemblée nationale, au plus haut niveau de l’État.
Sans aucune nuance, que le gouvernement soit de gauche ou de droite.
D’autre part parce que l’ouverture à la concurrence par appels d’offres se pratique depuis longtemps dans bien d’autres régions, dont celles, nombreuses, qui sont ou ont été gérées par la gauche.
Cette gauche, en bonne gestionnaire, n’y a rien vu à redire et a appliqué les « règles » qui entraînent automatiquement le dumping social.

Mais surtout, c'est inverser les rôles. Cette prétendue ouverture à la concurrence en Île-de-France est en réalité faite sur mesure et à la demande des grands groupes de transports comme Transdev.
Ils empochent les investissements de la région qui rachète ou remplace bus et dépôts et les subventions liés aux délégations de service public.
Les trois monopoles du secteur n’ont aucun mal à se partager les réseaux, à se les échanger quelques années plus tard, et à profiter de l’opportunité pour abaisser radicalement les conditions de travail.
Et ce sont eux, pas la région, qui empochent les profits sur le dos des conducteurs d’un côté, des usagers de l’autre.

L’ennemi des travailleurs, ce sont les grands groupes qui les exploitent – et les pouvoirs publics à leur service, au niveau régional ou national.

Lutter pour revenir au bon vieux service public ?

La RATP a beau être une entreprise à capitaux publics (mais pour combien de temps ?), elle n'est pas moins intégrée dans le groupe RATP, véritable multinationale capitaliste aux dizaines de filiales à capitaux privés. Son fleuron, RATP Dev, rafle des marchés de transport public partout en France (Lorient, Angers dernièrement) ou dans le monde (Le Caire, Alger, Londres). Elle sera la grande gagnante de cette ouverture à la concurrence qui lui permettra de se débarrasser des quelques protections dont bénéficient encore certains de ses salariés grâce au statut ou à la réglementation, ainsi que des lignes et dépôts les moins rentables, desservant les quartiers les plus pauvres de Seine-Saint-Denis.

La SNCF se comporte comme n’importe quel patron du chemin de fer, comme ce qu’elle est : une multinationale de 1 200 filiales dans le monde qui veut faire du profit !

En dehors du secteur du transport, une autre entreprise publique, La Poste, n'a pas attendu d'être privatisée pour se comporter comme n'importe quelle autre grand groupe capitaliste : bas salaires, sous-effectif constant, recours massif à l'interim, augmentation des cadences, etc.

Les services publics, nécessaires à la collectivité, doivent être organisés pour répondre aux besoins de la population et non pour rapporter du profit aux actionnaires.
Les privatiser est un recul social.

Mais les conditions de travail sont toujours issues d'un certain rapport de force dans les entreprises et plus généralement dans l'ensemble de la société, entre la classe ouvrière et la bourgeoisie.
Revendiquer de meilleurs services publics et mettre un stop aux projets de privatisations c'est bien se battre directement contre les attaques patronnales.
Mais gardons en tête que ce n'est que par la lutte que nous obtiendrons des avancées, des avancées qui ne sont jamais figées tant que nous resterons sous le capitalisme.

## Conclusion

Un plan de bataille patronnal et une riposte ouvrière, qu'il faudra élargir à tout le secteur
Union des transports publics et ferroviaires (UTP qui regroupe 230 entreprises dont les géants que sont la SNCF et la RATP

Dans le cadre d'un plan plus large du patronnat d'attaques des travailleurs (conditions de travail horribles et bas salaires d'un côté, indemnités chômage de plus en plus faible et difficile d'accès de l'autre)

Les coups d'essais dans la grande courronne sont scrutés de près par les patrons des transports SNCF et RATP

## Lectures

Enjeu écologiqe, contrôle ouvrier de la production

SNCF ouverture à la concurrence
2001 https://www.convergencesrevolutionnaires.org/La-SNCF-et-l-ouverture-a-la-concurrence
2013 https://www.convergencesrevolutionnaires.org/L-ouverture-a-la-concurrence-une-entente-entre-trusts-contre-les-travailleurs
2021 https://www.convergencesrevolutionnaires.org/A-propos-de-l-ouverture-a-la-concurrence-des-trains-voyageurs-concurrence-entre?navthem=1

La Poste
2011 https://www.convergencesrevolutionnaires.org/La-Poste-l-ouverture-a-la-concurrence-un-epouvantail-contre-les-travailleurs
2009 https://www.convergencesrevolutionnaires.org/Non-a-la-privatisation-de-la-Poste

Dossier transports publics 2020
https://www.convergencesrevolutionnaires.org/-Transports-publics-solidarite-ouvriere-contre-concurrence-patronale-

Transdev
19 sept https://www.convergencesrevolutionnaires.org/Secteur-du-transport-routier-de-voyageurs-quelles-revendications-unificatrices
2 oct https://www.convergencesrevolutionnaires.org/Transdev-STBC-les-raisons-de-la-colere?navthem=1
6 oct https://www.convergencesrevolutionnaires.org/Apres-cinq-semaines-la-greve-continue-de-s-etendre-a-Transdev?navthem=1
19 oct https://www.convergencesrevolutionnaires.org/Apres-six-semaines-de-greve-dans-les-transports-publics-en-Ile-de-France-Quelle?navthem=1
