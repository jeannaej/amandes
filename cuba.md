# Cuba et les limites d'une révolution dans un seul pays


## Message d'invitation

Le système éducatif et le système de santé de Cuba sont vantés jusque dans les grandes puissances occidentales. Le régime, mis en place par Castro il y a plus de 60 ans, est aussi loué pour son caractère "socialiste" et pour sa résistance acharnée face à l'impérialisme américain.

L'expérience cubaine a eu le mérite de montrer qu'il était possible de faire face à l'impérialisme de la première puissance mondiale, même quand on est une île de quelques millions d'habitants à quelques encablures de la Floride, mais elle a aussi montré qu'il n'était pas possible de se débarasser des contradictions du capitalisme dans le cadre national.


## Intro

Cuba c'est la plus grande île des Caraïbes, située à seulement 180 km des côtes de la Floride et compte 11 millions d'habitants.

L'Etat cubain est issu de la révolution de 1959, qui a renversé la dictature militaire de Batista, et il a été dirigé depuis par la famille Castro (d'abord Fidel puis Raul) puis depuis 2018 par Miguel Diaz-Canel.
À quel point le régime est-il encore populaire au sein de la population, c'est sûrement difficile à dire -> manifestations en 2021.
Ce qui est sûr c'est que les cubains ont soutenu le régime durant la guerre froide contre les Etats-Unis, contre leur embargo et leurs tentatives d'intimidations et de déstabilisations.
Et si le pouvoir de Castro s'est maintenu depuis tout ce temps, y compris face au géant américain, c'est bien sûr en parti le fait de la propagande omniprésente sur l'île, mais également d'une certaine assise et d'un certain prestige dans la population.

Il n'y a d'ailleurs pas qu'à Cuba que certains aspects du régime sont vantés (tandis que les autres aspects sont bien sûrs passés sous silence).
Le système de santé et d'éducation sont cités en exemple par nombre de militants de gauche et d'extrême-gauche, et il est vrai qu'il est bien plus développé que dans les autres pays d'Amérique latine.
Au début de l'épidémie de covid, on a ainsi pu voir à la télé des médecins et infirmiers cubains envoyés en Italie pour les aider à faire face à leur première vague.
Il y a bien sûr une part de coup de comm' là-dedans, mais il y a aussi une certaine réalité derrière.
Cuba a également été le premier pays d'Amérique latine à développer son propre vaccin contre le covid (je n'ai pas creusé la fiabilité).
De plus, se soigner y est gratuit, ce qui n'est même pas le cas chez son voisin les Etats-Unis, grande puissance économique par excellence !

Quant au système éducatif, de gros efforts ont été fait : depuis les années 1960 la quasi totalité de la jeunesse y est scolarisée et les taux d'analphabétisme sont depuis cette époque comparables à ceux des grandes puissances.

Pendant très longtemps, les inégalités de richesses étaient moindres que dans les pays alentours.
À relativiser toutefois, car cela resultait de la pauvreté générale de l'île : il y avait bien une couche privilégiée mais pas vraiment de classes moyennes, si presque tout le monde est pauvre et que les riches ne sont pas si riches, il n'y a pas tant d'inégalités que ça...
Ceci dit, avec les mutations du système cubain, et le marasme économique duquel le pays ne parvient pas à sortir, les inégalités augmentent depuis quelques temps déjà.

Il n'est pas rare d'entendre dans des discussions entre militants, avec des proches ou plus généralement dans la bouche de gens qu'on essaye de convaincre, que notre politique est similaire à celle menée par Castro, que le régime cubain est le résultat d'une politique que nous aurions nous aussi mené si nous avions été à sa place.
En somme on entend souvent : "ce que vous voulez faire c'est Cuba".

Pas vraiment, et on va voir pourquoi !

## De la dictature à l'avènement du castrisme

### Une ancienne colonie espagnole soumise à l'impérialisme américain

À la fin des années 1950, juste avant la révolution cubaine, 50 à 90 pourcents des actions des compagnies de chemins de fer, électricité, téléphone sont déténus par des américains.
Ce pourcentage est de 40% en ce qui concerne la production de canne à sucre, la principale ressource d'exportation du pays.
Cuba est donc indépendante depuis plus de 50 ans sur le papier, mais largement sous domination de l'impérialisme américain, qui fait la pluie et le beau temps sur l'île.

### Castro monte un groupe armé

En 1952, un coup d'Etat avait mis un militaire, Batista, à la tête du pays.
Il y fera régner une dictature féroce.
Durant les années du régime de Batista, un certain Fidel Castro, un jeune avocat, se distinguera pour son opposition à la dictature, y compris par les armes.
Il fait parti du Mouvement du 26 Juillet, opposé au régime, dont certains dirigeants, notamment ceux installés dans la capitale La Havane, sont farouchement anticommunistes.

Après un premier échec quelques années plus tôt, un emprisonnement et un exil, Castro tente à nouveau sa chance en montant un groupe de guerilleros dans le Sud de l'île, alors que de grosses mobilisations secoue le pays en 1955-1956.
Le groupe met une certain temps à grossir, ils partent de ... 20 !
Ils sont soutenus par une partie de la population locale : des paysans harcelés par le pouvoir, des hors-la-loi, une partie de la bourgeoisie libérale et la jeunesse des villes.
À force d'escarmouches avec le pouvoir et d'attaques de casernes, environ 300 personnes rejoignent au fur-et-à-mesure les rangs de Castro.

### La guérilla progresse

En mars/avril 1958, Castro estime que la situation est mûre et appelle à une grève générale.
Le Mouvement du 26 Juillet, qui voulait s'assurer la victoire pour lui seul, n'y associe même pas le Parti Communiste, une des principales force d'opposition, bien implanté chez les ouvriers, et la grève est un échec.
En fait les dirigeants du Mouvement du 26 Juillet comptaient bien plus sur les classes moyennes que sur la classe ouvrière pour cette grève, à l'image du comité de grève mis en place à la Havane, qui comportait un médecin, un journaliste, le chef de l'Eglise évangélique, un ingénieur et 2 dirigeants du Mouvement du 26 Juillet.
Suite à cet échec, qui reposait sur les épaules des dirigeants de La Havane, Castro prend la tête du Mouvement du 26 Juillet et de ses forces armées.

Batista tente alors d'anéantir la guérilla, en envoyant pas moins de 20 000 soldats contre les ... 200 de Castro !
Mais à force d'enlisement et de démoralisation, l'armée régulière est tenue en échec et doit se replier.
Le régime perd peu à peu son autorité sur certaines zones du pays, et c'est parfois les rebelles qui s'occupent de lever les impôts et réguler les échanges de marchandises et de personnes pour leur compte, en faisant bien attention toutefois à maintenir en place l'ordre et la propriété, selon les mots d'un des dirigeants du Mouvement du 26 Juillet.

### La chute de la dictature : Castro prend le pouvoir

À la fin de l'année, tout se précipite, alors que les hommes d'affaires cubains et américains n'attendent qu'une chose : le départ de Batista.
La CIA tente d'ailleurs de trouver une junte militaire pour remplacer le dictateur.
Batista finit par fuir le 31 décembre 1958, et dés le lendemain des groupes armés, dirigés notamment par Guevarra, s'emparent de points stratégiques.
Une véritable course de vitesse s'engage alors entre le Mouvement du 26 Juillet et les autres organisations politiques d'opposition à la dictature, pour savoir quelle place chacun occupera dans le nouveau régime.

Une grève générale, appelée par Castro met le pays à l'arrêt.
Dans le même temps il fait le tour de l'île, ovationné par les foules en liesses, qui le voient comme celui qui a renversé la dictature.
Il est acclamé et fait la preuve au reste des partis d'opposition que c'est à lui que doit revenir le pouvoir.
Après quelques jours, le 8 janvier, il prend la parole à la télé pour expliquer que tout doit rentrer dans l'ordre, qu'il faut arrêter la grève et rendre les armes au nouveau régime.

### Le nouveau pouvoir recycle l'ancien

La composition du nouvel Etat n'a rien de très effrayant pour la bourgeoisie cubaine et américaine : des politiciens libéraux liés aux partis traditionnels, des arrivistes qui avait rejoint la révolution sur le tard, des anti-communistes, un premier ministre avocat d'affaire et pro-américain, un ministre des finances qui avait déjà occupé un poste clé sous la dictature.
Les frères Castro et Che Guevarra, eux, n'ont pas de ministère mais ont un poids très important dans le nouveau pouvoir vu leur grande popularité.

Quand au reste de l'appareil d'Etat, pas de chamboulement par rapport à ce qu'il était sous Batista.
Une grande partie des fonctionnaires du ministère des finances, par exemple, restent en poste.
Les présidents de la banque nationale et de la banque du développement aussi, ainsi que la plupart des magistrats, des diplomates et de membres de la police, à l'exception de certains haut-placés qui craignent trop les représailles.
À la tête de l'armée, Castro place des proches, mais une partie des cadres et de militaires en place sous Batista restent à leur poste.
En outre, l'armée fonctionne de façon hiérarchissée avec un commandement auxquel obéir et sur lequel ni les troupes ni la population n'avait de contrôle.
Bref une armée bourgeoise classique.

Le nouvel Etat mis sur pied, ou plutôt recyclé à partir de celui de la dictature de Batista, allait pouvoir mettre en place ses premières mesures.


## Une île entre les Etats-Unis et l'URSS

### Les tribunaux exceptionnels, première crise politique

Une des premières tâches que se donne le nouveau pouvoir est de juger les tortionnaires du régime de Batista.
Pour se faire, des tribunaux exceptionnels se mettent en place, pour répondre aux aspirations de la population.
Des procès publiques et violents, qui résultent en des exécutions et des emprisonnements à vie (bien moins que sous le régime de Batista soit dit en passant).
Cela ne plait pas trop aux conservateurs, aussi bien cubains qu'américains, qui n'aiment pas trop que soit rappelée leur participation à la dictature, ni que la justice sorte du cadre habituel.

Quelques semaines seulement après la prise de pouvoir, une première crise politique s'installe donc, avec la démission du premier ministre.
Pour tenter de faire désescalader la situation et montrer patte blanche, Castro se rend aux Etats-Unis pour expliquer aux journalistes qu'il souhaite "établir une véritable démocratie à Cuba, sans aucune trace de communisme".


### La réforme agraire, pression des USA

Le 17 mai 1959, Castro promulgue la réforme agraire, souhaitée par les paysans pauvres et afin de développer l'agriculture.
Une réforme quand même relativement modérée, puisque les grands propriétaires fonciers ont le droit de posséder jusqu'à 402 hectares de la plupart des cultures et 1342 hectares pour le riz et le sucre.
De plus, la loi prévoie une indemnité assez favorable pour les propriétaires dont les terres sont confisquées.
Les confiscations de terre se font par en-haut, par les autorités et non pas les paysans directement, ce qui est aussi un gage que tout se passera bien pour les grands propriétaires.

Mais tout cela ne suffit pas à faire passer la pillule et une campagne anti-réforme agraire se met en place à Cuba comme aux USA.
Rapidement la situation économique se détériore avec la chute en bourse des compagnies sucrières.
Castro pose un ultimatum déguisé en démissionant le 17 juillet pour contester le président Urrutia qui n'irait pas assez loin selon lui.
Son ultimatum fonctionne : il organise une énorme manifestation dans laquelle il est acclamé par la foule, ce qui a comme conséquence de le remettre en selle, tandis qu'un autre président remplace Urrutia.
Mais la crise politique continue, et de nouvelles démissions se succèdent au sommet de l'Etat.

Des industriels, des propriétaires terriens, des techniciens, des intellectuels quittent Cuba pour les Etats-Unis.
De son côté le gouvernement américain menace de réduire ses importations de sucre si les indemnisations versées aux gros propriétaires expropriés ne sont pas plus élevées.
La pression économique est énorme pour le nouveau pouvoir cubain, embarqué dans un bras de fer qui dépasse les frontières de l'île.
Les USA font également pression auprès des autres pays d'Amérique latine pour qu'ils condamnet et isolent Cuba.

### Castro se tourne vers l'URSS

Les possibilités de coopération avec les USA écartées, Castro se tourne début 1960 vers l'URSS.
L'Etat soviétique est bien content de se trouver un allié si proche des Etats-Unis, et va donc multiplier les accords commerciaux avec Cuba : achat de centaines de tonnes de sucres, prêt de 100 millions de livres pour douze ans, livraison de pétrole et autres produits de base, ...
Avec ce nouveau revirement des démissions dans l'appareil d'Etat reprennent de plus belle.
Voyant Cuba fragilisée par plus d'un an d'instabilité politique, les USA pensent que Castro ne pourra plus tenir bien longtemps.

### Les Etats-Unis renforcent la pression économique

S'engage alors un face à face de plus féroce entre le pouvoir américain, representé par Eisenhower puis Kennedy, et le pouvoir cubain de Castro.
Les raffineries américaines à Cuba engagent le rapport de force avec le pouvoir en refusant de raffiner le prétrole soviétique, mais elles sont saisies par Castro.
La réponse américaine ne se fait pas attendre : Eisenhower réduit ses commandes de sucre.
Castro ne se laisse pas démonter et nationalise une partie des biens américains de Cuba (compagnies téléphoniques, électriques, sucrières) et accélère la mise en place de la réforme agraire.
Le 13 octobre, les Etats-Unis décrètent un embargo sur toutes les exportations américains vers Cuba, à l'exception de certains produits médicaux ou alimentaires (pour info, c'est un embargo que Cuba subit toujours aujourd'hui même si les modalités ont changé).
Pour riposter, le gouvernement cubain saisit de nouvelles usines américaines sur l'île.
Les capitalistes américains à Cuba avaient quand même sentit le vent tourner dés la révolution 1 an et demi avant, en faisant passé une partie de leur capitaux à l'étranger, sans le réinvestir dans leurs usines cubaines.
Et avec l'embargo, les pénuries de matériaux allaient se faire ressentir durement et retarderont le développement l'industrie.


### La baie des Cochons : les USA ne réussiront pas à se débarasser de Castro si facilement

Le 16 avril 1961, des exilés cubains anti-castristes, soutenus, entrainés et armés par la CIA, lancent une invasion depuis la baie des Cochons au centre de Cuba.
L'objectif : à partir des maquis du centre de l'île, espérer rallier le reste de la population pour renverser le régime.
Après 2 ans et demi de pressions économiques des Etats-Unis et de destabilisations, une nouvelle étape est franchie avec cette invasion militaire.
La riposte de Castro ne se fait pas attendre : la police et l'armée font 100 000 arrestations en 2 jours, et des milices sont envoyées contre les assaillants, qu'ils arrivent à repousser.
Pour Kennedy et l'impérialisme américain, habitué à faire et défaire les régimes selon ses intérêts, c'est un échec cuisant.


## Les évolutions de Cuba et son influence



-> le régime se repeint en rouge pour suivre la mode et contre les USA
-> À la fin de la guerre froide, doit trouver de nouveaux alliés, exemple pétrole du Vénézuela
-> rappeler que l'URSS de cette époque est issue de la stalinisation

## Conclusion

La population cubaine a réussi à se libérer du joug américain, ce qu'aucun autre pays d'Amérique latine n'avait vraiment fait à l'époque.
Castro et Che Guevarra ont representé un espoir pour les masses opprimées en Amérique latine et au-delà, dans les pays du Tiers-Monde soumis aux différentes puissances impérialistes.
Nous sommes du côté de Cuba, pour son indépendance face à l'impérialisme américain qui tente de l'étouffer depuis plus de 60 ans, car nous sommes du côté des pays sous-développés et anciennement colonisés qui tentent d'échapper à l'emprise des grosses puissances.
Si aujourd'hui la population de Cuba vit mal et que la misère y est criante, c'est de la responsabilité de l'impérialisme.

L'expérience cubaine a eu le mérite de montrer qu'il était possible de tenir tête à la première puissance mondiale, même quand on est une île de quelques millions d'habitants à quelques encablures des terres américaines.
Mais elle a aussi montré que ce n'était pas possible de se débarasser des contradictions du capitalisme dans le cadre national.

Malgré les avancées non négligeables obtenues grâce à la détermination de la population cubaine, malgré sa résistance féroce face à l'impérialisme américain, malgré une certaine élevation du niveau de vie et de développement de l'île à la sortie de la révolution, la misère y est encore très importante, les infrastructures sont vetustes et le pays doit faire face à de nombreux aléas économiques.

Cuba n'est pas le modèle de société que nous cherchons à mettre en place, une société où nous pourrions travailler moins et mieux vivre, une société débarassée des contraintes de l'économie capitaliste.
Nous ne pensons pas que la politique menée par Castro aurait pu permettre l'avènement d'une telle société, encore moins montrer la voie aux travailleurs par-delà les frontières.
D'ailleurs la révolution castriste ne s'est pas vraiment exportée, et contrairement à d'autres révolutions comme la Commune de Paris ou la révolution de 1917 en Russie, la classe ouvrière n'a pas réellement exercée le pouvoir en tant que telle.
Exercer le pouvoir à travers ses instruments de classes, qu'on les appelle soviets, comités d'usines, comités de quartier, ou autre.

En fait, le pouvoir castriste a fluctué au tout début, entre une politique de rapprochement avec les Etats-Unis, qui n'a pas marché, et plus tard avec l'URSS et d'autres pays du bloc de l'Est. Mais il n'a jamais tenté de s'adresser directement à la classe ouvrière cubaine, encore moins aux immenses classes ouvrières des pays du Tiers Monde ou des pays impérialistes.
Castro et les hommes politiques cubains qui ont pris le pouvoir en 1959 souhaitaient améliorer la vie de la population cubaine en mettant en plus un capitalisme plus humain et plus patriote, en contestant l'emprise de la bourgeoisie américaine sur l'île.
En élaborant sa politique au gré des interêts nationaux de Cuba, Castro a certes réussi à maintenir en place la société émanant de la révolution de 1959, ce qui n'était pas chose facile vu les pressions extérieures pour faire tomber le régime.
Mais il s'est aussi enfermé dans des frontières nationales, avec tous les sacrifices que ça implique pour la population et les limites qu'on a vu pour le développement du pays.

Les révolutionnaires russes, eux, ne voyaient pas la révolution dans leur pays comme un aboutissement, mais comme faisant partie d'un processus plus large qui aboutirait à l'émancipation du prolétariat mondial.
Ils avaient bien conscience que la révolution dans un pays arriéré économiquement et industriellement comme la Russie ne pourrait pas perdurer longtemps sans son extension rapide à d'autres pays.
La révolution russe a lancé une vague révolutionnaire en Europe, qui malheureusement a échoué, mais la preuve a été faite qu'une révolution ouvrière peut largement dépasser les frontières et trouve de l'écho dans les masses exploitées d'autres pays.
C'est pour cette perspective que nous militons et sur laquelle nous basons nos espoirs.



## Pour la discussion

- Dépendance aux importations de nourriture : 70% de la nourriture consommée en 2020

- En 2000, le secteur public représentait 77,5 % des emplois et le secteur privé 22,5 % alors qu'en 1981 le rapport était de 91,8 % et 8,2 %. L'investissement est réglementé et la plupart des prix sont fixés par les autorités publiques.

- Comités de défense de la révolution sorte de réseau de surveillance
