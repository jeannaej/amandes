# Les syndicats

- Premières organisations ouvrières

Reduction du temps de travail / augmentation des salaires
Le nombre n'est rien sans l'organisation.
Au Royaume-Uni au début du 19ème siècle, d'abord localement sous forme de clubs et associations, puis à l'échelle nationale pour toute une branche.
Mais aucun impact sur l'anarchie de la production capitaliste, ni sur les crises économiques. Nécéssité de s'organiser au-delà des frontières : création de l'AIT.

- Le bâton et la carotte

D'abord repression (les syndicats ne sont légalisés qu'en 1884) puis intégration des organisations syndicales au pouvoir de l'Etat.
[Pour la bureaucratie du mouvement syndical, la tâche essentielle consiste à "libérer" l'Etat de l'emprise capitaliste en affaiblissant sa dépendance envers les trusts et en l'attirant à lui.]
Fin du 19ème/début 20ème le nombre de syndiqué augmente énormément.
En Allemagne il est multiplié par 10 en 20 ans pour atteindre 2.5 millions en 1912.

- La 1ère Guerre Mondiale

La plupart des syndicats se rangent derrière l'union sacrée.

- Les années 30

Face à la montée ouvrière la bourgeoisie a 2 choix :

	* Se servir des appareils syndicaux pour canaliser la radicalisation
	* Briser les organisations ouvrières et morceler les travailleurs
	
En France et aux USA : première option
En Allemagne : deuxième option avec le nazisme

Exemple en France : grèves et occupations d'usines en mai-juin 1936, à l'époque le dirigeant de la CGT, Benoît Frachon, déclarera : "Nous disons aujourd'hui [aux ouvriers], avec franchise, que le prolongement de l'action gréviste, de la continuation de l'occupation des usines, les desservirait".

- La grève générale

Ce n'est pas une arme pûrement technique qui pourrait être décidée ou interdite.
Exemple de la Russie de 1905, à l'époque les appels à la grève générale de la part du parti socialiste ont été des échecs, et pourtant il y en a eu une.
Gréve générale comme un "saut qualitatif", phénomène historique résultant d'une situation sociale, découlant d'une nécessité historique (ne peut être décrété/décidé.

- Aller-retour entre grève politique et grève économique

Chap. 4 : "Chaque nouvel élan et chaque nouvelle victoire de la lutte politique donnent une impulsion puissante à la lutte économique en élargissant ses possibilités d'action extérieure et en donnant aux ouvriers une nouvelle impulsion pour améliorer leur situation en augmentant leur combativité. Chaque vague d'action politique laisse derrière elle un limon fertile d'où surgissent aussitôt mille pousses nouvelles les revendications économiques. Et inversement, la guerre économique incessante que les ouvriers livrent au capital tient en éveil l'énergie combative même aux heures d'accalmie politique; elle constitue en quelque sorte un réservoir permanent d'énergie d'où la lutte politique tire toujours des forces fraîches; en même temps le travail infatigable de grignotage revendicatif déclenche tantôt ici, tantôt là des conflits aigus d'où éclatent brusquement des batailles politiques."


- Potentiel révolutionnaire dans les éléments inorganisés du prolétariat

Chap. 6 : "Six mois de révolution feront davantage pour l'éducation de ces masses actuellement inorganisées que dix ans de réunions publiques et de distributions de tracts. Et lorsque la situation en Allemagne aura atteint le degré de maturité nécessaire à une telle période, les catégories aujourd'hui les plus arriérées et inorganisées constitueront tout naturellement dans la lutte l'élément le plus radical, le plus fougueux, et non le plus passif."

- Lien entre Parti et syndicat

Chap. 8 : "Les syndicats représentent l'intérêt des groupes particuliers et un certain stade du développement du mouvement ouvrier. La social-démocratie représente la classe ouvrière et les intérêts de son émancipation dans leur ensemble."

Réunir le Parti et les syndicats, subordonner les syndicats au Parti ?

- Bureaucratisation

Chap. 8 : "surestimer l'organisation et à en faire peu à peu une fin en soi et le bien suprême auquel les intérêts de la lutte doivent être subordonnés"

- La politique des révolutionnaires dans les syndicats

On pourrait en conclure que les syndicats ne laisse plus de place à la démocratie ouvrière et que le travail révolutionnaire au sein des syndicats disparait. Mais ça serait une erreur
Mots d'ordre :

	* Indépendance complète et inconditionnelle des syndicats vis-à-vis de l'Etat capitaliste
	* Démocratie dans les syndicats
	
Pousser les travailleurs à prendre la direction de leurs propres luttes et non prendre des places à la CGT et attendre qu'elle change (en influançant/faisant pression sur les syndicats)
Intervenir dans les syndicats mais s'adresser à l'ensemble des travailleurs (mise en place de comités de grèves, etc)
Mettre en avant l'indépendance de classe dans les mouvements sociaux.
Prendre la direction des mouvements.
Nécessité pour les révolutionnaires de ne pas se couper des syndicats.

"Bien sûr, la tâche vitale de l'heure reste de reconstruire un parti révolutionnaire. Sans ce parti, l'activité syndicale reste limitée au terrain juridique ou à celui de l'entraide sociale, c'est-à-dire qu'elle reste sur le terrain de la bourgeoisie."

"Militer dans un syndicat c'est apprendre aux travailleurs, aux syndiqués, à diriger eux-mêmes leurs propres luttes tout en luttant au jour le jour contre l'exploitation. Les révolutionnaires doivent impulser systématiquement des assemblées générales de grévistes. Même quand ils animent eux-mêmes le syndicat, ils doivent chercher à mettre en place, dès que possible, des comités de grève ou de lutte, qui soient le plus dynamiques et le plus représentatifs possibles. Une grève entraîne bien plus de travailleurs que ceux habituellement influencés par les syndicats et chaque gréviste doit pouvoir participer lui-même à sa direction."

"En effet, pour mettre un terme à l'exploitation, il ne suffira pas d'une lutte radicale contre tel plan de licenciement ni d'une grève générale contre la réforme des retraites. Il faudra changer radicalement la société, c'est-à-dire contester à la bourgeoisie son pouvoir. Cela ne pourra se faire qu'à travers un tremblement social profond. Transformer la société bourgeoise nécessite la prise de conscience et l'entrée en lutte de la quasi totalité du prolétariat. Une telle radicalisation peut venir sans prévenir. Elle peut se traduire par un afflux vers les syndicats ou prendre de tout autres formes."

- Peu de syndiqués

8% des salariés (5% dans le privé)
Il en va de même dans les autres pays occidentaux (Allemagne, Angleterre, etc)
Recul depuis la fin des années 1970.
Un recul qui inquiète même, d'une certaines façon, les dirigeants, qui ont bien plus de mal à négocier avec les masses en colère qu'avec des bureaucrates autour d'une table
Exemple : les gilets jaunes, peu syndiqués, parfois issue de petites boîtes (en réalité sous traitant de grosses entreprises)


- Raison de ce recul des syndicats

Des attaques féroces du patronat, la sous-traitance, recourt à l'interim, le chômage massif, ...
Mais également démoralisation des militants syndicaux, déception avec la gauche au pouvoir


# Sources :


## Grève de masse, parti et syndicat

- https://www.marxists.org/francais/luxembur/gr_p_s/greve.htm
- https://wikirouge.net/Gr%C3%A8ve_de_masse,_parti_et_syndicat


## Des exposés du CLT

- https://www.lutte-ouvriere.org/documents/archives/cercle-leon-trotsky/article/temps-de-travail-salaires-et-lutte
- https://www.lutte-ouvriere.org/documents/archives/cercle-leon-trotsky/article/les-syndicats-hier-et-aujourd-hui-12899
- https://www.lutte-ouvriere.org/documents/archives/cercle-leon-trotsky/article/les-retraites-faire-face-a-l
- https://www.lutte-ouvriere.org/publications/brochures/lexplosion-sociale-de-mai-juin-1968-107027.html


## 4 articles de la LDC de 1967 (les militants révolutionnaires dans les syndicats)

- https://mensuel.lutte-ouvriere.org//documents/archives/la-revue-lutte-de-classe/serie-1967-1968/article/les-militants-revolutionnaires-et-7414
- https://mensuel.lutte-ouvriere.org//documents/archives/la-revue-lutte-de-classe/serie-1967-1968/article/les-militants-revolutionnaires-et-7420
- https://mensuel.lutte-ouvriere.org//documents/archives/la-revue-lutte-de-classe/serie-1967-1968/article/les-militants-revolutionnaires-et-7425
- https://mensuel.lutte-ouvriere.org//documents/archives/la-revue-lutte-de-classe/serie-1967-1968/article/les-militants-revolutionnaires-et
