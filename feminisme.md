# Topo féminisme

## Message d'invitation

La légalisation de l'avortement en Argentine il y a deux mois, après des mobilisations massives, constitue une victoire historique pour les Argentines.
Partout dans le monde des femmes se battent pour leurs droits : droit à l'avortement, à la contraception, à l'indépendance économique, voire même le droit de vivre.
D'ailleurs, au cours de l'histoire elles ont été de tous les combats : durant la révolution française, la Commune de Paris ou encore la révolution russe, elles ont lutté contre une société inégalitaire qui les decrètent inférieures aux hommes.
Que peut-on apprendre de la longue lutte des femmes pour leur émancipation ?
Discutons-en *vendredi à 18h* en distanciel !

## Introduction

L'oppression des femmes à travers le monde continue d'être un des principaux fléaux de nos sociétés, rendant la vie particulièrement difficile, précaire, instable voire dangereuse pour une large partie de l'humanité.

On peut penser, bien sûr, aux manifestations les plus spectaculaires de cette oppression dans l'actualité des derniers mois.

En France, les féminicides, médiatisés notamment par les collages féministes qui ont recouvert les murs des villes, sont un des plus brutal aspect des violences exercées contre les femmes.
Les témoignages massifs rassemblés depuis la vague MeToo parlent d'eux-mêmes : dans le cadre familiale ou privée, dans les transports, sur les lieux de travail, les violences sexistes sont omniprésentes.
Loin de lutter efficacement contre ces violences, Macron a récemment nommé Darmanin, accusé de viol, au ministère de l'intérieur.
C'est un véritable crachat à la face des toutes les femmes victimes de violences : pour l'égalité hommes-femmes comme "grande cause du quinquennat", on repassera.

Quand il est question de violences contre les femmes, on peut penser également aux viols collectifs en Inde, qui ont causé la mort de deux femmes en septembre dernier.
Là-bas l'oppression des femmes est renforcée par la survivance du système des castes.

Aux États-Unis, les milieux réactionnaires tentent d'imposer des reculs du droit à l'avortement, soutenus par le président sortant et par les fondamentalistes chrétiens.

Au Brésil, l'accès à l'IVG est limité aux femmes victimes de viol, dans un cas où il y a danger pour la vie de la femme ou dans le cas de problèmes congénitaux graves du foetus, et encore, il faut se battre pour trouver un médecin qui accepte de le pratiquer.
Le président brésilien, Jair Bolsonaro, a décidé, l'été dernier, de durcir encore plus la réglementation sur l'avortement en cas de viol, tentant de culpabiliser les victimes d'avantages.

En février dernier, la militante féministe saoudienne Loujain al-Hathloul a été libérée après 2 ans et demi de prison, où elle a subit flagellation, tortures et harcèlement sexuel, tout ça pour s'être mise en scène sur les réseaux sociaux au volant d'une voiture.

En plus de ces aspects les plus violents de l'oppression des femmes, il y a aussi toutes les inégalités, les injustices et le sexisme du quotidien.
Il y a la différences de salaires hommes/femmes déjà, mais également la plus large proportion de contrats précaires et en temps partiel des femmes (souvent pour adapter ses horaires à la garde d'enfants).
Ces inégalités se retrouvent d'ailleurs inévitablement dans le montant des pensions de retraites.

Les mères de famille monoparentale sont les plus exposées à la pauvreté.
En 2015, plus de 8 famille monoparentales sur 10 avait à leur tête une femme, ce qui a un impact sur leurs vies professionnelles.
Ainsi, en 2017, parmi les mères de familles monoparentales actives, 17 pourcents étaient au chômage dont 47 de longue durée.

Et puis que dire de la répartition du travail domestique dans les couples hétéro, de l'hypersexualisation du corps des femmes, etc.

Mais partout les femmes se battent pour leurs droits : droit à l'avortement, droit à l'égalité avec les hommes, droit à une vie digne ou parfois juste droit à vivre, etc.

En Argentine, la légalisation de l'avortement a enfin été adoptée par le Sénat fin 2020.
Bien que la mesure ait été une promesse de campagne de l’actuel président, la victoire que constitue ce vote n’a été obtenue que parce que des centaines de milliers d’Argentines et d’Argentins se sont battus pour l’obtenir.

La Pologne a également connu en novembre dernier une mobilisation massive contre un projet de loi visant à restreindre l'accès à l'avortement.
Même si la loi a finalement été adoptée, la colère des centaines de milliers de personnes qui ont pris la rue est loin d'avoir disparue.

Lundi prochain aura lieu la journée internationale de lutte pour les droits des femmes, qui sera l'occasion pour celles et ceux qui luttent contre l'oppression des femmes de se faire entendre !

Mais alors comment se débarasser de cet ordre social dans lequel les femmes sont considérées inférieures aux hommes ?

### L'oppression des femmes, une vieille (pré) histoire

Il n'y a pas, à priori, de consensus scientifique clair sur l'origine de l'oppression des femmes, et les travaux des préhistoriens à ce sujet reflètent en partie celles et ceux qui les mènent.
Ce qui est sûr, c'est que la division sexuelle du travail et l'appropriation des femmes par le mariage n'est pas nouvelle.
Ce qui est sûr également, c'est que les inégalités entre hommes et femmes n'ont rien de naturelles, elles sont sociales (comme le disait Simone de Beauvoir, théoricienne féministe, "on ne nait pas femme, on le devient").

On a pu observer à travers les époques, que l'oppression des femmes est particulièrement marquée dans les sociétés de classes, et que les femmes étaient partout en charge de toutes les tâches domestiques liées à la reproduction de la force de travail, c'est-à-dire toutes les tâches qui permettent le repos, l'éducation, les soins, l'alimentation, etc, du travailleur et de la travailleuse pour qu'il et elle puissent retourner travailler le lendemain.

### La lutte des femmes dans les révolutions

Des femmes, il en a bien fallut et il en faut encore pour mener la longue lutte contre leur oppression.
Il n’y a pas d’évolution naturelle vers l’égalité entre hommes et femmes dans la société... il faut se battre pour l’imposer !

### Révolution française

On peut déjà se pencher sur la Révolution française, dans laquelle les femmes jouent un rôle de premier plan.
Elles réclament la liberté du travail pour les hommes comme pour les femmes.
Elles exigent la fin des privilèges des corporations héritées du Moyen Âge pour avoir le libre accès à toutes les professions artisanales.
Elles contestent l'ordre social et posent leurs propres revendications : l’éducation pour les filles, le droit au divorce, l’égalité des droits politiques.
C'est dans cette période que Olympes de Gouges écrit la _Déclaration des droits de la femme et de la citoyenne_.
Mais dés 1793, la montée révolutionnaire s'achève, et les avancées conquises depuis 4 ans sont remises en cause.


### La famille bourgeoise, un frein à l'émancipation des femmes

Napoléon se place à la tête de la réaction qui suit la révolution, et enterinne la domination des femmes dans le Code civil de 1804.
Le Code civil va, entre autre, décreter que le mari constitue le chef de famille et priver les femmes de nombreux droits juridiques.
Il accorde au mari, non seulement le droit à disposer de sa propriété, mais aussi le droit à la domination morale et physique sur son épouse.

Même si le code civil a été modifié depuis, sa version de 1804 donne un bon aperçu des aspirations de la bourgeoisie victorieuse concernant la place des femmes dans la société et sa conception de la famille.
Les anciennes classes dominantes battissaient leur puissance sur le droit divin, l'appartenance à une lignée prestigieuse, etc.
Mais la classe capitaliste, elle, base son pouvoir sur la possession de capitaux.
Il lui faut donc un cadre pour légitimer la transmission de ces capitaux par l'héritage ; ce cadre elle le trouve dans la famille.

La libération des femmes devra forcément passer par l'émancipation des formes actuelles de la famille, qui sont dépassées et oppressives, et qui tend à les releguer au rôle d'épouse ou de mère.
L'oppression familiale, nourries par les coutumes et les traditions, est omniprésentes dans toutes les couches sociales : aussi bien la femme bourgeoise que l'ouvrière et la paysanne sont sous la tutelle de leurs maris.

L'institution familliale, en faisant de la norme l'union d'une femme et d'un homme, est d'ailleurs aussi largement responsable de la situation des personnes LGBTI à travers le monde (violences physiques, suicides, déni de droits au mariage, à l'adoption, voire même de vivre).

### Le rôle des femmes dans le premier État ouvrier de l'histoire : la Commune

La réaction napoléonienne durera quelques années, et déjà de nouvelles révolutions, en 1830 et 1848, viennent rebattre les cartes.
Au long du 19ème siècle se développeront conjointement les revendications ouvrières, comme la diminution du temps de travail, et les revendications des droits des femmes, comme le droit au travail.

En 1871, la Commune de Paris, premier Etat administré par des ouvriers et des ouvrières, met en place pour quelques semaines une société plus égalitaire et représentera un immense espoir pour les millions d'exploités et d'opprimés à travers le monde.
Les femmes y joue un rôle primordial.
Elles sont armées et montent la garde aux portes de Paris, organisent le soin et l'approvisionnement aux blessés.

Il reste encore beaucoup à faire : elles n'ont pas accès au droit de vote ni aux postes de pouvoir, mais la Commune met quand même en place certaines mesures qui constituent des avancées pour les droits des femmes : entre autre la reconnaissance de l'union libre et la reconnaissance des enfants nés hors mariage, la mise en place d'une début d'égalité salariale.

Lors de la semaine sanglante, les communards et les communardes sont massacrées par les troupes versaillaises envoyées par Thiers.
Mais la flamme allumée par cette expérience révolutionnaire ne s'est pas éteinte après la défaite, et les bouleversements sociaux rendent inexorable la lutte des femmes pour leurs droits.

### Développement du travail salarié des femmes

Avec l'avènement du capitalisme, les femmes, souvent reléguées aux travail dans le cadre familiale durant les siècles précédents, sont de plus en plus contraintes d'aller s'employer hors des foyers.

Avec ce nouveau mode de production, une nouvelle organisation du travail se met en place.
Les femmes, en plus d'autres tâches, créaient autrefois des produits tels que de la toile, du fil, du beurre, etc.
Mais ces produits avaient vocation à être utilisé dans la sphère familiale, ou alors minoritairement, à être échangé sur le marché.
Cette façon de produire de la richesse est en opposition au fonctionnement du capitalisme, qui tend à faire de tout produit une marchandise, c'est-à-dire à le rendre échangeable sur le marché.

Les biens fabriqués autrefois au sein de la famille se retrouvent de plus en plus produit à l'échelle industrielle, de façon collective (donc plus efficacement) et sont ensuite vendus.

Ainsi, le développement de l'industrie a permis la diminution du travail domestique effectué par les femmes : là où elles devaient fabriquer elle-même certains produits, elles les achètent maintenant déjà manufacturés.

De plus, le salaire procuré par le travail d'un ouvrier tend à n'être pas suffisant pour faire vivre toute une famille.

Bref, dés le 19ème siècle et l'essor de l'industrie, les femmes des familles ouvrières rejoignent massivement les fabriques, ou deviennent journalières, vendeuses, commis de bureau, blanchisseuses, servantes, etc.

Mais à cette époque, les femmes sont loin d'avoir toutes accès au travail salarié, et de nombreux secteurs refusent des les employer.

Le patronnat voit l'arrivée des femmes à la fois comme une aubaine, en ce qu'elles représentent une nouvelle main d'oeuvre bon marché et encore peu syndiquée, mais également comme une menace pour la survie de la famille bourgeoise.
Le calcul est que si la femme prend trop d'indépendance, elle risquerait de sortir de l'emprise du mari, remettre en cause la transmission du capital par l'héritage du père au fils, de se voir comme l'égale de l'homme, etc.

Rapidement les patrons comprirent l'interêt d'opposer les femmes aux hommes dans le monde du travail.
Ils utilisèrent les femmes, moins bien payées, pour faire pression sur les salaires des hommes.
En mettant les hommes et les femmes en concurrence, ils firent peser sur ces dernières la responsabilité du chômage.
Ils menacèrent de briser les grèves en utilisant des femmes pour remplacer les grévistes, instrumentalisant la méfiance et les préjugés des premières organisations ouvrières envers les femmes.

Alexandra Kollontaï, une dirigeante bolchévique, raconte dans une série de conférence sur la libération des femmes :

"Dans d'innombrables branches industrielles (par exemple dans l'industrie mécanique, la typographie, etc., employant une main-d'œuvre qualifiée), l'arrivée des ouvrières dans la production fut activement combattue par leurs collègues masculins. De nombreux syndicats stipulaient dans leurs statuts « l'exclusion de la main-d'œuvre féminine non qualifiée, responsable de la dégradation des revenus des ouvriers »."

Ce n'est qu'à force de luttes au sein même des orgranisations du mouvement ouvrier, et par la démonstration qu'elles n'étaient pas moins combattives que les hommes, que les femmes réussirent à s'imposer dans le mouvement socialiste.
Celui-ci commence à porter, à la fin du 19ème et début du 20ème siècle, des revendications concernant l'amélioration de la vie des femmes, et des militantes reconnues émergent à ce moment, telles que Rosa Luxemburg, Clara Zetkin, Adelheid Popp, Alexandra Kollontaï, et bien d'autres, liant la lutte des femmes et la lutte du prolétariat.

### Les avancées politiques des femmes et du mouvement ouvrier

De même que des améliorations de conditions de travail sont possibles dans une certaine mesure et pour un certain temps, l'amélioration partielle de la vie des femmes est possible dans le cadre du système capitaliste.
Il faut bien sûr se battre pour toutes ces avancées, qui permettent non seulement des améliorations matérielles, mais aussi la prise de conscience de la majorité de la population qu'elle a des interêts en commun à défendre.
Mais il ne faut pas oublier que ces avancées ne sont pas figées, il n'y a qu'à voir que l'accès à l'avortement est rendu plus difficile qu'avant dans certains pays.
Pour se débarasser d'une oppression il faut détruire les bases sur lesquelles elle est batie.

À la fin 19ème et au début du 20ème siècle, la politisation des femmes, et en particulier des femmes issue du prolétariat, s'accroit.
Elles participent à la vie politique, à travers les campagnes, les mettings socialistes, etc.
Pas à pas, et après des luttes féroces, les gouvernements ont été forcé d'accorder aux femmes des droits syndicaux et de réunion.

Un des mots d'ordre de la période, encore emblématique de ces luttes jusqu'à aujourd'hui, est le droit de vote des femmes.

La lutte pour ce droit est vue par bien des femmes socialistes comme un tremplin permettant de mener une bataille bien plus large.
Une bataille qu'elles proposent de mener, non pas à la remorque des femmes bourgeoises, mais en entrainant de larges couches de la classe ouvrière, femmes et hommes, à l'instar de Rosa Luxemburg qui écrivait en 1912 :
"Le suffrage féminin, c’est le but.
Mais le mouvement de masse qui pourra l’obtenir n’est pas que l’affaire des femmes, mais une préoccupation de classe commune des femmes et des hommes du prolétariat.
Le manque actuel de droits pour les femmes en Allemagne n’est qu’un maillon de la chaîne qui entrave la vie du peuple.
[...]
Grâce au prolétariat féminin, le suffrage universel, égal et direct des femmes, ferait avancer considérablement et intensifierait la lutte des classes du prolétariat.
C’est la raison pour laquelle la société bourgeoise déteste et craint le suffrage féminin.
Et c’est pourquoi nous le défendons et nous l’obtiendrons."

Qu'il s'agisse du droit de vote ou d'autres droits, au delà d'une égalité de droit avec les hommes, il s'agit d'imposer une égalité de fait.
À titre d'exemple, le préambule de la constitution française de 1946 garantit l'égalité des droits reconnus aux femmes et aux hommes dans tous les domaines.
C'est bien gentil, mais il faudra plus que de jolies phrases sur un bout de papier pour mettre fin à des milliers d'années d'inégalités !

Imposer une avancée et qu'elle soit réellement appliquée, cela nécessite plus qu'un article de consitution, un projet de loi ou une majorité parlementaire.
Nous avons déjà discuté un peu de la Commune, dans laquelle des femmes et des hommes ont pris eux-mêmes leurs affaires en main, appliquant par eux-mêmes les mesures décidées collectivement.
Marx, décrivant la Commune, disant qu'elle "devait être non pas un organisme parlementaire, mais un corps agissant, exécutif et législatif à la fois".
À une échelle beaucoup plus large, le développement des Soviets et de l'État ouvrier en Russie en 1917, est la démonstration de quoi sont capables les travailleurs et les travailleuses qui prennent leurs affaires en main.


### La cause des femmes et la révolution russe

La révolution russe débute le 8 mars 1917, par la mobilisation des ouvrières du textile de Pétrograd, et les femmes seront durant les mois suivants de toutes les luttes, elles manifestent, font grèves et s'arment.
Elles participent à l'Armée rouge en creusant des tranchées et montant la garde aux barrages routiers, et Alexandra Kollontaï devient la première femme du monde à rentrer dans un gouvernement.
Dés les premières semaines et les premiers mois, le pouvoir des soviets s'attaquent aux inégalités entre femmes et hommes, à l'instar du décret des Commissaires du Peuple du 18 décembre 1917 qui autorise le divorce à l’amiable.
Le mariage religieux est aboli, l’autorité du chef de famille disparaît du Code civil, le droit d’héritage est supprimé, l'IVG est légalisée.

Le Parti Bolchévique entreprend également de s'attaquer aux racines du problème.
Il fait participer un maximum de femmes à la vie politique, les associant à l'organisation de la production et à l'Etat soviétique.

Des restaurants collectifs, des laveries, des crèches sont construites, pour émanciper la population du travail domestique, mais restent encore rare à cause des conditions militaires et alimentaires de la période.

L'évolution des mentalités reste lente, à cause des reflèxes hérités du passé, et l'échec de l'extension de la révolution à d'autres pays ouvre la voix aux stalinisme et aux reculs pour les droits des femmes notamment.

### La réaction stalinienne revient sur les revendication pour les droits des femmes

Sortie d'une guerre mondiale suivie de plusieurs années de guerre civile, le jeune Etat ouvrier est aux abois, et une bureaucratie se développe en son sein.
Avec elle, les idées réactionnaires prennent peu à peu le pas à l'intérieur même du Parti Bolchévique, dans lequel les révolutionnaires de 1917 sont peu à peu écartés.
La procédure de divorse s'allourdie et l'avortement est interdit en 1936, la famille est glorifiée.

Au delà des frontières de l'URSS, Staline contrôle les partis communistes récemment créés et en fait des relais de sa politique.

Ainsi, en France, le parti communiste qui défendait avortement et contraception au début des années 1920, s'alignera à la politique pro-nataliste de Staline.
D'ailleurs, jusque dans les années 1960, le PCF reste opposé au droit à l'avortement.

De même, la CGT, centrale syndicale liée au PCF, estimait que les droits des femmes avaient comme but de les protéger dans leur rôle de mère (obtenir des congés spéciaux pour soigner ses enfants, exiger la retraite pour les femmes cinq ans avant les hommes, afin de compenser l’usure de la double journée, ou encore revendiquer des aménagements d'horaires pour leur permettre d'accomplir plus facilement les tâches de cette deuxième journée familiale.
Pas très émancipateur comme programme !

Au début des années 1970, la rupture est consommée entre le PCF et une partie des militantes féministes : le MLF (Mouvement de libération des femmes) choisit de manifester au côté des révolutionnaires et non pas avec la gauche traditionnelle, comme c'était le cas auparavent.

Aujourd'hui, la question de la place du mouvement féministe par rapport au mouvement ouvrier est encore sujet à débat dans la gauche et l'extrême-gauche.

Pour en discuter, il nous semble important de constater que l'oppression des femmes se combine avec l'exploitation capitaliste et qu'elles se renforcent l'une l'autre.

### Oppression des femmes et exploitation capitaliste font toujours bon ménage

Il y a déjà la double journée de travail.
Les femmes étant encore majoritairement en charge du travail domestique, elles doivent donc supporter une sorte de deuxième journée de travail, non salarié cette fois, à la maison.
Délimiter strictement ce qui relève du travail domestique ou non n'est pas évident, mais une étude de l'INSEE datant de 2010 est assez instructif à ce sujet.
Cette étude montre qu'en France, les mères en couple avec des enfants de moins de 25 ans consacrent en moyenne 34 heures par semaine au travail domestique.
Avec une durée moyenne de 36 ou 37 heures de travail salarié par semaine, on est donc assez proche du même nombre d'heure de travail domestique !
Toujours selon cette étude, les tâches au coeur du travail domestique sont réalisées à 72 pourcents par les femmes.

Au croisement entre l'organisation capitaliste du travail et l'oppression des femmes, on retrouve également toutes les violences sexuelles faites au travail.
Récemment, c'est le studio de jeux vidéos Ubisoft qui a connu une vague d'accusations de harcèlement sexuel, mais ce genre de pratiques est répandu très largement dans le monde du travail.

Il peut s'agir de situations renforcées par le cadre hiérarchique de l'entreprise, dans laquel la victime, en bas de l'échelle, risque gros si elle dénonce les agissements.
Il y a quelques semaine, une employée d'un restaurant Mc Donalds au Havre s'est faite licenciée après avoir dénoncé ces derniers mois le harcèlement moral et sexuel dont elle était victime.
Mais il ne s'agit pas de cas isolés : parmi les 400 personnes suivies par l'Association européenne contre les violences faites aux femmes au travail (AVFT), 95 pourcents ont perdu leur travail après avoir dénoncé les actes.
Au gouvernement, Darmanin lui-même est accusé d'avoir profité de sa position dominante d'élu pour obtenir des faveurs sexuelles !

Mais même quand la victime et le ou les agresseurs sont au même niveau hiérarchique, la boîte cherchera à couvrir les faits et éviter toute mauvaise pub.

Des aspects de l'oppression des femmes qui se renforcent par le mode de production capitaliste, il y en a encore bien d'autres : ce système économique et la société qu'il engendre est profondément inégalitaire sur tous les aspects, y compris du point de vue des femmes.

## Conclusion

Plus qu'à aucune époque, le développement des forces productives engendré par le capitalisme porte en germe les bases de la fin de l'oppression des femmes, en rendant possible leur indépendance économique des hommes.
Nous avons les moyens de collectiviser les tâches domestiques : restaurants, laveries, prise en charge de la couture, prise en charge des enfants (nourrir, vêtir, crèches, ...) et de reléguer la famille bourgeoise au statut de relique du passé.
Mais seul un changement profond dans la société est en mesure de détruire la division actuelle du travail, qui enchaîne les femmes aux travaux domestiques.

Les forces réactionnaires qui accompagnent inévitablement le mode de production capitaliste, basé sur la propriété privée, l'accaparement individuelle et sur une forme oppresive de la famille, est une entrave à l'émancipation des femmes.
Il n'y a pas de raccourcis : on ne peut se débarasser de cette oppression sans saper les bases matérielles sur lesquelles elle se fonde.
Et cela, seule une révolution socialiste peut le permettre.

Cependant il ne s'agit pas d'une excuse pour abandonner le combat féministe.

Certains voudraient remettre à plus tard, à après la révolution, la question de l'amélioration des conditions de vie des femmes.
Ils se trompent, il faut dés maintenant saisir chaque opportunité de se battre et d'entrainer celles et ceux qui veulent un monde meilleur, débarassé de l'exploitation et des oppressions.


### Sources

- Viols collectifs en Inde : https://www.convergencesrevolutionnaires.org/Bas-les-castes?navthem=1
- IVG au Brésil : https://www.lemonde.fr/international/article/2020/08/29/le-bresil-durcit-sa-reglementation-sur-l-avortement-en-cas-de-viol_6050306_3210.html
- En prison pour avoir conduit en Arabie Saoudite https://journal.lutte-ouvriere.org/2021/02/17/arabie-saoudite-liberee-pas-vraiment-libre_154762.html
- Taux de pauvreté hommes/femmes : https://www.insee.fr/fr/statistiques/3567016#figure1_radio2
- PIS : https://www.convergencesrevolutionnaires.org/Pologne-PiS-hors-de-nos-chattes-ou-Retour-sur-la-mobilisation-polonaise-pour-le
- ¡ ES LEY ! : https://www.convergencesrevolutionnaires.org/ES-LEY-Victoire-pour-les-Argentines
- Temps de travail domestique (INSEE) : https://www.insee.fr/fr/statistiques/2123967#titre-bloc-7
- Mères de familles monoparentales : https://www.haut-conseil-egalite.gouv.fr/pied-de-page/ressources/reperes-statistiques/
- Harcèlement sexuel au travail : https://www.lemonde.fr/societe/article/2017/10/18/harcelement-sexuel-au-travail-comment-peut-on-agir_5202901_3224.html
- Licenciement à Mc Donalds : https://www.lepoint.fr/justice/mcdonald-s-une-employee-licenciee-apres-avoir-denonce-un-harcelement-sexuel-08-01-2021-2408741_2386.php
- La peur de dénoncer les violences sexuelles au travail : https://www.lemonde.fr/societe/article/2017/11/23/violences-sexuelles-chez-les-ouvrieres-la-peur-de-perdre-son-travail_5219215_3224.html
- CLT : https://www.lutte-ouvriere.org/publications/brochures/les-combats-pour-lemancipation-des-femmes-et-le-mouvement-ouvrier-65653.html
- Conférence sur la libération des femmes, Alexandra Kollontaï : https://www.marxists.org/francais/kollontai/works/1921/0a/kollontai_conf.htm
- Suffrage féminin et lutte de classes, Rosa Luxemburg : https://www.marxists.org/francais/luxembur/works/1912/05/suffrage.htm
