# Cuba : Castro et le castrisme

### Intro

En 1959, Cuba est le 2eme pays d'Amérique latine après le Venezuela pour l'importance des capitaux US.
Capitaux US = 50 à 90 pourcents des actions des compagnies de chemins de fer, électricité, téléphone ; 40% de la production de canne à sucre.
Principale production de l'île = canne à sucre et 25% des meilleures terres appartiennent aux US.

### Les débuts de l'opposition à la dictature

1952 coup d'Etat de Batista
Fidel Castro, jeune avocat, intellectuel de l'opposition (y compris armée)
Il est emprisonné pendant 2 ans, puis libéré mais part en exil.

### Le retour de Castro

1955-1956 mobilisations dans une large partie de la population

### Les débuts de la guérilla

Groupe de guerilleros dirigé par Castro trouve refuge au Sud de l'île sur des terres inexploitées (soutenu par des paysans harcelé par le pouvoir, des hors-la-loi, une partie de la bourgeoisie libérale, jeunesse des villes)
mais au début le groupe ne grossit pas, certains désertent (ils sont une vingtaine au début)
D'autres groupes armés (tentative de prise du palais en février 1957)
Régulièrement des attaques contre les casernes
Mais en quelques mois environ 300 personnes dans le groupe de Castro, discipliné, véritable proto-appareil d'Etat

### Une difficile progression

Soutien du parti communiste à Castro et la lutte armée en février 1958
En mars/avril Castro veut lancer une grève générale en appui à une série d'attaques militaires. Mais c'est un échec.
Le mouvement du 26 juillet, notamment une partie des dirigeants installés à La Havane n'avait pas pris la peine de prévenir le PC (certains étant farouchement anticommunistes et voulant s'assurer la victoire pour eux seuls)
En fait le mouvement comptait bien plus sur les classes moyennes pour paralyser le pays que sur la classe ouvrière.
En témoigne le comité de grève mis en place à La Havane qui comprenait, en plus de 2 dirigeants du mvt du 26 juillet, un ingénieur chef de la resistance civique, le chef de l'église évangélique, un journaliste et un médecin.
Suite à cette échec organisé par les dirigeants du mouvement de La Havane, Castro prend la tête du mouvement du 26 juillet et des forces armées.

### Vers la chute de Batista

Batista tente d'anéantir la guerilla en envoyant l'armée sur Castro (20 000 hommes contre 200).
Castro se replie, arrive à organiser des contre attaques qui démoralisent l'armée de Batista, qui se retire finalement de la province.
Fidel Castro et son frère Raoul harcèle l'armée démoralisée (d'autant plus après les élections en novembre où Batista est élu avec seulement 30% des votants)
Des officiers se rendent à Castro.
Le régime perd peu à peu l'autorité sur certaines zones.
Guevarra commence à procèder à la réforme agraire en partageant les terres du gouverneur de la province.
La Province d'Oriente était quasiment entièrement aux mains des rebelles, qui levaient les impôts, réglaient la circulation des marchandises et des personnes, et Carlos Franqui, l'un des dirigeants du Mouvement du 26 juillet, explique à ce propos comment les détenteurs « des principales richesses et propriétés du pays, biens nationaux et étrangers, entraient en rapport avec le nouveau pouvoir révolutionnaire et recevaient garanties et protections pour leur développement, le maintien de l'ordre et de la propriété ».
L'instabilité du régime est une épine dans le pied pour les hommes d'affaires de La Havane qui n'attendent qu'une chose : le renversement de Batista par tous les moyens.
Il en va de même pour les Etats-Unis.
La CIA tente de trouver une junte militaire pour remplacer le dictateur.
Des complots se trament au sein de l'armée, dont certains officiers sont en lien avec Castro.
Celui-ci s'efforcera de préserver l'armée de la désintégration.
Le 31 décembre 1958, Batista fuit et laisse le président de la cour suprême comme président provisoire.

### Castro prend le pouvoir

Le 1er janvier 1959, des groupe armés, dirigés notammenet par Guevarra, s'emparent des points stratégiques (c'est la course entre le mvt du 26 juillet, le directoire revo et le PC)
Castro appelle à la grève générale pour le 2 janvier, en voulant garder le contrôle et éviter les débordements.
Le juge Urrutia est nommé président.
Rivalité entre le mvt du 26 juillet et le PC pour savoir qui allait dirigier les syndicats et la grève générale.
Castro fait le tour de l'ile, ovationné.
Le directoire revo ne dépose pas les armes car ils n'obtiennent pas de places au gvt.
Le 8 janvier Castro s'adresse aux cubains à la télé pour dire qu'il faut que tout rentre dans l'ordre, il est interdit de posséder des armes (pique envoyé au directoire revo) et il faut arrêter la grève.

### La mise en place du nouveau régime

Le nouveau gvt devait assurer la transition vers un régime parlementaire et les élections fixées à 15 mois environ.
Sa composition n'avait pas de quoi effrayer la bourgeoisie aussi bien cubaine que US.
Des politiciens liés aux partis traditionnels occupaient des postes clefs : le président Urrutia était un liberal modéré, le premier ministre un des grands avocats d'affaires du pays et politiciens pro-américain, le ministre des affaires étrangères avait été candidat pour le parti orthodoxe dans les années 1950, le ministre des finances avait été chargé du dvpt des banques sous le gvt précédant la dictature de Batista, le ministre de la justice était lié aux milieux d'affaire et au parti orthodoxe. Même chez les plus jeunes politiciens qui n'avaient pas encore participé à des gvts avant Batista, des politiciens, parfois qui avaient rallié la guerrila castriste dans les derniers mois, souvent aussi des anti-communistes, pas de quoi effrayer la bourgeoisie.
Les hommes qui avaient conduit l'armée (les frères Castro et Che Guevarra) n'ont pas de porte feuille ministériels, mais ont un grand poids dans le nouveau pouvoir, vu leur popularité dans la population cubaine.
Le nouveau pouvoir s'attèle à remettre en place les institutions étatiques désorganisées par la fin de la dictature.
C'est la continuité qui prédonmine.
Dans l'administration, pas de grands changements, aux finances par exemple les 2/3 des fonctionnaires ne changent pas.
Les présidents de la banque nationale et de la Banque de développement sont ceux qui les avaient dirigées avant la dictature de Batista.
Les magistrats ne changent pas (même s'ils étaient doublés de tribinaux révolutionnaires composés de membre de l'armée)
Les diplomates ne changent pas beaucoup non plus.
La police ne changea pas beaucoup, même si certains haut placés craignaient des répresailles et quittèrent leurs postes ou furent mutés.
L'armée s'était divisée entre ceux qui suivaient Batista et ceux qui se sont ralliés à Castro.
À la tête de la nouvelle armée, des proches de Castro, mais beaucoup de cadres et de militaires en place sous Batista.
Une armée bourgeoise avec un fonctionnement hiérarchisé obéissant à un commandement sur lequel ni les troupes ni la population n'avaient le contrôle.
Déstabilisation relative de l'économie : fuite des capitaux dans les derniers mois, mais guerre civile assez rapide et pas très destructive, les classes possédantes cubaines, les industriels et les financiers américains jouent le jeu et attendent de voir.

### Les premières crises et la réforme agraire de mai 1959

Mise en place de tribunaux exceptionnels pour répondre aux aspirations de justice de la population cubaine, ça ne plait pas aux conservateurs cubains et américains.
En février le premier ministre démissione.
En conférence de presse aux USA, Castro multiplie les déclarations anti-communistes pour montrer patte blanche.
Réforme agraire le 17 mai 1959.
-> les propriétés ne doivent pas dépasser 402 hectares pour la plupart des cultures et 1342 hectares pour riz et sucre.
La loi prévoyait cependant des indemnités pour les propriétaires expropriés.
Les terres sont redistribuées entre les paysans individuelles, des coopératives ou passaient aux mains de l'Etat.
Pas de quoi défriser les grands propriétaires fonciers, les compensations financières proposées leurs étaient favorables.
Cette réforme et les "expropriations" sont réalisées par les autorités et non pas les paysans directement.
Chute en bourse des compagnies sucrières, campagne anti-réforme agraire se met en place à Cuba et aux USA.
Crise gouvernementale, Castro démissione le 17 juillet estimant que le président Urrutia le bloquait dans sa politique et ne revient qu'une fois Urrutia remplacé.
Le 26 juillet il organise une grande manifestation, mais les crises continuaient.
En octobre un dirigeant militaire démissionne, puis le président de la Banque nationale, puis nouveau remaniement ministériels.
Désertion aussi des industriels, des propriétaires terriens, des techniciens, des intellectuels, qui quittent Cuba pour les Etats-Unis.
Grosse pression économique des USA qui menace de réduire le quota sucrier si les indemnisations ne sont pas plus favorables aux gros propriétaires expropriés.

## Les Etats-Unis multiplient pressions et menaces : Castro tient tête

Début 1960, Castro se tourne vers l'URSS, car constat qu'il se fait asphyxier par les USA.
Achat de centaines de tonnes de sucres, prêts de 100 millions de livres pour douze ans, livraison de pétrole et autres produits de base, mise en place d'usines avec matériel soviétique.
Ministre des finances démissione en mars, mais également départ dans les ministères et administration.
La presse américaine parle de préparatifs d'invasion contre Cuba par des exilés cubains hostiles au régime, qui sont entrainés et armés par les USA.
Les raffineries américaines à Cuba refusent de raffiner le pétrole soviétique, elles sont saisies par Castro.
Eisenhower, le président étasunien, réduit ses commandes de sucre.
En réponse Castro nationalise une partie des biens américains de l'île (compagnies téléphoniques, électriques, sucrières).
La réforme agraire s'accélère.
Le 13 octobre, embargo sur toutes les exportations américaines vers Cuba sauf certains produits médicaux ou alimentaires.
Le gouvernement cubain saisit de nouvelles usine en riposte.
Depuis janvier 1959, les capitalistes, qui avaient fait mine de collaborer avec le nouveau régime, avaient en réalité déjà fait passé une partie de leur argent à l'étranger, sans le réinvestir dans leurs usines.

### À la baie des Cochons : la tentative d'invasion échoue

Mise en place de l'opération soutenue par la CIA à la baie des Cochons : constituer un "territoire libre" et espérer rallier le reste de la population depuis là.
-> 16 avril 1961
Riposte de Castro : l'armée et la police fait 100 000 arrestations en 2 jours. Les milices envoyées par Castro contre l'invasion repoussent les assaillants.
C'est un échec cuisant pour les USA.


### Conclusion

Castro a eu le mérite de mener la réforme agraire et de ne pas céder aux pression américaines, allant jusqu'à réquisitionner des entreprises américaines.
Il a en fait bien été contraint de le faire à un certain moment quand le bras de fer a été engagé, mais ce n'était pas son but premier, d'ailleurs une partie de ses amis et des haut-placés du gouvernement n'y étaient pas favorables.
La population cubaine vit certainement mieux, et les inégalités de richesses sont moins criantes que dans tout un tas de pays de l'Amérique latine et ce malgré le blocus.
Mais si les ingalités de richesse sont objectivement moins importantes que dans d'autres pays, c'est surtout car la société cubaine est pauvre, les classes moyennes (médecins, avocats, architectes, etc) ont préféré s'exiler aux Etats-Unis.
Castro a voulu faire en sorte que son peuple vive mieux.
Mais il n'était pas internationaliste, et ne tenta pas de s'adresser ou de défendre les intérêts de la classe ouvrière, ni à Cuba ni par delà les frontières.
Il se dira communiste à partir d'un certain moment, mais il fait réference à l'idéologie stalinienne du socialisme dans un seul pays.
La révolution russe de 1917 a elle aussi échouer, mais ce ne sont pas les mêmes hommes et femmes qui ont pris le pouvoir en 1917 et en 1959.
Les hommes qui entouraient Castro souhaitaient l'avenement d'un capitalisme plus humain, plus cubain et patriote, mais ne souhaitaient pas changer fondamentalement la société cubaine et encore moins au delà.
Les révolutionnaires russes au contraire n'étaient pas des nationalistes, il ne se voyaient que comme un des maillons du prolétariat mondial, bien conscients que la révolution dans un pays arriéré économiquement et industriellement comme la Russie ne pourrait pas perdurer longtemps sans son extension rapide dans d'autres pays.
La révolution russe a lancé une vague révolutionnaire en Europe, qui malheureusement ont échoué, mais la preuve a été faite qu'une révolution ouvrière peut largement dépasser les frontières et trouve de l'écho dans les masses exploitées d'autres pays.

### Annexe : le mouvement ouvrier cubain jusqu'en 1952

PC Cubain créé en 1925
Grosses grèves dans les années 1930, classe ouvrière combattive sur laquelle il aurait été possible de s'appuyer
[À creuser avec exemples]





# Cuba, 35 ans après la révolution castriste (1994)

### Intro

Nous sommes du côté de Cuba contre l'impérialisme américain, car nous sommes du côté des pays sous-développés qui tentent d'échapper à l'emprise de l'impérialisme.
La misère à Cuba est de la responsabilité de l'impérialisme.

### La dégradation actuelle de la situation économique

4/5 des échanges avec des pays du bloc de l'Est, donc à la chute de l'URSS grosse crise économique.
Cuba ne peut plus s'appuyer sur des alliés qui achetaient du sucre à prix favorable.
La Russie ne vend plus de pétrole à prix préférentiel.
Problème énergétique, beaucoup de cubains se sont mis au vélo faute de pétrole !
Rationnement, problèmes d'approvisionnement, mesures d'austérité du gouvernement pour surmonter la crise.

### Le départ des "balseros"

Dans les années 1990, des milliers de cubains tentent chaque années de rejoindre clandestinement les Etats-Unis, qui en profite et joue sur la corde du "le régime cubain ne laisse pas les gens s'expatrier chez nous".
À ce moment-là, Castro change son fusil d'épaule et décide de ne plus s'opposer au départ des "balseros", et des dizaines de milliers de cubains en profite pour tenter leur chance.
Le gouvernement américain retourne alors sa veste et oriente les exilés vers leur base militaire de Guantanamo Bay, qui est en réalité ... à Cuba, bien qu'au main des USA.

### La révolution de 1959

C'était les américains qui tenaient l'île économiquement et politiquement entre leurs mains.
Ils se contentaient d'ailleurs très bien d'un Batista.

### La période de la main tendue à la bourgeoisie et aux américains

Première friction avec les USA au moment des procès des tortionnaires à l'époque du régime de Batista.
Des procès publiques et violents, des exécutions et emprisonnements à vie (bien moins que sous le régime Batista).
Voyant que les relations avec les USA se compliquaient il entama un voyage médiatique là-bas, durant lequel il rejette le communisme et explique que "Les Etats-Unis et Cuba ont toujours maintenues d'étroites relations. Il n'y a pas de raison que celles-ci ne s'améliorent pas de jour en jour".

### La rupture avec les USA

### Le rapprochement avec l'URSS

Rapprochement amorcés en 1960 plus d'un an après la révolution, avec des accords commerciaux sur les exportations de sucre.
Les USA font pression auprès des autres pays de l'Amérique latine pour condamner et isoler Cuba.

### La tentative d'invasion de 1961

Des marquis armés par la CIA au centre de Cuba.

### Le développement de l'économie et la militarisation du travail

Rapidement une campagne d'alphabétisation et développement du système de santé, construction de cliniques et d'hôpitaux.
Tentative d'industrialiser le pays, de diversifier la production.
Mais c'est un processus venu d'en haut, les masses ouvrières du pays ne sont pas vraiment consultées.
Bâtir une industrie qui rivalise avec celle des grandes puissances, à partir de quasiment rien, c'est une tâche impossible dans le cadre nationale.
Même la Russie soviétique avec sa superficie et ses ressources naturelles n'a pas pu rivaliser avec le niveau de développement des grandes puissances impérialistes.

### La "crise des fusées" et les menaces constantes des USA

En 1962, les Soviétiques profitent de la proximité de Cuba avec les USA pour installer des missiles, c'est la "crise des fusées".
Mais face au chantage américain, Khrouchtchev le dirigeant de l'URSS fait marche arrière et retire les missiles sans même consulter Castro.

### Vers l'intégration économique avec les pays de l'Est

L'industrialisation patine, à cause du poids du budget militaires et toutes les contraintes économiques liées à l'isolement de l'île.
Fin 1963, Castro réoriente l'économie vers la canne à sucre à nouveau, pour obtenir des capitaux.

### Le "nouveau" parti communiste cubain

Dans les années 1960, Castro fusionne différente organisations pour refonder un parti "communiste", un parti unique dont il est à la tête.

### Enthousiasme populaire et guerillas

La résistance de Cuba aux USA est très populaires en Amérique latine et plus généralement dans les pays du Tiers-Monde.
Entre 1960 et 1967, des guerillas touchent une vingtaine de pays d'Amérique latine qui tentent de suivre l'exmple cubain.
Les guérilleros cubains avaient su trouver la sympathie des paysans locaux et étaient soutenus par la population, après plusieurs tentatives infructueuses d'ailleurs.
Mais cela n'avait rien d'automatique, et ces méthodes ne marchèrent pas dans les autres endroits, les groupes armées se sont fait majoritairement écraser.
Exemple avec Che Guevarra parti essayer la même méthode en Bolivie mais lui et ses hommes furent massacrés.
Il avait d'ailleurs juste avant démissioné du gouvernement cubain, et sa perspective d'impulser des luttes d'émancipation dans le reste des pays d'Amérique latine n'était pas partagée par Castro qui souhaitait les soutenir uniquement s'il pouvait en obtenir quelque chose par la suite.

### Le soutien à certains régimes latino-américains et à ceux des pays de l'Est

Les prises de position internationale de Castro étaient avant tout liées aux intérêts cubains, et il ne voulait donc pas froisser son allié soviétique.
Il approuve par exemple l'intervention en Tchécoslovaquie pour faire cesser le "Printemps de Prague".
Dans les années 1970, soutien parfois des Etats d'Amérique latine qui veulent bien faire affaire, mais aussi de certaines guerillas.
Une seule ligne politique : défendre les intérêts nationaux de Cuba.
Le soutien aux peuples du Tiers-Monde et aux guerillas est en fait subordonné à la défense de Cuba.

### L'ouverture aux investissements étrangers

### Du côté de Cuba !

Un Etat qui lutte pour préserver son indépendance face à l'impérialisme.
Nous sommes pour cette raison du côté de Cuba !






## Docu Arte : Cuba, la révolution et le monde

1961/62 : grosses tensions entre Kennedy et Castro
Embargo qui dure encore aujourd'hui
Che Guevarra, anti impérialisme, pays du Sud
Changement de stratégie vers les années 2000 : se rapprocher du Venezuela (car plus URSS, avoir du pétrole moins cher etc)


https://mensuel.lutte-ouvriere.org/documents/archives/la-revue-lutte-de-classe/serie-actuelle-1993/article/cuba-une-tentative-desesperee-d

2021
https://www.convergencesrevolutionnaires.org/A-propos-des-recentes-manifestations-a-Cuba
https://www.convergencesrevolutionnaires.org/D-ou-vient-la-contestation-a-Cuba

2016
https://www.convergencesrevolutionnaires.org/Lider-maximo-ma-non-troppo

2004
https://www.convergencesrevolutionnaires.org/Dans-notre-courrier-Cuba-entre-le-marche-et-la-planification

2002
https://www.convergencesrevolutionnaires.org/Quelques-apercus-de-la-situation-a-Cuba

2000
https://www.convergencesrevolutionnaires.org/Des-trotskystes-cubains-nous-communiquent
