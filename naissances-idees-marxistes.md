# La naissance des idées marxistes : "projection" du film Le jeune Karl Marx

## Introduction

Durant le 19ème siècle, le nouvel ordre capitaliste pousse les ouvriers à
s'organiser face aux patrons et aux conditions de travail infernales. Le
prolétariat se retrouve bientôt sur les devants de la scène, et les courants
socialistes se développent au rythme des insurrections, comme celles de 1848. Un
de ces courants dont nous nous revendiquons, le marxisme, naît dans cette
période trouble.

Le film de Raoul Peck "Le Jeune Karl Marx" retrace la jeunesse de Marx et
Engels, à l'époque où ils élaborent leurs idées et commencent à interagir avec
les autres penseurs et dirigeants du mouvement ouvrier de l'époque. Il revient
sur leurs premières collaborations, suivant les exils forcés de Marx à travers
plusieurs pays d'Europe, et sur les embryons d'organisation internationale des
ouvriers. En toile de fond, et au delà de l'aspect biographique, ce long-métrage
évoque la naissance des idées marxistes.

## L'apparition des idées socialistes

### Les idées ne viennent pas de nulle part

Revenons un peu avant Marx, à l'époque des Lumières. Les idées des Lumières sont
progressistes à leur époque : elles énoncent qu'il faut tout baser sur la
raison, y compris l'organisation de la société et l'État. Ces idées, en
opposition aux idées religieuses et à la monarchie absolue, vont créer une base
théorique permettant à la bourgeoisie de rallier paysans, artisans,
sans-culottes, etc, c'est-à-dire les forces sociales nécessaires pour renverser
l'ancien ordre social pendant la Révolution française.

Mais force est de constater que la Révolution, malgré les aspirations
progressistes qu'elle a porté, n'est pas venue à bout des inégalités.

Au delà des espoirs qu'elle a suscité à travers le monde, elle a surtout eu pour
rôle de sacraliser la propriété privée des moyens de production, socle de la
société capitaliste, en l'inscrivant même dans la fameuse déclaration des droits
de l'Homme et du citoyen : _le but de toute association politique est la
conservation des droits naturels et imprescriptibles de l'Homme. Ces droits sont
la liberté, la **propriété**, la sûreté, et la résistance à l'oppression
(Article 2)_.

### Des idées socialistes inabouties : le socialisme utopique

Dés le début du 19ème siècle, les contradictions exploiteurs/exploités vont
amener les penseurs à remettre en question la nouvelle organisation sociale,
c'est-à-dire l'organisation capitaliste de la société. Les premières idées
socialistes naissent avec le socialisme utopique, qui s'appuie sur les principes
des philosophes des Lumières du 18ème siècle.

La production encore peu développée, et un capitalisme encore à ses
balbutiements produisent alors, à cette époque, des théories socialistes
imparfaites chez St Simon, Fourrier et Owen notamment. Mais il leur était
impossible de réellement changer la société, car il leur manquait l'analyse du
rôle de la classe ouvrière qui a intérêt au renversement du système capitaliste.

## Le matérialisme historique (l'Histoire est l'histoire de la lutte des classes)

Pour produire une critique du capitalisme et comprendre comment le renverser,
Marx et Engels développent une méthode d'analyse et une façon de comprendre le
fonctionnement des sociétés humaines : le matérialisme historique.

Ils analysent qu'à chaque période, le mode de production et d'échange d'une
société produit une certaine ditribution des richesses, et donc une division de
la société en classes sociales aux intérêts antagonistes.

Pour citer Engels : "la structure économique d'une société donnée forme toujours
la base réelle qui permet, en dernière analyse, de comprendre toute la
superstructure des institutions juridiques et politiques, aussi bien que des
idées religieuses, philosophiques et autres qui lui sont propres".

Cette analyse engendre une conception de l'Histoire à travers l'Histoire de la
lutte des classes.

À l'opposé d'une conception de l'Histoire comme une suite de grands évènements
du fait de quelques grands individus, rois, reines, empereurs, présidents, etc,
l'Histoire est vue comme découlant des rapports économiques entre classes
sociales. Ces rapports économiques peuvent être étudiés, analysés. C'est un des
buts du matérialisme historique, rendu possible par le développement des
sciences sociales comme l'économie politique, l'anthropologie, etc.

Les groupes sociaux ont différents interêts et différentes formes d'actions à
leurs disposition, selon leur place dans l'appareil de production et leur
rapport à la propriété. C'est pour cela qu'en plus d'apporter une analyse de la
société, le socialisme scientifique développé par Marx et Engels apporte des
éléments pour mettre au point un programme pour l'émancipation des
travailleurs. C'est d'ailleurs ce que s'efforceront de faire les deux penseurs
au long de leur vie, en s'impliquant dans la construction d'organisations
politiques de la classe ouvrière, comme cela est montré dans le film.


## Conclusion

Les analyses marxistes, loin de nous laisser simples spectateurs de la société
d'exploitation, nous donnent des outils pour agir et pour la renverser, car
comme le dit Marx, en critiquant certains courants philosophiques le précédant,
"Les philosophes n'ont fait qu'interpréter le monde, mais ce qui importe c'est
de le transformer".


## À lire pour aller plus loin

[Socialisme utopique et socialisme scientifique, F. Engels](https://www.marxists.org/francais/marx/80-utopi/index.htm)

[Les Trois Sources du Marxisme, K. Kautsky](https://www.marxists.org/francais/kautsky/works/1908/00/kautsky_19080000.htm)

Marx et Engels, David Riazanov
