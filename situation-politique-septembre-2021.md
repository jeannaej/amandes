# Message d'invitation

Ces derniers mois, nous avons assisté à de nombreux phénomènes spectaculaires et meurtriers : incendies, inondations, ouragans et bien sûr une épidémie mondiale.
Même si ces évènements sont parfois inévitables, ils sont favorisés par l'aggravation de la crise écologique, et leurs répercussions sont gérées à chaque fois par des gouvernements peu intéressés par le sort des populations les plus exposées aux risques.
Écologie, accès à la nourriture ou aux soins : comment s'assurer un avenir meilleur à l'heure où le capitalisme semble incapable de répondre aux besoins les plus simples de l'humanité ?

# Urgence : changeons de société

# Introduction

Ces derniers mois, la situation dans le monde a pris un aspect parfois presque "apocalyptique".
En juillet l'Allemagne, la Belgique et les Pays-Bas étaient touchés par des inondations d'une ampleur inédite, causant des dizaines de morts et des dégâts très importants dans les habitations de milliers de personnes.
Puis cet été, des feux ont ravagé des centaines de milliers d'hectares de forêts dans le pourtour méditerranéen : dans le Sud de la France, en Grèce, en Turquie, en Algérie, mais aussi en Russie et dans l'Ouest des USA notamment.
Des sécheresses au Sud de Madagascar ont conduit à de nombreux morts par famine.
Il y a quelques semaines, un ouragan a traversé Haïti, un des pays les plus pauvres du monde et également touché par un séisme extrêmement meurtrier, avant d'attendre le Mexique et la Nouvelle Orléans.
Et puis bien sûr il y a cette épidémie qui n'en finit plus, et qui a déjà décimé plus de 5 millions de personnes, sans compter le nombre de personnes qui sont passées en réanimation, etc
Lister tout ces épisodes n'a pas pour but de démoraliser, mais plutôt de prendre conscience qu'à chaque problème, les solutions apportées par nos dirigeants ne sont pas du tout à la hauteur pour les populations directement impactées et on va se demander comment faire changer ça !

# La lutte nécessaire pour l'écologie

## Une situation de plus en plus préoccupante

Même si on aura l'occasion de revenir dessus plus en détail (probablement sous forme d'un topo dédié à ça), impossible de ne pas parler de la crise écologique quand on voit les déchainements météorologiques et les rapports du GIEC qui s'accumulent sans que ne s'enclenche un véritable changement de société.

Déjà un constat : cet enchaînement d'évènements météorologiques spectaculaires et d'incendies cet été dans le monde ont été favorisés par le réchauffement climatique, et ces évènements sont amenés à se répéter et s'aggraver tant qu'il n'y aura pas une diminution drastique des émissions de gaz à effets de serre.

Il y a quelques semaines, le GIEC, groupe d'experts intergouvernementaux sur l'évolution du climat, a publié un nouveau rapport qui permet de mesurer l'ampleur du désastre climatique vers lequel on se précipite si rien n'est fait.
Ainsi, ils estiment que "dans le cas d’un réchauffement planétaire de 1,5°C, les vagues de chaleur seront plus nombreuses, les saisons chaudes plus longues et les saisons froides plus courtes. Avec une  haussede  2°C,  les  chaleurs  extrêmes  atteindraient  plus  souvent  des  seuils  de  tolérance critiques pour l’agriculture et la santé publique."

En plus des augmentations de températures qui rendent des zones de moins en moins vivables, la montée des eaux va également boulversé des régions entières du globe et forcer des centaines de millions de personnes à se déplacer.

Cerise sur le gâteau, des liens ont été établis entre la déforestation et la propagation de virus des animaux vers les humains, impliquant que des épidémies comme celle du covid-19 risque de se produire d'autant plus fréquemment que la destruction des forêts est importante.

On peut également mentionner des catastrophes écologiques qui ne sont pas directement liées au réchauffement climatique, comme les fréquentes marées noires qui polluent les océans et les côtes, l'incendie dans l'usine Lubrizol de Rouen et l'explosion dans le port de Beyrouth qui en plus de causer de nombreux morts et bléssés ont libéré des produits nocifs dans l'air.

Même si le tableau n'est pas reluisant, on peut au moins noter que ces aggravations des conditions de vie ne sont pas nécessairement inévitables puisqu'elles sont causées en grande partie par des mécanismes bien connues liés à la façon dont sont organisées les activités de l'humanité actuellement.

Et c'est bien cette organisation qui est la cause des problèmes, non pas les progrès techniques ou l'augmentation de la population comme on l'entend parfois.
Par exemple, l'incendie de l'usine Lubrizol de Rouen en 2019, qui avait libéré un immense nuage de fumée noire chargé de substances toxiques, n'était pas vraiment un hasard.
En 2013, sur le même site, une fuite de gaz toxique, le mercaptan, a été ressentie jusqu’en région parisienne et en Angleterre !
Le manque d’entretien et de surveillance était alors pointé du doigt.
La justice a condamné l’entreprise, qui réalise six milliards de dollars de chiffre d’affaires, à une simple amende de 4 000 euros.

À chaque fois, l'Etat fait l’autruche et supprime les contraintes pour les industriels pollueurs au mépris des travailleurs et des riverains.
Sur les 2 100 arrêtés de mise en demeure pris chaque année contre les industriels, seuls 20 % débouchent sur des sanctions administratives, souvent symboliques.
Dans le même temps, les inspections de sites ont été divisées d’un tiers en douze ans, du fait des suppressions de postes dans la fonction publique.

## Le capitalisme vert et la fin des touillettes de café

En regardant de plus près, rien d'étonnant à ce que les Etats rechignent à condamner les industriels pollueurs.
Pour ça, il faudrait qu'ils soient près à s'attaquer à la loi du profit.
Mais cela remettrait en cause tout notre système économique, quand le rôle des Etats sous le capitalisme est justement de maintenir ce système économique à flot.
C'est ainsi que les COP, ces sommets mondiaux pour le climat, s'enchainent et se ressemblent, avec leurs lots de promesses dans le vent : neutralité carbone d'ici quelques dizaines d'années par-ci, taxe ridicule sur les gros pollueurs par-là, avec à chaque fois aucune réelle volonté de mettre à application ces mesures, aussi dérisoires soit elle.

Mais il n'y a pas que les Etats qui se repeignent en vert : les grandes entreprises aussi tentent de surfer sur la prise de conscience écologique d'une frange importante de la population.
Incorporant de nouveaux visuels plus "vert", arborant fièrement des listes d'ingrédients supposés plus respectueux de la nature, etc, il n'est pas rare de voir jusque les firmes les plus polluantes de la planète (énergie, textile, automobile, etc) mettre le paquet pour changer leur image auprès du grand public.
Il n'y a en général pas à gratter loin sous le vernis pour se rendre compte de la supercherie.
On peut penser par exemple au dieselgate : entre 2009 et 2015, le constructeur de voiture Volkswagen avait utilisé plusieurs techniques visant à réduire frauduleusement les émissions polluantes de ses voitures durant les essais d'homologation des véhicules.
On peut également citer la marque H&M qui s'est fait épingler lors de la commercialisation de sa collection "Conscious", censée être fabriquée à partir de matériaux recyclés, alors qu'ils ne representaient en réalité qu'une petite partie du vêtement.

L'argument écologique est même parfois utilisé par les patrons pour justifier des licenciements, comme chez Renault ou Total.
Et quand l'Etat verse des subventions "vertes" à des grandes entreprises, avec promesse de réorienter la production de façon plus écologique, cet argent est souvent une aubaine pour des réorganisations sous forme de diminution du nombre de postes avec les lourdes conséquences sur l'emploi évidemment.
Leur "conscience écologique", sous forme d'argument de vente ou de motif de licenciement, n'est heureusement pas la même conscience écologique que la majorité des personnes qui sont inquiètes pour leur avenir.

## L'impasse du repli nationaliste

Un autre exemple d'entreprises qui prétendent s'adapter aux enjeux écologiques sont celles qui font apparaitre des labels "fabriqué en France" sur leurs produits.
Dans la même veine on retrouve les défenseurs de la relocalisation de l'industrie en France.

S'opposer à la fermeture d'un site et aux suppressions d'emplois qui en résultent est une chose (une bonne chose bien évidemment), mais faire des plans de réindustrialisation et s'essayer aux conseils d'investissement pour le patronat pour produire plus français est une toute autre chose.
Sans même discuter de la faisabilité et des gains écologiques d'une telle démarche, en comparaison des gains d'échelle permis par le regroupement de certaines industries dans certaines régions, demander à des industriels, souvent à coup de subventions et de zones franches, d'investir plus en France que dans d'autres pays, c'est un peu donner le baton pour se faire battre.
Car au final, c'est bien eux qui gardent tout le pouvoir de décision sur l'organisation de la production : combien de salariés ils embauchent, dans quelles conditions, pour produire quoi, de quelle façon etc.
Et puis cela nous fait rentrer dans le jeu de la concurrence entre travailleurs.
Car en plus de renforcer la montée des idées xénophobe (type "ils nous volent notre travail"), nous pensons que seul la solidarité entre les travailleurs par delà les frontières nous permettra de résoudre les problèmes qui se posent à l'échelle internationale tel que l'écologie.

## L'écologie contre les pauvres ?

Un autre aspect de ce capitalisme vert est la mise en avant de la responsabilité individuelle dans la catastrophe écologique.
Chacun est rendu responsable de ses choix, sa consommation, son mode de vie, etc, quand bien même tout ça est en réalité en grande partie contraint.
En effet, quand il s'agit de trouver un logement près de son lieux de travail, a-t-on vraiment la possibilité d'en choisir un en fonction de la qualité de son isolation ?
Choisi-t-on les produits que l'on trouve dans les supermarchés ?
Décide-t-on de l'aménagement des réseaux de transport en commun, des infrastructures de production d'énergie ?

Cette écologie sauce capitalisme consiste à s'attaquer aux plus pauvres directement au portefeuille sans jamais remettre en cause la façon totalement irrationnelle dont est organisée la production ; quelques exemples d'aberations : produits qui font plusieurs fois le tour du monde pour être assemblés, l'obsolescence programmée, les innovations largement freinées quand elles entrent en conflit avec les intérêts des grands industriels (ex dans le secteur de l'énergie), etc.

# Crise sanitaire : le capitalisme incapable de nous soigner

## La gestion capitaliste retarde l'accès des populations aux vaccins

Puisqu'on en est à parler d'irrationnalité du capitalisme, on va discuter de la crise sanitaire.
Dans la gestion des hôpitaux, d'abord, on voit très rapidement les limites de la gestion comptable des gouvernements successifs, pour qui le personnel et le matériel sont d'abord vu comme des coûts.
Alors même que les soignantEs dénonçaient le sous-effectifs constant et le manque de moyens depuis des années, le gouvernement n'a pas levé le petit doigt pour ces travailleurs de première ligne, qui ont du faire face à l'épidémie de Covid-19 avec les moyens du bord.
Il faut dire que dans cette société, la santé est soumise à la loi du marché, alors rien d'étonnant à ce que les hôpitaux ne soient pas gérés en fonction des besoins de la population.

Alors que la vaccination à l'échelle mondiale permettrait de réduire considérablement la transmission du virus et le nombre de victimes, là encore le mode de production capitaliste a été et continue de constituer un des freins majeurs de l'accès au vaccin pour les populations.
Que cela soit dans la recherche, la fabrication ou la distribution des vaccins, mais également la répartition des produits médicaux comme les masques et les appareils respiratoires, tout a été fait de façon cloisonnée et irréfléchi.

Quelques exemples peut-être.
D'abord les brevets sur les vaccins.
Les vaccins contre le Covid mobilisent des centaines de brevets (achetés par les laboratoires), chacun concernant une technologie précise utilisée dans le processus de production.
Évidemment la coopération serait bien plus efficace, il faudrait le partage des ingrédients des vaccins bien sûr, mais également des transferts de technologies, une réquisition des infrastructures de production, de fabrication des matières premières, de personnel et des chaînes logistiques.
Sans quoi les labos se réservent un monopole et limitent la production pour faire monter les prix.

Un autre exemple : les énormes disparités de la vaccination dans le monde.
Pour protéger les personnes les plus vulnérables, mais également se prémunir d’apparitions de nouveaux variants, il est nécessaire, entre autres, de vacciner rapidement dans tous les pays et pas uniquement les plus riches.
Mais les disparités économiques entre pays se retrouvent aussi dans l’accès aux vaccins : si 40 % de la population mondiale a reçu au moins une dose, ce chiffre tombe à 1,8 % seulement parmi les pays les plus pauvres.
Dans un récent article de la revue Nature, des chercheurs estiment qu’à ce rythme, la plupart des habitants de ces régions devront attendre 2023 pour être vaccinés !
Et pourtant les Etats des pays les plus riches continuent de s'accaparer toutes les doses pour pouvoir rouvrir l'économie au plus vite.

Pourtant, une étude menée par l’association américaine Public Citizen démontre qu’il est possible de vacciner la population mondiale en un an, avec seulement 22,8 milliards de dollars pour produire et administrer le Moderna, ou 9,4 milliards de dollars pour le Pfizer !
À comparer avec les dizaines de milliards de budget attribué chaque année à l'armée française par exemple...
Drôle de sens des priorités.

## Le gouvernement choisit les profits sur la santé des gens

Le gouvernement affirme à qui veut l'entendre qu'il n'était pas possible de prévoir une telle épidémie, qu'il n'est pas possible de former des soignants en quelques mois, qu'il a fait du mieux qu'il pouvait.
Il serait exempt de tout reproche ?
Réduire les budgets dans la recherche quand elle ne rapporte pas assez, comme c'est le cas avec la LPR passée l'année dernière, par exemple, c'est réduire aussi les possibilités d'étudier les virus et de gagner un temps précieux si une épidémie se répend.
Est-ce-que tenter de se déresponsabiliser en pointant du doigt la population et les travailleurs qui ne respecteraient pas assez les gestes barrières ne serait pas une opération politique plutôt qu'une bonne façon de lutter contre le Covid ?
Est-ce-que rendre les test payants et ne pas embaucher dans l'éducation nationale permettent d'endiguer la propagation du virus ?
À la place des embauches et des titularisations nécessaires dans les hôpitaux, on a des soignantEs épuiséEs qui démissionnent.
Plutôt que de chercher à protéger les salariés de l'épidémie, le gouvernement a sorti des centaines de milliards d'euros d'argent magique pour le grand patronat.
Toutes les obligations et les sanctions ce sont les travailleurs qui se les sont prises, jamais les directions des entreprises.
Alors quand le gouvernement nous explique qu'il a fait de son mieux, on a de quoi être enragé.

Pourtant on aurait pu les trouver les moyens contre la propagation de l'épidémie, en s'attaquant aux profits des trusts pharmaceutiques par exemple.
D'ailleurs il est apparut clair pour une bonne partie de la population qu'il y a un problème à ce que des entreprises se fassent de l'argent avec la commercialisation des vaccins, tels des charognards.

# Conclusion : face à la crise mondiale il faut la révolution

Le capitalisme se montre incapable de gérer les principaux enjeux de l'humanité, il est urgent de dégager ce système.
Nous sommes exclus de la prise de décisions qui nous concernent directement et tant que nous n'aurons pas les manettes de l'économie nous n'aurons pas vraiment notre sort en main.
Alors pour faire face aux problèmes auxquels nous sommes confrontés il nous faut un changement radical de société, un changement qui ne viendra pas d'en haut, de nos dirigeants.
Contrairement au gouvernement, certains travailleurs, sans attendre l'aval de leur direction, ont pris leurs responsabilités face à la crise.
À la Poste, dans les hôpitaux, l'éducation nationale, des salariés se sont battus pour imposer des conditions sanitaires décentes dés le début de l'épidémie.
En se mettant en grève ou en droit de retrait, ils ont réagi collectivement contre leurs conditions de travail, imposées d'en haut.
Que cela soit dans les industries polluantes, dans les services, dans l'éducation, dans la santé, etc, les travailleurs sont capables de gérés collectivement la production d'une façon beaucoup plus efficaces que les patrons.
Le contrôle des entreprises par les salariés est nécessaire pour que l'organisation de la société soit orientée vers la satisfaction des besoins de toute l'humanité.
Pour ça, il faudra contester à la classe capitaliste la propriété des moyens de production, et ça seule une lutte d'ensemble peut l'imposer.


# Sources
- Rapport du GIEC : https://www.ipcc.ch/site/assets/uploads/2021/08/IPCC_WGI-AR6-Press-Release_fr.pdf
- https://www.convergencesrevolutionnaires.org/Face-a-l-urgence-climatique-une-seule-solution-la-revolution?navthem=1
