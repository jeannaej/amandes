# Les luttes d'indépendances en Algérie

https://mensuel.lutte-ouvriere.org//1972/07/01/internationalisme-nationalisme-et-internationale_108446.html

Au début du 20ème siècle, les grandes puissance imépérialistes ont colonisé une large partie du globe.

Après la seconde guerre mondiale, tout un tas de révolution dirigées contre les puissances impérialistes : Afrique, Amérique latine et Asie.

Les organisations, certes faibles, de la IVème Internationale n'ont pas été capable de mener une politique dans ces luttes.
Soutien des organisations nationalistes petite-bourgeoise comme le FLN algérien et le FNL vietnamien.
Au contraire, il faut lutter pour l'indépendance et les revendications démocratiques bourgeoises liées (libertés démocratiques, réforme agraire), mais sans se mettre à la remorque de ces organisations.
Il faut militer pour un anti-impérialisme qui s'attaque à la bourgeoisie dans une perspective internationaliste et pas pour un anti-impérialisme qui se limite à la revendication d'un Etat national.

Les possibilités de réorganisations de la société dans les pays riches sont particulièrement importantes, mais il ne s'agit pas pour les pays pauvres d'attendre la révolution dans les pays développés.
La situation explosive dans ces pays est propice à donner une impulsion révolutionnaire aux exploités.

L'absence d'Internationale fait défaut pour unir les travailleurs des pays opprimés et des pays oppresseurs.

Dans une lutte d'indépendance, il faut combattre au côté de toutes les franges de la population qui se battent contre l'oppression nationale mais militer pour que le prolétariat prenne la tête de cette lutte.

https://mensuel.lutte-ouvriere.org//1972/06/01/en-soutenant-la-lutte-du-peuple-vietnamien-faut-il-cautionner-la-politique-du-fnl_108442.html

Polémique avec la Ligue Communiste

Suivisme de la 4ème Internationale vis-à-vis des organisations nationalistes bourgeoises ou petites-bourgeoises, staliniennes ou non, en y voyant des vertus prolétariennes.
Des directions de partis staliniens ou aux mouvements nationalistes petit-bourgeois auraient selon eux remplis un rôle de direction de révolutions socialistes.
Exemple au Vietnam : politique de "Front national" qui regroupe le Parti Populaire Révolutionnaire (PPR), des représentants d'un "parti démocrate", d'un parti "radical-socialiste", des sectes religieuses, des minorités nationales et divers organisations de masse.
Mais pas d'indépendance de classe pourtant primordiale.

Où est donc la position de militants censés défendre les intérêts historiques du prolétariat contre tous ceux qui tentent de les dévoyer ? Comment serait-il possible d'éduquer une avant-garde prolétarienne, en mettant en avant une telle politique frontiste nationaliste ? Comment pourrait-on, de cette façon, préparer le prolétariat vietnamien non seulement à jouer un rôle dirigeant dans la lutte d'émancipation nationale, mais à exercer son propre pouvoir, une fois l'indépendance obtenue ?


https://mensuel.lutte-ouvriere.org/documents/archives/la-revue-lutte-de-classe/serie-1986-1993-trilingue/article/politique-des-nationalistes

Mais pour l'Internationale Communiste, même la réalisation conséquente de transformations économiques, sociales et politiques qui, à une certaine époque historique, se sont faites sous la direction politique de la bourgeoisie ou en tous les cas à son profit, exigeait à notre époque, que dans les pays arriérés le prolétariat révolutionnaire prenne la direction de la révolution.

Le nationalisme bourgeois et l'intermédiaire stalinien

Par dela la diversité des situations, il y a un certain nombre de traits en commun dans la politique de toutes ces organisations. Directement ou indirectement, elles ont toutes puisé chez Mao, qui a en quelque sorte codifié ce qu'une direction nationaliste bourgeoise pouvait faire devait faire, ou ne devait surtout pas faire pour canaliser et dominer un mouvement révolutionnaire de masse. Mao leur a fourni, en quelque sorte, clé en main :

- une idéologie : d'abord nationaliste mais se prétendant socialiste,

- une stratégie d'unité nationale : le bloc des classes (dites révolutionnaires) contre la lutte des classes,

- un modèle de pouvoir : le parti unique (ouvertement ou sous le couvert d'un « front » ),

- une tactique de conquête de pouvoir : création, par l'utilisation de méthodes terroristes dans les villes ou/et par la mise sur pied d'une armée de guérilla dans les campagnes, d'un appareil militaire auquel les masses qui s'engagent dans la lutte sont obligés de se rallier sans pouvoir le contrôler,

- l'utilisation d'un danger extérieur, réel ou artificiellement attisé, pour justifier, une fois au pouvoir, les sacrifices demandés aux masses, ainsi que la dictature et l'absence de libertés.

https://journal.lutte-ouvriere.org/2020/08/26/lindependance-sous-controle-des-colonies-africaines_151116.html

https://journal.lutte-ouvriere.org/2012/07/04/il-y-50-ans-le-5-juillet-1962-lindependance-pour-lalgerie-mais-pas-lemancipation-sociale-des_27747.html

https://www.convergencesrevolutionnaires.org/La-guerre-d-independance
https://www.convergencesrevolutionnaires.org/Les-manifestations-de-decembre-1960-en-Algerie
https://www.convergencesrevolutionnaires.org/Le-FLN-declare-la-guerre-au-colonialisme

https://www.convergencesrevolutionnaires.org/60-ans-apres-les-independances-africaines-ou-en-est-on

https://www.lemonde.fr/international/article/2021/03/17/guerre-d-algerie-le-tabou-des-viols-commis-par-des-militaires-francais_6073395_3210.html
https://www.lutte-ouvriere.org/documents/archives/cercle-leon-trotsky/article/le-colonialisme-1830-1914
https://www.lutte-ouvriere.org/documents/archives/cercle-leon-trotsky/article/la-gauche-et-les-guerres
https://journal.lutte-ouvriere.org/2000/04/14/tunisie-une-independance-que-le-peuple-tunisien-dut-arracher_1223.html
https://journal.lutte-ouvriere.org/1999/12/31/il-y-50-ans-lindependance-de-lindonesie_658.html
https://journal.lutte-ouvriere.org/2010/07/01/30-juin-1960-lindependance-du-congo-belge_22683.html
https://journal.lutte-ouvriere.org/2000/06/30/il-y-40-ans-30-juin-1960-lindependance-du-congo_1581.html

# CLT : Il y a 50 ans, la fin de la guerre d’Algérie : la fin du colonialisme, mais pas de l’oppression
https://www.lutte-ouvriere.org/documents/archives/cercle-leon-trotsky/article/il-y-a-50-ans-la-fin-de-la-guerre

## Cent trente-deux ans de barbarie coloniale

1830 : débarquement des troupes françaises à Alger
Dans les décennies qui suivent, guerre, révoltes, les terres sont volés et appropriées par des colons, en général à des capitalistes, famines
Colonie de peuplement ("pieds noirs")

1881 : code de l'indigénat, infractions spécifiques pour les Algériens

## Le mouvement communiste et la lutte anticoloniale

Une des 21 conditions de l'adhésion à l'IC : mener réellement la lutte contre le colonialisme
Dans ses premières années, le PC dénonçait le système colonial et s'affirmait pour l'indépendance de toutes les colonies.
En octobre 1925, le PC organise la première grève politique de plusieurs dizaines de milliers de travailleurs contre une guerre coloniale.

Dans les années 1930, des partis bourgeois jusqu'au PC algériens et français, tous réclament l'assimilation (i.e. l'égalité des droits) mais pas l'indépendance, exception : l'Étoile nord-africaine.

## Le Front populaire : la trahison des peuples colonisés

## De la Seconde Guerre mondiale à Sétif  : la révolte gronde en Algérie

Des milliers d'Algériens sont enrollés dés 1943 pour aller libérer l'Europe
À travers cette guerre, ces hommes purent mesurer l'affaiblissement de l'impérialisme français

1er mai 1945, manifestation pour l'indépendance de l'Algérie, réprimées par le gouvernement de De Gaulle, le CNR (conseil national de la résistance)
8 mai, manif pour la fin de la guerre, un homme sort un drapeau algérien et est tué par la police, déclenche une révolte
Répression atroce, village brûlés, assassinats, viols, entre vingt mille et quarante mille morts du côté algérien
Tout ça avec le soutien du PCA et du PCF qui étaient au gouvernement

1947 : droit de vote pour les algériens, mais avec un statut particulier, pas beaucoup de représentants, intimidations dans les bureaux de vote, arrestations de militants
En parallèle, beaucoup de jeunes révoltés par la situation rejoignent les rangs des nationalistes

## Le FLN se lance dans la lutte armée...

## ... et dans la construction d'un nouvel appareil d'État

FLN et MNA se font la guerre à partir de l'été 1955
FLN plus puissant militairement notamment grâce au soutien des pays arabes (surtout Egypte)
MNA plus puissant à Alger et dans la CO immigrée en France
Assassinats de militants des 2 camps (notamment un village au Sud de la Kabylie massacré par le FLN)
La fédération française du FLN ne fait pas l'effort de s'adresser aux ouvriers français
2 directions du FLN : intérieure (CCE) et extérieure (au Caire). La direction extérieure est arrêtée en octobre 1956

## 56-58 : Ceux qui ont amené De Gaulle

En France gouvernement socialiste de Guy Mollet a les pouvoirs spéciaux et les transmet à l'armée (il y a alors 450 000 soldats fr en Algérie)

7 janvier 1957 : "Bataille d'Alger", pleins pouvoir de la police au général Massu pour chasser le FLN d'Alger -> usage fréquent de la torture

Début de revendication de l'armée au pouvoir par l'ED française, plus seulement revendication d'Algérie française.
De plus, la 4ème République est instable et l'ED est partisane d'un pouvoir fort

13 mai 1958 : grande manifestation d'ED à Alger pour l'Algérie française, mais aussi pour l'armée au pouvoir, slogan lancé par les organisateurs de la manif
Ils entrent dans le bâtiment du gouvernement général à peine empêché par les CRS
Des personnalités militaires montent au balcon faire des déclarations
Un Comité de Salut Public est créé, présidé par Massu, qui fait acclamer le nom de De Gaulle par la foule.
Un télégramme est envoyé à Coty, le PR, pour un recours au "général De Gaulle, seul capable de prendre la tête du gouvernement de Salut Public pour assurer la pérénnité de l'Algérie Fr";
20 jours après, De Gaulle est investi de tous les pouvoirs par le Parlement, c'est le début de la 5ème République

## L'arrivée de De Gaulle au pouvoir

Presque toutes les colonies d'Afrique subsaharienne prennent leurs indépendance en 59-60?
Même si l'indépendance de l'Algérie apparaît de plus en plus inévitable, De Gaulle continue la guerre.
Ébauche de pourparlers avec le FLN en 1960 : échec
De mai 1961 à mars 1962 : entretiens à Evian

Pendant ce temps là, encore des coups de force de l'ED, pas satisfaite :
- semaine des barricades en janvier 1960
- nouveau putsch des généraux en avril 1961

En avril 1961, l'OAS (organisation armée secrète) devient un mouvement politique structuré
-> attentats, bombes, assassinats au couteau
Résultat : creuse le fossé entre les communautés, pour les pieds-noirs (français installés en Algérie) impossibole de rester en Algérie
Terrorisme de l'OAS transféré en France.
La police gangrénée par l'OAS ne lui fait pas vraiment la guerre, mais au contraire mattraque les travailleurs algériens.
L'expatriation des pieds noirs aurait pu faire naître un parti fasciste en France s'il n'avaient pas été intégré.

## De Gaulle et la 5ème République

De Gaulle et la nouvelle rep va beaucoup réduire l'influence électorale du PCF qui passe de 25% en 1956 à 19% en 1958, ce qui n'entraine pas vraiment de réaction
Même si PS, PC et syndicats avaient appelé à une manif de 200 000 personnes, elle était d'avantage contre le putsch et dans un esprit républicain, que contre DG

17 octobre 1961 : couvre feu imposé aux Algériens vivant en France
Ceux-ci manifestent quand même (30 000)
Repression féroce, des morts jetés dans la Seine, il y a 11 500 arrestations

8 février 1962 : Nouvelle repression contre des manifestants, 9 morts à Charonne

En France, solidarité de centaines de militants avec la cause des Algériens
-> hébergement de militant FLN, transport en voiture, passage de frontières

Du côté des organisations politiques, l'UNEF affiche de plus en plus une hostilité à la guerre dés 1959

Mais dans l'extrême-gauche, y compris dans le mouvement trotskyste, certains soutiennent le MNA, certains le FLN, mais beaucoup d'illusions sur le "socialisme algérien".

## Le FLN au pouvoir

L'ossature du pouvoir en Algérie : l'Armee de Libération Nationale, le bras armé du FLN.
Elle est en partie constituée de troupes amassées à la frontière à partir de 1959, destinées à servir de forces de l'ordre après l'indépendance, sous les ordres de Boumédienne.

5 juillet 1962 : Indépendance officielle.
Les troupes de l'état-major rentrent dans le pays depuis le Maroc.
Course aux postes et rivalités de cliques
Les dirigeants du FLN ne toléraient pas que les classes populaires fassent de la politique, débatent, etc

## Conclusion

Durant la guerre d'Algérie, les classes populaires algériennes ont fait preuve d'un héroïsme immense. Elles ont arraché à l'impérialisme français leur indépendance et montré leur capacité à défendre leur dignité.

Cette période de lutte d'émancipation nationale a représenté à plusieurs reprises occasions manquées
Pas eu de combat commun entre CO française et peuple algérien malheureusement
Pas eu de rapprochement en grande partie du fait de l'absence de direction révolutionnaire aux luttes, qui ont été mise à la remorque de directions nationalistes bourgeoises
Aujourd'hui encore, liens historiques, politiques, sociaux, entre les exploités et les opprimés des 2 côtés de la Méditerrannée

# Chap 10 du livre de M. Harbi

En 55-56, plusieurs formations politiques se rallient au FLN, qui apparaît comme un pôle d'attraction (UDMA, centralistes, Oulémas, ...)
Ces formations politiques doivent se positionner par rapport à l'insurrection armée.

Mars 1956 : le PCA fonde sa propre organisation militaire (les Combattants de la Libération), mais en juillet ils sont intégrés à l'ALN

Les messalistes créent l'Union Syndicale des Travailleurs Algériens (USTA), en réponse le FLN crée l'UGTA.
L'UGTA exige le monopole syndical
À ce moment, les syndicalistes sont réprimés en envoyés dans des "camps d'hébergement"

## Le premier visage du FLN

Imposer l'hégémonie par tous les moyens, y compris la terreur
Contrôle sur l'appreil militair (ALN)
Il y a un avant et un après novembre 1954
Le FLN joue sur la rupture


# Idées :

- L'ED française aujourd'hui, héritière de l'ED de l'époque de la guerre d'Algérie, tribune "putsch" dans VA
- Et aujourd'hui ? Hirak, ...
